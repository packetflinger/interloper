<?php
include("_inc/main.php");

// POST first
$op = (isset($site->post->op)) ? $site->post->op : "";
switch  ($op) {
	case "SetPassword":
		profile_set_password($site->post->cp, $site->post->np);
		die();
		
	case "SetStatement":
		profile_set_statement($site->post->stmt);
		die();
		
	case "SetGravatar":
		profile_set_gravatar();
		die();
		
	case "AddComment":
		comment_insert();
		die();
	
	case "EditComment":
		die("hi");
		comment_save();
		break;
		
	case "Admin.Com.SetDescription":
		admin_community_save_description();
		die();
		
	case "Admin.Com.SetDisabled":
		admin_community_save_disabled();
		die();
		
	case "Admin.Com.SetName":
		admin_community_save_name();
		die();
		
	case "Admin.Com.SetNameURL":
		admin_community_save_name_url();
		die();
	
	case "Admin.League.Create":
		admin_league_create();
		die();
		
	case "Admin.League.Delete":
		admin_league_delete();
		die();
		
	case "Admin.League.SetName":
		admin_league_save_name();
		die();
	
	case "Admin.League.SetShortName":
		admin_league_save_shortname();
		die();
		
	case "Admin.League.SetNameURL":
		admin_league_save_urlname();
		die();
	
	case "Admin.League.SetType":
		admin_league_save_type();
		die();
		
	case "Admin.League.SetTeamType":
		admin_league_save_teamtype();
		die();
		
	case "Admin.League.SetDisabled":
		admin_league_save_disabled();
		die();
	
	case "Admin.League.SetEnrollmentOpen":
		admin_league_save_enrollmentopen();
		die();
		
	case "Admin.League.SetEnrollLimit":
		admin_league_save_enrollmentlimit();
		die();
		
	case "Admin.League.SetEnrollStart":
		admin_league_save_enrollmentstart();
		die();
		
	case "Admin.League.SetEnrollEnd":
		admin_league_save_enrollmentend();
		die();
		
	case "Admin.League.SetInviteOnly":
		admin_league_save_inviteonly();
		die();
		
	case "Admin.League.SetInvisible":
		admin_league_save_invisible();
		die();
	
	case "Admin.League.Staff.Add":
		admin_league_staff_add();
		die();
		
	case "Admin.League.Staff.Delete":
		admin_league_staff_delete();
		die();
	
	case "Admin.League.Group.Add":
		admin_league_group_add();
		die();
		
	case "Admin.League.Group.Edit":
		admin_league_group_edit();
		die();
		
	case "NameLookup":
		if (isset($site->post->query)){
			name_lookup2($site->post->query);
		}
		die();
		
	case "TeamLookup":
		if (isset($site->post->query)){
			team_lookup($site->post->query);
		}
		die();
		
	case "Admin.League.Group.Members.Edit":
		admin_league_group_members_edit();
		break;
}

// GET next
$op = (isset($site->get->op)) ? $site->get->op : "someunknownoperation";
switch ($op){
	case "GetAvatar":
		profile_get_avatar($site->get->id);
		die();
		
	case "SetCountry":
		profile_set_country($site->get->id);
		die();
	
	case "SetTimeZone":
		profile_set_timezone($site->get->zone);
		die();
		
	case "SetTimeFormat":
		profile_set_timeformat($site->get->format);
		die();
		
	case "GetFlagURL":
		$id = (isset($site->get->id)) ? $site->get->id : $site->user->id;
		get_flag_url($id);
		die();
	
	case "GetTime":
		$id = (isset($site->get->id)) ? $site->get->id : $site->user->id;
		$zone = (isset($site->get->zone)) ? $site->get->zone : 38;	// 38 == UTC in the database
		$format = (isset($site->get->format)) ? $site->get->format : 0;
		get_time($id, $zone, $format);
		die();
	
	case "NameLookup":
		if (isset($site->get->query)){
			name_lookup($site->get->query);
		}
		die();
		
	case "NameLookup2":
		if (isset($site->get->query)){
			name_lookup($site->get->query);
		}
		die();
		
	case "TeamLookup":
		if (isset($site->get->query)){
			team_lookup($site->get->query);
		}
		die();
		
	case "ListLeagues":
		list_leagues();
		die();
		
	case "ListLeagueStaff":
		list_league_staff();
		die();
	
	case "TeamMembershipRoles":
		list_team_membership_roles();
		die();
		
	case "CommunityMembershipRoles":
		list_community_membership_roles();
		die();
}

function async_error() {
	$err = new stdclass();
	$err->result = 0;
	$err->message = "An error occurred, request failed";
	echo json_encode($err);
	die();
}

function name_lookup($n){
	global $site;
	
	if (strlen($n) < 1) {
		echo "";
		return;
	}
	
	$sql = "SELECT id, name, user FROM player WHERE name LIKE ?";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, "%$n%", PDO::PARAM_STR);
	$q->execute();
	
	$out = array();
	while ($r = $q->fetch(PDO::FETCH_OBJ)){
		$name = array("id" => $r->id, "name" => $r->name);
		array_push($out, $name);
	}

	echo json_encode($out);
}

function team_lookup($n){
	global $site;
	
	if (strlen($n) < 1) {
		echo "";
		return;
	}
	
	$sql = "SELECT id, name FROM team WHERE name LIKE ?";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, "%$n%", PDO::PARAM_STR);
	$q->execute();
	//if ($q->rowCount() > 0){
		$out = array();
		while ($r = $q->fetch(PDO::FETCH_OBJ)){
			$name = array("id" => $r->id, "name" => $r->name);
			array_push($out, $name);
		}
	//}
	echo json_encode($out);
}

function get_flag_url($id){
	global $site;
	$sql = "SELECT flag FROM country WHERE id = ? LIMIT 1";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $id, PDO::PARAM_INT);
	$q->execute();
	if ($q->rowCount() == 1){
		$r = $q->fetch(PDO::FETCH_OBJ);
		printf("%s/%s", $site->settings->flag_path, $r->flag);
	}
}

function profile_set_country($id) {
	global $site;
	if (!user_is_logged_in()) {
		return;
	}
	
	$sql = "UPDATE user SET country = ? WHERE id = ? LIMIT 1";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $id, PDO::PARAM_INT);
	$q->bindValue(2, $site->user->id, PDO::PARAM_INT);
	$q->execute();
}

function get_time($id, $zone, $format){
	global $site;
	$sql = "SELECT
				t.zone,
				NOW() AS now
			FROM timezone t
			WHERE t.id = ?
			LIMIT 1";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $zone, PDO::PARAM_INT);
	$q->execute();
	if ($q->rowCount() == 1){
		$r = $q->fetch(PDO::FETCH_OBJ);
		$f = ($format == "12") ? true : false;
		printf("%s",time_format($r->now, $r->zone, $f));
	}
}

function profile_set_timezone($zone) {
	global $site;
	if (!user_is_logged_in()) {
		return;
	}
	
	$sql = "UPDATE user SET timezone = ? WHERE id = ? LIMIT 1";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $zone, PDO::PARAM_INT);
	$q->bindValue(2, $site->user->id, PDO::PARAM_INT);
	$q->execute();
}

function profile_set_timeformat($f) {
	global $site;
	if (!user_is_logged_in()) {
		return;
	}
	
	$sql = "UPDATE user SET timeformat = ? WHERE id = ? LIMIT 1";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $f, PDO::PARAM_INT);
	$q->bindValue(2, $site->user->id, PDO::PARAM_INT);
	$q->execute();
}

function profile_set_password($current, $new) {
	global $site;
	if (!user_is_logged_in()) {
		return;
	}
	
	$out = new stdclass();
	if (user_auth($site->user->id, $current)) {
		user_setpassword($site->user->id, $new);
		$out->result = 1;
		$out->message = "Password changed successfully";
	} else {
		$out->result = 0;
		$out->message = "Authentication failed, wrong password";
	}
	
	echo json_encode($out);
}

function profile_set_statement($statement) {
	global $site;
	if (!user_is_logged_in()) {
		return;
	}
	
	$sql = "UPDATE user SET personal_statement = ? WHERE id = ? LIMIT 1";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, addslashes($statement), PDO::PARAM_STR);
	$q->bindValue(2, $site->user->id, PDO::PARAM_INT);
	$q->execute();
}

function profile_set_gravatar() {
	global $site;
	if (!user_is_logged_in()) {
		return 0;
	}
	
	$sql = "UPDATE user SET avatar = 'gravatar' WHERE id = ? LIMIT 1";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $site->user->id, PDO::PARAM_INT);
	$q->execute();
	
	$hash = md5(trim(strtolower($site->user->email)));
	return "https://s.gravatar.com/avatar/$hash?s=200";
}

function profile_get_avatar($id) {
	global $site;
	
	$u = user_get($id);
	$a = avatar_get_url($u, 200);
	
	if ($a == null) {
		async_error();
	}
	
	$out = new stdclass();
	$out->result = 1;
	$out->message = "request successful";
	$out->user = $u->name;
	$out->userid = $u->id;
	$out->avatar = $a;
	echo json_encode($out);
}

function list_leagues() {
	global $site;
	$c = isset($site->get->c) ? $site->get->c : 0;
	$l = league_list($c);
	if ($l != null) {
		$r = return_obj_success();
		$r->resultset = $l;
		echo json_encode($r);
		die();
	}
	
	$r = return_obj_success();
	echo json_encode($r);	
}

function list_league_staff() {
	global $site;
	$id = isset($site->get->id) ? $site->get->id : 0;
	$s = league_staff($id);
	if ($s != null) {
		// add role names
		for ($i=0; $i<sizeof($s); $i++) {
			$s[$i]->rolename = league_staff_level_to_string($s[$i]->level); 
		}
		$r = return_obj_success();
		$r->resultset = $s;
		echo json_encode($r);
		die();
	}
	
	$r = return_obj_success();
	echo json_encode($r);	
}

function list_team_membership_roles() {
	global $site;
	
	$id = isset($site->get->id) ? $site->get->id : 0;
	$m = team_membership_get($id);
	
	if ($m != null) {
		echo json_encode($m);
	}
}

function list_community_membership_roles() {
	global $site;
	
	$id = isset($site->get->id) ? $site->get->id : 0;
	$m = team_membership_get($id);
	
	if ($m != null) {
		echo json_encode($m);
	}
}
?>