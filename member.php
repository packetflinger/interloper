<?php
include_once("_inc/main.php");

// save password change
if (isset($site->post->savepw) && $site->post->savepw == "yes") {
	user_setpassword($site->user->id, $site->post->newpw);
	
}

// save email change
if (isset($site->post->saveemail) && $site->post->saveemail == "yes") {
	$sql = "UPDATE user SET email_tmp = ? WHERE id = ? LIMIT 1";
	redirect($site->settings->uri_profile_edit);
}

// edit my profile
if (isset($site->get->op) && $site->get->op == "profile" && 
	isset($site->get->cmd) && $site->get->cmd == "edit") {
	
	user_login_required();
	
	include_once("_inc/header.php");
	navigation();
?>
	<div class="container">
		<h1 class="header-title">
			Edit Profile
		</h1>
		
		<div class="well">
			<label class="font-large"><?=$site->user->email?><?=($site->user->email_tmp != "") ? " <span style=\"font-weight: normal;\">pending to</span> {$site->user->email_tmp}" : ""?></label>
			<div>
				<a class="btn btn-primary" data-toggle="modal" data-target="#email" href="#">Change Email Address</a>
				<a class="btn btn-primary" data-toggle="modal" data-target="#passwd" href="#">Change Password</a>
			</div>
		</div>
		
		<div class="well">
			<label for="country">Country</label>
			<div class="row">
				<div class="col-xs-7 col-sm-7 col-md-7">
					<select class="form-control" name="country" id="country">
					<?=user_list_countries_select($site->user->country)?>
					</select>
				</div>
				<div class="col-xs-5 col-sm-5 col-md-5">
					<img id="flag">
				</div>
			</div>

			 <label for="zone">Time Zone</label>
			<div class="row">
				<div class="col-xs-5 col-sm-5 col-md-5">
					<select class="form-control" name="zone" id="zone">
					<?=user_list_timezones_select($site->user->timezone)?>
					</select>
				</div>

				<div class="col-xs-2 col-sm-2 col-md-2">
					<input type="checkbox" id="twelvehr" name="twelvehr" <?=($site->user->timeformat == 12) ? "checked" : ""?>> 12 hour
				</div>

				<div class="col-xs-5 col-sm-5 col-md-5">
					<span id="currenttime" class="font-large"></span>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-5 col-sm-5 col-md-5">
						Pick your local timezone. All times are stored in GMT+00:00 so they can be displayed in the timezone you select.
				</div>
			</div>
		</div>
		
		<div class="well">
			<label for="statement">Personal Statement</label>
			<textarea class="form-control" id="statement" name="statement"><?=stripslashes($user->personal_statement)?></textarea>
		</div>
		
		<div class="well">
			<label for="avatar">Avatar</label>
			<div style="margin: 10px 10px 10px 0px;">
				<button id="av_gravatar" class="btn btn-primary">Use Gravatar</button>
				<button id="av_upload" class="btn btn-primary" data-toggle="modal" data-target="#avatar-modal">Upload</button>
			</div>
			<div><img id="avatar" src="<?=avatar_get_url($site->user, 200)?>"></div>
		</div>
	</div>
	
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$.get("<?=$settings->uri_rpc?>?op=GetFlagURL&id=<?=$site->user->country?>", function(data){
				$("#flag").attr("src", data);
			});
			
			$("#country").change(function() {
				$.get("<?=$site->settings->uri_rpc?>?op=GetFlagURL&id=" + $("#country").val(), function(data){
					$("#flag").attr("src", data);
				});
				$.get("<?=$site->settings->uri_rpc?>?op=SetCountry&id=" + $("#country").val());
				toastr.info("Country/flag saved");
			});
			
			$.get("<?=$settings->uri_rpc?>?op=GetTime&id=<?=$site->user->id?>&zone=<?=$site->user->timezone?>&format=<?=$site->user->timeformat?>", function(data){
				$("#currenttime").html(data);
			});
						
			$("#zone").change(function() {
				format = ($("#twelvehr").is(':checked')) ? 12 : 0;
				$.get("<?=$site->settings->uri_rpc?>?op=GetTime&id=<?=$site->user->id?>&zone=" + $("#zone").val() + "&format=" + format, function(data){
					$("#currenttime").html(data);
				});
				$.get("<?=$site->settings->uri_rpc?>?op=SetTimeZone&zone=" + $("#zone").val());
				toastr.info("Timezone saved.");
			});
			
			$("#twelvehr").change(function() {
				format = ($("#twelvehr").is(':checked')) ? 12 : 0;
				$.get("<?=$site->settings->uri_rpc?>?op=GetTime&id=<?=$site->user->id?>&zone=" + $("#zone").val() + "&format=" + format, function(data){
					$("#currenttime").html(data);
				});
				$.get("<?=$site->settings->uri_rpc?>?op=SetTimeFormat&format=" + format);
				toastr.info("Time format saved.");
			});
			
			$("#savepw").click(function() {
				if ($("#newpw").val() == $("#newpw2").val() && $("#newpw").val() != "") {
					$.post("<?=$site->settings->uri_rpc?>", {op: 'SetPassword', cp: $("#currentpw").val(), np: $("#newpw").val()}, function(result) {
						var output = JSON.parse(result);
						if (output.result == 0) {
							$("#newpw").val("");
							$("#newpw2").val("");
							$("#currentpw").val("");
							toastr.error(output.message);
						}
						
						if (output.result == 1) {
							$("#newpw").val("");
							$("#newpw2").val("");
							$("#currentpw").val("");
							$("#passwd").modal('toggle');
							toastr.success(output.message);
						}
					});
				} else {
					toastr.error("New passwords don't match");
				}
			});
			
			$("#statement").summernote({
				height: '200px',
				callbacks: {
					onBlur: function(event) {
						$.post("<?=$site->settings->uri_rpc?>", {op: 'SetStatement', stmt: $("#statement").val()}, function(result) {
							toastr.success("Personal statement saved");
						});
					}
				}
			});
			
			$("#av_gravatar").click(function() {
				$.post("<?=$site->settings->uri_rpc?>", {op: 'SetGravatar'}, function(result) {					
					toastr.success("Now using Gravatar");
					$.get("<?=$site->settings->uri_rpc?>?op=GetAvatar&id=<?=$site->user->id?>", function(result) {
						var obj = JSON.parse(result);
						$("#avatar").attr("src", obj.avatar);
					});
				});
			});
		});
	</script>
	
	<div id="passwd" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Change Password</h4>
				</div>
				<div class="modal-body">
					<fieldset class="form-group">
						<!-- <label for="currentpw">Current Password:</label> -->
						<input class="form-control" type="password" name="currentpw" id="currentpw" placeholder="Current Password" required>
					</fieldset>
					<hr>
					<fieldset class="form-group">
						<!-- <label for="currentpw">New Password:</label> -->
						<input class="form-control" type="password" name="newpw" id="newpw" placeholder="New Password" required>
						<!-- <label for="currentpw">...and again:</label> -->
						<input class="form-control" type="password" name="newpw2" id="newpw2" placeholder="Again.." required>
					</fieldset>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" name="savepw" id="savepw">Save Password</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	
	<div id="email" class="modal fade">
		<form action="" method="post">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Change Email Address</h4>
					</div>
					<div class="modal-body">
						<fieldset class="form-group">
								<input class="form-control" type="email" name="email" id="email" placeholder="Email Address" value="<?=$site->user->email?>" required>
						</fieldset>
						<p>You'll receive an email at this address to confirm the change and ensure the address is valid.</p>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary" name="saveemail" id="saveemail" value="yes">Send Request</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</form>
	</div>
	
	<link href="/assets/css/cropper.min.css" rel="stylesheet">
	<link href="/assets/css/avatar.css" rel="stylesheet">
	
	<div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form class="avatar-form" action="crop.php" enctype="multipart/form-data" method="post">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" id="avatar-modal-label">Change Avatar</h4>
					</div>
					<div class="modal-body">
						<div class="avatar-body">

							<!-- Upload image and data -->
							<div class="avatar-upload">
								<input type="hidden" class="avatar-src" name="avatar_src">
								<input type="hidden" class="avatar-data" name="avatar_data">
								<label for="avatarInput">Local upload</label>
								<input type="file" class="avatar-input" id="avatarInput" name="avatar_file">
							</div>

							<!-- Crop and preview -->
							<div class="row">
								<div class="col-md-9">
									<div class="avatar-wrapper"></div>
								</div>
							</div>

							<div class="row avatar-btns">
								<div class="col-md-9">
									<div class="btn-group">
										<button type="button" class="btn btn-primary" data-method="rotate" data-option="-90" title="Rotate -90 degrees">Rotate Left</button>
									</div>
									<div class="btn-group">
										<button type="button" class="btn btn-primary" data-method="rotate" data-option="90" title="Rotate 90 degrees">Rotate Right</button>
									</div>
								</div>
								<div class="col-md-3">
									<button type="submit" class="btn btn-primary btn-block avatar-save">Done</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
    </div>
	
	<script src="/assets/js/cropper.min.js"></script>
	<script src="/assets/js/avatar.js"></script>
<?php
	include_once("_inc/footer.php");
	die();
}

// view my profile
if (isset($site->get->op) && $site->get->op == "profile") {
	user_login_required();
	
	$u = user_get($site->user->id);
	$avatar = avatar_get_url($u, 200);
	$names = user_get_names($u);
	
	include_once("_inc/header.php");
	navigation();
?>
	<div class="container">
		<h1 class="header-title">
			My Profile
			<div class="pull-right">
				<a href="<?=$site->settings->uri_profile_edit?>" class="btn btn-primary">Edit</a>
			</div>
		</h1>
		
		<div class="well">
			<div class="row profile-name">
				<div class="col-xs-3">Player Name(s):</div>
				<div class="col-xs-9"><?=user_get_names_list($u)?></div>
			</div>
		</div>
		
		<div class="well">
			<div class="row">
				<div class="col-xs-3">Email:</div>
				<div class="col-xs-9"><?=$u->email?> <?=($u->confirmed) ? " <span title=\"Confirmed\" class=\"good glyphicon glyphicon-ok\"></span>" : ""?></div>
			</div>
			<div class="row">
				<div class="col-xs-3">User ID:</div>
				<div class="col-xs-9"><?=$u->userid?></div>
			</div>
			<div class="row">
				<div class="col-xs-3">Country/Flag:</div>
				<div class="col-xs-9"><?=$u->cname?> <img class="flag" src="<?=sprintf("%s/%s", $site->settings->flag_path, $u->flag)?>"></div>
			</div>
			<div class="row">
				<div class="col-xs-3">Date Registered:</div>
				<div class="col-xs-9"><?=format_date($u->date_registered, 1)?></div>
			</div>
			<div class="row">
				<div class="col-xs-3">Registration IP:</div>
				<div class="col-xs-9"><?=$u->register_ip . " [" . $u->register_hostname . "]"?></div>
			</div>
			<div class="row">
				<div class="col-xs-3">Login Count:</div>
				<div class="col-xs-9"><?=$u->logincount?> - last: <?=format_date($u->date_lastlogin, 1)?></div>
			</div>
			
			<div class="row">
				<div class="col-xs-3">Disabled:</div>
				<div class="col-xs-9"><?=(!$u->disabled) ? "<span class=\"good glyphicon glyphicon-remove\"></span>" : "<span class=\"bad glyphicon glyphicon-ok\"></span>"?></div>
			</div>
			<div class="row">
				<div class="col-xs-3">Muted:</div>
				<div class="col-xs-9"><?=(!$u->muted) ? "<span class=\"good glyphicon glyphicon-remove\"></span>" : "<span class=\"bad glyphicon glyphicon-ok\"></span>"?></div>
			</div>
			<div class="row">
				<div class="col-xs-3">Comment Count:</div>
				<div class="col-xs-9"><?=$u->comments?></div>
			</div>
			<div class="row">
				<div class="col-xs-3">Score:</div>
				<div class="col-xs-9"><?=$u->score?></div>
			</div>
			<div class="row">
				<div class="col-xs-3">Communities:</div>
				<div class="col-xs-9"><?=$u->community_count?></div>
			</div>
			<div class="row">
				<div class="col-xs-3">Timezone:</div>
				<div class="col-xs-9"><?=$u->zonename?></div>
			</div>
			<div class="row">
				<div class="col-xs-3">Time Format:</div>
				<div class="col-xs-9"><?=($u->timeformat == 12) ? "12 Hour - AM/PM" : "24 Hour"?></div>
			</div>
			<div class="row">
				<div class="col-xs-3">Personal Statement:</div>
				<div class="col-xs-9"><blockquote><?=stripslashes($u->personal_statement)?></blockquote></div>
			</div>
			<div class="row">
				<div class="col-xs-3">Avatar:</div>
				<div class="col-xs-9"><img src="<?=$avatar?>"></div>
			</div>
		</div>
	</div>
<?php
	include_once("_inc/footer.php");
	die();
}


// show a member
if (isset($site->get->cmd) && $site->get->cmd != "") {
	$u = user_get_by_urlname($site->get->cmd);
	$avatar = avatar_get_url($u, 200);
	include_once("_inc/header.php");
	navigation();
	
?>
	<div class="container">
		<h1 class="header-title">&nbsp;
			<div class="pull-right">
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#reply">Send Message</button>
			</div>
		</h1>
		
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<h1 style="margin-top: 0;"><?=$u->player_name?> <img src="<?=$site->settings->uri_flag?>/<?=$u->flag?>" title="<?=$u->cname?>"></h1>
					<div class="row">
						<div class="col-xs-6 col-sm-6 col-md-6 col-xl-6">Join Date</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-xl-6"><?=format_date($u->date_registered)?></div>
					</div>
					<div class="row">
						<div class="col-xs-6 col-sm-6 col-md-6 col-xl-6">Last Login</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-xl-6"><?=format_date($u->date_lastlogin)?></div>
					</div>
					<div class="row">
						<div class="col-xs-6 col-sm-6 col-md-6 col-xl-6">Communities</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-xl-6"><?=$u->community_count?></div>
					</div>
					<div class="row">
						<div class="col-xs-6 col-sm-6 col-md-6 col-xl-6">Comments</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-xl-6"><?=$u->comments?></div>
					</div>
					<div class="row">
						<div class="col-xs-6 col-sm-6 col-md-6 col-xl-6">Score</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-xl-6"><?=$u->score?></div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-xl-12"><?=$u->personal_statement?></div>
					</div>
				</div>
				
				<div class="col-md-4">
					<div class="pull-right hidden-xs"><img src="<?=$avatar?>"></div>
				</div>
			</div>
		</div>
<?=comment_block(CTX_PLAYER, $u->id)?>
	</div>
	
	
<?php
	include_once("_inc/footer.php");
	die();
}


include_once("_inc/header.php");
navigation();
$page = (isset($site->get->p)) ? $site->get->p : 0;
$limit = (isset($site->get->l)) ? $site->get->l : 25;
$users = user_list($page, $limit);
?>

	<div class="container">
		<h1 class="header-title">
			Members
			<div class="pull-right">
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#reply">Sign up</button>
			</div>
		</h1>

<?php for ($i=0; $i<sizeof($users); $i++) { ?>
		<a class="member-card" href="<?=sprintf("%s/%s", $site->settings->uri_user, $users[$i]->name_url)?>">
			<div class="row">
				<div class="col-md-6">
					<img src="<?=avatar_get_url($users[$i])?>">
				</div>
				
				<div class="col-md-6">
					<?=$users[$i]->name. "\n"?>
				</div>
			</div>
		</a>
<?php } ?>
	</div>
	
<?php
include_once("_inc/footer.php");
?>