<?php
include_once("_inc/main.php");

// handle form posts first
if (isset($site->post->op) && strlen($site->post->op) > 0) {
	switch ($site->post->op) {
		case "team.name.save":
			team_name_save();
			break;
			
		case "team.members.update":
			team_members_update();
			break;
			
		case "team.members.add":
			team_members_add();
			break;
		
		case "team.roles.save":
			team_roles_save();
			break;
	}
	
	die();
}

// then page requests
if (isset($site->get->t) && strlen($site->get->t) > 0) {
	$team = team_get($site->get->t);
	
	if (isset($site->get->admin) && $site->get->admin == 1) {
		team_admin($team);
	} else {
		team_home($team);
	}
} else {
	team_index();
}

// support functions
function team_name_save() {
	global $site;
	
	$team = team_get($site->post->team);
	$team->name = $site->post->team_name;
	$team->name_url = url_string($team->name);
	$team->country = $site->post->country;
	team_edit($team);
	
	redirect(sprintf("%s/%s/admin", $site->settings->uri_team, $team->name_url));
}

function team_members_update() {
	global $site;
	
	$team = team_get($site->post->team);
	
	if (isset($site->post->removemembers) && $site->post->removemembers == 1) {
		foreach ((array) $site->post->members as $member) {
			if ($site->user->player_id != $member) {	// can't remove yourself
				team_part($team->id, $member);
			}
		}
	}
	
	redirect_return();
}

function team_members_add() {
	global $site;
	
	$team = team_get($site->post->team);
	
	foreach ((array) $site->post->members as $member) {
		if ($site->user->player_id != $member) {	// can't add yourself
			team_join($team->id, $member);
		}
	}
	
	redirect_return();
}

function team_roles_save() {
	global $site;
	
	$bitmask = 0;
	foreach ((array) $site->post->selectedroles as $role) {
		$bitmask += (int)$role;
	}
	
	$r = team_membership_get($site->post->roleid);
	
	if ($r->player == $site->user->player_id) {
		$bitmask |= TEAM_ROLE_CAPTAIN;	// you cant un-captain yourself
	}
	
	$r->role = $bitmask;
	team_membership_edit($r);
	
	redirect_return();
}
?>