<?php
include_once("_inc/main.php");
include("_inc/header.php");
navigation();
?>

<div class="container">
	<div class="card card-container">
		<img id="profile-img" class="profile-img-card" src="/assets/img/avatar_1x.png" />
		<p id="profile-name" class="profile-name-card"></p>
		<form class="form-signin" method="post" action="<?=$settings->uri_join?>">
			<span id="reauth-email" class="reauth-email"></span>
			<p>Become a member</p>
			<input type="email" id="email" name="email" class="form-control" placeholder="Email address" required autofocus>
			<input type="hidden" id="op" name="op" value="auth.join">
			<button class="btn btn-primary btn-block btn-signin" type="submit">Continue...</button>
		</form>
	</div>
</div>

<?php
include("_inc/footer.php");
?>