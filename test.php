<?php
include_once("_inc/main.php");
include_once("_inc/header.php");
navigation();
?>

	<div class="container">
		<button id="test" class="btn btn-primary">Test</button>
	</div>

	<input type="text" id="test1">
	
	<script>
	$("#test").click(function() {
			$.get("/async.php?op=NameLookup&term=utte", function(result) {
				
				var obj = JSON.parse(result);
				
				//alert(JSON.stringify(obj));
				alert(obj[0].label);
			});
		});
		
	$( function() {
		var availableTags = [
			  "ActionScript",
			  "AppleScript",
			  "Asp",
			  "BASIC",
			  "C",
			  "C++",
			  "Clojure",
			  "COBOL",
			  "ColdFusion",
			  "Erlang",
			  "Fortran",
			  "Groovy",
			  "Haskell",
			  "Java",
			  "JavaScript",
			  "Lisp",
			  "Perl",
			  "PHP",
			  "Python",
			  "Ruby",
			  "Scala",
			  "Scheme"
		];
		
		
		$("#test1").autocomplete({
			source: 'async?op=NameLookup'
		});
	});
	</script>
<?php
include("_inc/footer.php");
?>