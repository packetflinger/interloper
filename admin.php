<?php
include_once("_inc/main.php");

if (!is_loggedin()) {
	admin_denied();
}

if (!isset($site->get->community)) {
	admin_denied();
}

if (isset($site->post->op)) {
	switch ($site->post->op) {
		case "League.Member.Add":
			admin_league_member_add();
		
		case "League.Member.Remove":
			admin_league_member_remove();
			
		case "League.Match.Edit":
			admin_league_match_edit();
			
		case "Admin.League.Match.Generate":
			admin_league_match_generate();
		
		case "Admin.League.Match.Create":
			admin_league_match_create();
		
		case "Admin.Features.Save":
			admin_features_save();
		
		case "community.staff.roles.save":
			admin_community_roles_save();
			
		case "community.staff.add":
			admin_community_staff_add();
		
		case "community.staff.update":
			admin_community_staff_update();
			
	}
	
	die();
}

switch ($site->get->community) {
	
}

admin_index();
?>