<?php
include_once("_inc/main.php");

// logout
if (isset($site->get->op) && $site->get->op == "logout") {
	user_logout();
	redirect($site->settings->uri_login);
}

// login
if (isset($site->post->op) && $site->post->op == "auth.login") {
	$u = user_get_by_email($site->post->email);
	if ($u == null) {
		login_failed();
	}

	if (user_auth($u->id, $site->post->password)) {
		$_SESSION['uid'] = $u->id;
		$r = (isset($site->post->return) && $site->post->return != "") ? base64_decode($site->post->return) : "/";
		redirect($r);
	} else {
		login_failed();
	}
}

include_once("_inc/header.php");
navigation();
?>

<div class="container">
	<div class="card card-container">
		<!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
		<img id="profile-img" class="profile-img-card" src="/assets/img/avatar/avatar_1x.png" />
		<p id="profile-name" class="profile-name-card"></p>
		<form class="form-signin" method="post" action="<?=$settings->uri_login?>">
			<span id="reauth-email" class="reauth-email"></span>
			<input type="email" id="email" name="email" class="form-control" placeholder="Email address" required autofocus>
			<input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
			<input type="hidden" id="op" name="op" value="auth.login">
			<input type="hidden" id="return" name="return" value="<?=(isset($site->get->r)) ? $site->get->r : ""?>">
			<div id="remember" class="checkbox">
				<label><input type="checkbox" value="remember-me"> Remember me</label>
			</div>
			<button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Sign in</button>
		</form>
		<a href="#" class="forgot-password">
			Forgot the password?
		</a>
	</div>
		
	<div class="text-center">
		<a href="<?=$site->settings->uri_join?>">Become a member</a>
	</div>
</div>


<?php
function login_failed() {
	global $site;
	include("_inc/header.php");
	navigation();
?>
<div class="container">
	<div class="row">
		<div class="col-xs-2 col-sm-4 col-md-4"></div>
		<div class="col-xs-8 col-sm-4 col-md-4">
			<h2 class="form-signin-heading">auth failed</h2>
			<a href="<?=$site->settings->uri_login?>" class="btn btn-primary">Try Again<a>
		</div>
	</div>
</div>
<?php
	include("_inc/footer.php");
	die();
}



include_once("_inc/footer.php");
?>