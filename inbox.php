<?php
include_once("_inc/main.php");

if (!user_is_logged_in()) {
	redirect($site->settings->uri_login);
}

$op = MSG_INBOX;

if (isset($site->get->cmd)) {
	switch ($site->get->cmd) {
		case "":
			$op = MSG_INBOX;
			break;
		case "sent":
			$op = MSG_SENT;
			break;
		case "drafts":
			$op = MSG_DRAFT;
			break;
		case "compose":
			$op = MSG_COMPOSE;
			break;
		default:
			$op = MSG_VIEW;
			$msgid = $site->get->cmd;
	}
}

// send message
if (isset($site->post->send) && $site->post->send == 1) {
	
	$sender = user_get_by_name($site->post->sender);
	
	$o = new StdClass();
	$o->sender_id = $sender->player_id;
	$o->body = $site->post->body;
	$o->subject = $site->post->subject;
	
	for ($i=0; $i<sizeof($site->post->recipient); $i++) {
		$recip = user_get_by_player_id($site->post->recipient[$i]);
		$o->userid = $recip->id;
		$o->recipient_id = $recip->player_id;
		$o->box = MSG_BOX_INBOX;
		
		// for recipient
		message_deliver($o);
		
		$o->userid = $sender->id;
		$o->box = MSG_BOX_SENT;
		
		// one for the sender
		message_deliver($o);
	}
	
	redirect($site->settings->uri_msg);
}

// view a message
if ($op == MSG_VIEW) {
	$msg = message_get($msgid);

	//die(var_dump($site->user));
	if ($msg == null) {
		die_gracefully("Invalid Message", "No message with the ID <span class=\"code-font\">$msgid</span> was found in your message store.");
	}
	
	if ($msg->user != $site->user->id) {
		die_gracefully("Invalid Message", "No message with the ID <span class=\"code-font\">$msgid</span> was found in your message store.");
	}
	
	if (!$msg->seen) {
		message_toggle_seen($msg->lookup);
	}
	
	include("_inc/header.php");
	navigation();
	?>
	<div class="container">
		<h1 class="header-title">
			Message
			<div class="btn-group pull-right">
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#reply">Reply</button>
				<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<span class="caret"></span>
					<span class="sr-only">Toggle Dropdown</span>
				</button>
				<ul class="dropdown-menu">
					<li><a href="#">Forward</a></li>
					<li><a href="#">Delete</a></li>
				</ul>
			</div>
		</h1>
		
		<div class="well">
			<div class="message-header">
				<div class="row">
					<div class="col-md-1 col-sm-1 col-xs-4">From:</div>
					<div class="col-md-11 col-sm-11 col-xs-8"><a href="<?=$site->settings->uri_user . "/" . $msg->name_sender_url?>"><?=$msg->name_sender?></a></div>
					
					<div class="col-md-1 col-sm-1 col-xs-4">To:</div>
					<div class="col-md-11 col-sm-11 col-xs-8"><?=$msg->name_recipient?></div>
					
					<div class="col-md-1 col-sm-1 col-xs-4">Date:</div>
					<div class="col-md-11 col-sm-11 col-xs-8"><?=format_date($msg->sent_date, true)?></div>
					
					<div class="col-md-1 col-sm-1 col-xs-4">Subject:</div>
					<div class="col-md-11 col-sm-11 col-xs-8"><?=$msg->subject?></div>
				</div>
			</div>
			<p>
			<?=$msg->body?>
			</p>
		</div>
		<div><a href="<?=$site->settings->uri_msg?>">Back to Inbox</a></div>
	</div>
	
	<div id="reply" class="modal fade" role="dialog">
		<form role="form" method="post" action="<?=$settings->uri_msg?>">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Reply to <?=$msg->name_sender?></h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<textarea id="body" name="body">
								<p>&nbsp;</p>
								<div class="well" style="background-color: #ccc; !important">
									<p style="margin-top: 0; border-bottom: 1px dashed #000;">From <?=user_make_anchor($msg->name_sender)?> to <?=user_make_anchor($msg->name_recipient)?> on <?=time_format($msg->sent_date, "UTC")?> UTC</p>
									<?=$msg->body?>
								</div>
							</textarea>
						</div>
						<input type="hidden" name="op" value="sendmessage">
						<input type="hidden" name="sender" value="<?=$msg->name_recipient?>">
						<input type="hidden" name="recipient" value="<?=$msg->name_sender?>">
						<input type="hidden" name="subject" value="Re: <?=$msg->subject?>">
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary" name="send" value="1"><span class="glyphicon glyphicon-send"></span> Send</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</form>
	</div>
	
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$("#body").summernote({
				height: '200px'
			});
		});
	</script>
	<?php

	include("_inc/footer.php");
	die();
}


// inbox list
$offset = (isset($site->get->offset) && is_numeric($site->get->offset)) ? $site->get->offset : 0;
$length = (isset($site->get->length) && is_numeric($site->get->length)) ? $site->get->length : $site->settings->msg_pagelimit;

include("_inc/header.php");
navigation();

$col = "From";
switch ($op) {
	case MSG_SENT:
		$title = "Sent Msgs";
		$col = "To";
		$folder = MSG_BOX_SENT;
		break;
	case MSG_DELETED:
		$title = "Discarded Msgs";
		$folder = MSG_BOX_TRASH;
		break;
	case MSG_DRAFT:
		$title = "Drafts";
		$folder = MSG_BOX_DRAFTS;
		break;
	default:
		$title = "Inbox";
		$folder = MSG_BOX_INBOX;
}

$players = user_get_players($site->user->id);
$playeroptions = "";
for ($i=0; $i<sizeof($players); $i++) {
	$playeroptions .= "<option value=\"{$players[$i]->name}\">{$players[$i]->name}</option>";
}

$msgs = message_inbox($site->user->id, $offset, $length, $folder);
?>

	<div class="container">
		<h1 class="header-title"><?=$title?> <span class="pull-right"><button class="btn btn-primary" data-toggle="modal" data-target="#compose">Compose</button></span></h1>
		<div class="well">
			<form>
				<table class="table">
					<thead>
						<tr>
							<th></th>
							<th><?=$col?></th>
							<th>Subject</th>
							<th>Sent</th>
						</tr>
					</thead>
					<tbody>
					<?php for ($i=0; $i<sizeof($msgs); $i++) { ?>
						<tr<?=($msgs[$i]->seen == 0) ? " class=\"message-unread\"" : ""?>>
							<td><input type="checkbox"></td>
							<td><?=$msgs[$i]->name_sender?></td>
							<td><a href="<?=$site->settings->uri_msg?>/<?=$msgs[$i]->lookup?>"><?=$msgs[$i]->subject?></a></td>
							<td><?=format_date($msgs[$i]->sent_date)?></td>
						</tr>
					<?php } ?>
					</tbody>
				</table>
				
				<span>
					<div class="btn-group">
						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Selection <span class="caret"></span></button>
						<ul class="dropdown-menu">
							<li><a href="#">Delete</a></li>
							<li><a href="#">Read</a></li>
						</ul>
					</div>
				</span>
				
				<span>
					<div class="btn-group">
						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">View <span class="caret"></span></button>
						<ul class="dropdown-menu">
							<li><a href="<?=$site->settings->uri_msg?>">Inbox</a></li>
							<li><a href="<?=$site->settings->uri_msg?>/sent">Sent</a></li>
							<li><a href="<?=$site->settings->uri_msg?>/drafts">Drafts</a></li>
							<li><a href="<?=$site->settings->uri_msg?>/garbage">Garbage</a></li>
						</ul>
					</div>
				</span>
			</form>
			
			<!--
			<span class="pull-right">
				<a href="#">Prev.</a>
				X/Y
				<a href="#">Next</a>
			</span>
			-->
		</div>
	</div>
	<style>
		.ui-autocomplete-loading {
			background: white url("/assets/img/ui-anim_basic_16x16.gif") right center no-repeat;
		}
		
		.ui-autocomplete {
			z-index: 5000;
		}
	</style>
	
	<div id="compose" class="modal fade" role="dialog">
		<form role="form" method="post" action="<?=$site->settings->uri_msg?>">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">New Message</h4>
					</div>
					
					<div class="modal-body">
<?php if (sizeof($players) > 1) { ?>
						<div class="row newmsg-row">
							<div class="col-md-2">From:</div>
							<div class="col-md-10"><select class="form-control" id="senderplayer" name="sender"><?=$playeroptions?></select></div>
						</div>
<?php } else { ?>
						<input type="hidden" name="sender" value="<?=$site->user->name?>"/>
<?php } ?>
						<div class="row newmsg-row">
							<div class="col-md-2">To:</div>
							<!-- <div class="col-md-10"><input class="form-control" type="text" id="targetplayer" name="recipient" placeholder="Start typing a name"></div> -->
							<div class="col-md-10"><input class="form-control" type="text" id="toplayer" name="recipient" placeholder="Start typing a name"></div>
						</div>
						
						<div class="row newmsg-row">
							<div class="col-md-2">Subject:</div>
							<div class="col-md-10"><input class="form-control" type="text" id="subject" name="subject" placeholder="What is this about?"></div>
						</div>
					
						<div class="form-group">
							<textarea id="body" name="body"></textarea>
						</div>
						
						<input type="hidden" name="op" value="sendmessage">
					</div>
					
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary" name="send" value="1"><span class="glyphicon glyphicon-send"></span>  Send</button>
						<!-- <button type="submit" class="btn btn-default" name="draft" value="1">Save Draft</button> -->
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</form>
	</div>

	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$("#body").summernote({
				height: '200px'
			});
			
			$("#targetplayer").autocomplete({
				source: '<?=$site->settings->uri_rpc?>?op=NameLookup',
				minLength: 3
			});
			
			$("#toplayer").magicSuggest({
				allowFreeEntries: false,
				data: '<?=$site->settings->uri_rpc?>',
				dataUrlParams: {op: 'NameLookup'},
				hideTrigger: true,
				highlight: false,
				maxDropHeight: 145,
				method: 'get',
				minChars: 3
			});
		});
	</script>
<?php
include("_inc/footer.php");
?>