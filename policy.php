<?php
include_once("_inc/main.php");

// add new
if (isset($site->post->op) && $site->post->op == "policy.add") {
	$p = slash($site->post);
	$c = community_get_by_name($p->community);
	if (community_is_admin($c->id, $site->user->id)) {
		$sql = "INSERT INTO policy (
					creator, community, active, name, name_url, body, date_created
				) VALUES (
					?, ?, 1, ?, ?, ?, NOW()
				)";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $site->user->id, PDO::PARAM_INT);
		$q->bindValue(2, $c->id, PDO::PARAM_INT);
		$q->bindValue(3, $p->title, PDO::PARAM_STR);
		$q->bindValue(4, $p->utitle, PDO::PARAM_STR);
		$q->bindValue(5, $p->body, PDO::PARAM_STR);		
		$q->execute();
		redirect("/policy/" . $p->utitle);
	}
	die_gracefully("Can't Add Policy", "You don't have sufficient permission to add a policy to this community");
}

// save edit
if (isset($site->post->op) && $site->post->op == "policy.edit") {
	$p = slash($site->post);
	$policy = policy_get($p->id);
	if (community_is_admin($policy->community, $site->user->id)) {
		$sql = "UPDATE policy SET
					name = ?,
					name_url = ?,
					body = ?,
					date_updated = NOW()
				WHERE id = ?
				LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $p->title, PDO::PARAM_STR);
		$q->bindValue(2, $p->utitle, PDO::PARAM_STR);
		$q->bindValue(3, $p->body, PDO::PARAM_STR);
		$q->bindValue(4, $p->id, PDO::PARAM_INT);
		$q->execute();
		redirect("/policy/" . $p->utitle);
	}
	redirect("/policy/" . $policy->name_url);
}

// edit a policy
if (isset($site->get->op) && $site->get->op == "edit" && isset($site->get->name) && $site->get->name != "") {
	$policy = policy_get_by_name($site->get->name);
	$policy = unslash($policy);
	include_once("_inc/header.php");
	navigation();
?>
	<div class="container">
		<h1 class="header-title">Edit Policy</h1>
		
		<form action="" method="post">
			<div class="well">
				<label for="title">Policy Title</label>
				<input class="form-control" type="text" id="title" name="title" value="<?=$policy->name?>" required autofocus>
				
				<label for="title">URL Title</label>
				<input class="form-control" type="text" id="utitle" name="utitle" value="<?=$policy->name_url?>" required>
			</div>
			<div class="well">
				<textarea name="body" id="body" required><?=$policy->body?></textarea>
			</div>
			
			<input type="hidden" name="op" value="policy.edit">
			<input type="hidden" name="id" value="<?=$policy->id?>">
			<input type="hidden" name="return" value="<?=get_return_url()?>">
			<p><button type="submit" class="btn btn-primary">Save Policy</button></p>
		</form>
	</div>
	
	<script>
		jQuery(document).ready(function($) {
			$("#body").summernote({
				height: '500px'
			});
		});
	</script>
<?php
	include_once("_inc/footer.php");
	die();
}

// add form
if (isset($site->get->op) && $site->get->op == "add" && isset($site->get->community) && $site->get->community != "") {
	include_once("_inc/header.php");
	navigation();
?>
	<div class="container">
		<h1 class="header-title">Create a Policy</h1>
		
		<form action="" method="post">
			<div class="well">
				<label for="title">Title</label>
				<input class="form-control" type="text" id="title" name="title" value="" placeholder="Community X's Cheating Policy" required autofocus>
				
				<label for="title">URL Title</label>
				<input class="form-control" type="text" id="utitle" name="utitle" value="" placeholder="community-xs-cheating-policy" required>
			</div>
			<div class="well">
				<textarea name="body" id="body" required></textarea>
			</div>
			
			<input type="hidden" name="op" value="policy.add">
			<input type="hidden" name="community" value="<?=$site->get->community?>">
			<input type="hidden" name="return" value="<?=get_return_url()?>">
			<p><button type="submit" class="btn btn-primary">Create</button></p>
		</form>
	</div>
	
	<script>
		jQuery(document).ready(function($) {
			$("#body").summernote({
				height: '500px'
			});
		});
	</script>
<?php
	include_once("_inc/footer.php");
	die();
}

// show a policy
if (isset($site->get->name) && $site->get->name != "") {
	$policy = policy_get_by_name($site->get->name);
	if ($policy == null) {
		die_gracefully("Policy Not Found", "The policy you requested could not be located");
	}
	$policy = unslash($policy);
	$date = ($policy->ustamp > $policy->cstamp) ? format_date($policy->date_updated) : format_date($policy->date_created);
	$editbtn = (community_is_member($policy->community, $site->user->id) >= 100) ? " <span class=\"pull-right\"><a class=\"btn btn-primary\" href=\"?op=edit\">Edit</a></span>" : "";
	$affirm = (isset($site->get->affirm) && $site->get->affirm != "") ? "<div class=\"alert alert-danger\"><span class\"font-large\">You must agree to this policy before continuing.</span><p>Have you read, understood, and agree to follow this policy?</p> <button id=\"agree\" class=\"btn btn-success\">Yes, I agree</button> <button id=\"disagree\" class=\"btn btn-default\">No I do not</button></div>" : "";
	include_once("_inc/header.php");
	navigation();
?>

	<div class="container">
		<h1 class="header-title">Policy<?=$editbtn?></h1>
		<?=$affirm?>
		<div class="well">
			<h2><?=$policy->name?></h2>
			<div class="policy-byline">Created by <a href="<?=$site->settings->uri_user?>/<?=$policy->creator_name_url?>"><?=$policy->creator_name?></a> - updated <?=$date?></div>
			<hr>
			<div>
			<?=$policy->body?>
			</div>
		</div>
	</div>
	
<?php
	include_once("_inc/footer.php");
}
?>