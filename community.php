<?php
include_once("_inc/main.php");

include_once("_inc/header.php");

navigation();
$communities = community_list();
?>
	<div class="container">
		<h1 class="header-title">
			Communities
<?php if (user_is_logged_in()) {?>
			<div class="pull-right">
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#new-community">Create</button>
			</div>
<?php } ?>
		</h1>
		
		<div class="well">
<?php for ($i=0; $i<sizeof($communities); $i++) {
	if ($communities[$i]->disabled || !$communities[$i]->approved) {
		continue;
	}
	$c = unslash($communities[$i]);
?>
			<div><a href="<?=$c->name_url?>"><?=$c->name?></a></div>
<?php } ?>
		</div>
		
		<div id="new-community" class="modal fade">
			<form role="form" action="<?=$sites->settings->uri_community?>" method="post">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">Create a community</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary" name="postcomment">Create</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						</div>
					</div>
				</div>
				<input type="hidden" name="context" value="10"/>
				<input type="hidden" name="id" value="33"/>
				<input type="hidden" name="return" value="L2Fzc2V0LnBocD9vcD1wcm9maWxlLm1hbmFnZSZpZD0zMw=="/>
			</form>
		</div>
	</div>
<?php
// output html footer
include_once("_inc/footer.php");

?>