<?php

function group_create($o) {
	global $site;
	
	$sql = "INSERT INTO league_group (
				name, name_url, league, playoff,
				player_count, creator, sort_order, date_added
			) VALUES (
				?, ?, ?, ?,
				?, ?, 0, NOW()
			)";
	try {
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $o->name, PDO::PARAM_STR);
		$q->bindValue(2, $o->name_url, PDO::PARAM_STR);
		$q->bindValue(3, $o->league, PDO::PARAM_INT);
		$q->bindValue(4, $o->playoff, PDO::PARAM_INT);
		
		$q->bindValue(5, $o->player_count, PDO::PARAM_INT);
		$q->bindValue(6, $o->creator, PDO::PARAM_INT);
		
		$q->execute();
		
		$r = return_obj_success();
		$r->league_id = $site->db->lastInsertId();

		return $r; 
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}

function group_delete($id) {
	global $site;
	
	$sql = "DELETE FROM league_group WHERE id = ? LIMIT 1";
	
	try {
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $id, PDO::PARAM_INT);
		$q->execute();

		$r = return_obj_success();
		return $r; 
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}

function group_edit($o) {
	global $site;
	
	$sql = "UPDATE league_group SET
				name = ?, name_url = ?, league = ?, playoff = ?,
				player_count = ?, creator = ?, sort_order = ?, date_added = ?
			WHERE id = ?
			LIMIT 1";
	
	try {
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $o->name, PDO::PARAM_STR);
		$q->bindValue(2, $o->name_url, PDO::PARAM_STR);
		$q->bindValue(3, $o->league, PDO::PARAM_INT);
		$q->bindValue(4, $o->playoff, PDO::PARAM_INT);
		
		$q->bindValue(5, $o->player_count, PDO::PARAM_INT);
		$q->bindValue(6, $o->creator, PDO::PARAM_INT);
		$q->bindValue(7, $o->sort_order, PDO::PARAM_INT);
		$q->bindValue(8, $o->date_added, PDO::PARAM_STR);
		
		$q->bindValue(9, $o->id, PDO::PARAM_INT);
		
		$q->execute();
		
		$r = return_obj_success();

		return $r; 
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}

function group_get($id) {
	global $site;
	
	try {
		$sql = "SELECT * FROM league_group WHERE id = ? LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $id, PDO::PARAM_INT);
		$q->execute();
		
		while ($r = $q->fetch(PDO::FETCH_OBJ)) {
			return $r;
		}
		
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}

function group_get_available_members($id) {
	global $site;
	
	try {
		$sql = "SELECT l.league_type 
				FROM league l
				JOIN league_group g ON g.league = l.id
				WHERE g.id = ?
				LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $id, PDO::PARAM_INT);
		$q->execute();
			
		while ($r = $q->fetch(PDO::FETCH_OBJ)) {
			
			if ($r->league_type == LEAGUE_TEAM_TYPE_PLAYER) {
				$sql = "SELECT lp.*, 
							p.id AS member_id, 
							p.name AS member_name, 
							p.name_url AS member_name_url
						FROM league_player lp
						JOIN player p ON p.id = lp.team
						WHERE p.id NOT IN (
							SELECT lgm.team_id 
							FROM league_group_membership lgm
							WHERE lgm.group_id = ?
						)";
						
			} else {
				$sql = "SELECT lp.*, 
							p.id AS member_id, 
							p.name AS member_name, 
							p.name_url AS member_name_url
						FROM league_player lp
						JOIN team t ON p.id = lp.team
						WHERE p.id NOT IN (
							SELECT team_id
							FROM league_group_membership
							WHERE group_id = ?
						)";
			}
			$qm = $site->db->prepare($sql);
			$qm->bindValue(1, $id, PDO::PARAM_INT);
			$qm->execute();
			
			while ($rm = $qm->fetchAll(PDO::FETCH_OBJ)) {
				return $rm;
			}
		}
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}

function group_get_members($id) {
	global $site;
	
	try {
		$sql = "SELECT l.league_type 
				FROM league l
				JOIN league_group g ON g.league = l.id
				WHERE g.id = ?
				LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $id, PDO::PARAM_INT);
		$q->execute();
			
		while ($r = $q->fetch(PDO::FETCH_OBJ)) {
			
			if ($r->league_type == LEAGUE_TEAM_TYPE_PLAYER) {
				$sql = "SELECT m.*, 
							p.id AS member_id, 
							p.name AS member_name, 
							p.name_url AS member_name_url
						FROM league_group_membership m
						JOIN player p ON p.id = m.team_id
						WHERE m.group_id = ?
						ORDER BY p.name";
			} else {
				$sql = "SELECT m.*, 
							p.id AS member_id, 
							p.name AS member_name, 
							p.name_url AS member_name_url
						FROM league_group_membership m
						JOIN team t ON p.id = m.team_id
						WHERE m.group_id = ?
						ORDER BY p.name";
			}
			$qm = $site->db->prepare($sql);
			$qm->bindValue(1, $id, PDO::PARAM_INT);
			$qm->execute();
			
			while ($rm = $qm->fetchAll(PDO::FETCH_OBJ)) {
				return $rm;
			}
		}
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}

function group_list($league_id, $offset = 0, $limit = 25) {
	global $site;
	
	try {
		$sql = "SELECT * 
				FROM league_group 
				WHERE league = ? 
				ORDER BY sort_order DESC, name_url, id
				LIMIT ? 
				OFFSET ?";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $league_id, PDO::PARAM_INT);
		$q->bindValue(2, $limit, PDO::PARAM_INT);
		$q->bindValue(3, $offset, PDO::PARAM_INT);
		$q->execute();
		
		while ($r = $q->fetchAll(PDO::FETCH_OBJ)) {
			return $r;
		}
		
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}

function group_member_add($g, $t) {
	global $site;
	
	$sql = "INSERT INTO league_group_membership (
				group_id, team_id
			) VALUES (
				?, ?
			)";
	try {
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $g, PDO::PARAM_INT);
		$q->bindValue(2, $t, PDO::PARAM_INT);
		$q->execute();
		
		$r = return_obj_success();
		$r->id = $site->db->lastInsertId();
		
		return $r;
		
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}

function group_member_remove($g, $t) {
	global $site;
	
	$sql = "SELECT id
			FROM league_group_membership
			WHERE group_id = ? AND team_id = ?
			LIMIT 1";
	try {
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $league_id, PDO::PARAM_INT);
		$q->bindValue(2, $player_id, PDO::PARAM_INT);
		$q->execute();
		
		while ($r = $q->fetch(PDO::FETCH_OBJ)) {
			$site->db->query("DELETE FROM league_group_membership WHERE id = {$r->id} LIMIT 1");
			$r = return_obj_success();
			return $r;
		}
		
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}

function group_member_remove_all($g) {
	global $site;
	
	$sql = "DELETE FROM league_group_membership WHERE group_id = ?";
	try {
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $g, PDO::PARAM_INT);
		$q->execute();
		
		$r = return_obj_success();
		return $r;
		
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}
?>