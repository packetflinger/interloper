<?php

function content_add($obj) {
	global $site;
	
	try {
		$sql = "INSERT INTO content (
					community, player, date_added, name, name_url, body
				) VALUES (
					?, ?, NOW(), ?, ?, ?
				)";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $obj->community, PDO::PARAM_INT);
		$q->bindValue(2, $obj->player, PDO::PARAM_INT);
		$q->bindValue(3, $obj->name, PDO::PARAM_STR);
		$q->bindValue(4, $obj->name_url, PDO::PARAM_STR);
		$q->bindValue(5, $obj->body, PDO::PARAM_STR);
		$q->execute();
		return return_obj_success();
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}

function content_get($id) {
	global $site;
	
	try {
		$sql = "SELECT * FROM content WHERE id = ? LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $obj->id, PDO::PARAM_INT);
		$q->execute();
		while ($r = $q->fetch(PDO::FETCH_OBJ)) {
			$tmp = return_obj_success();
			$tmp->result = $r;
			return $r;
		}
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}

function content_get_list($cid, $offset=0, $length=10) {
	global $site;
	
	try {
		$sql = "SELECT c.*, p.name AS pname, p.name_url AS pname_url, p.user
				FROM content c
				JOIN player p ON p.id = c.player
				WHERE c.community = ?
				LIMIT ? 
				OFFSET ?";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $cid, PDO::PARAM_INT);
		$q->bindValue(2, $length, PDO::PARAM_INT);
		$q->bindValue(3, $offset, PDO::PARAM_INT);
		$q->execute();
		
		$success = return_obj_success();
		while ($r = $q->fetchAll(PDO::FETCH_OBJ)) {	
			$success->result = $r;
			return $success;
		}
		
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}

?>