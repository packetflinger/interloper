<?php

function community_add_player($p) {
	global $site;
	$sql = "INSERT INTO player (
				user, community, name, name_url
			) VALUES (
				?, ?, ?, ?
			)";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $p->user, PDO::PARAM_INT);
	$q->bindValue(2, $p->community, PDO::PARAM_INT);
	$q->bindValue(3, $p->name, PDO::PARAM_STR);
	$q->bindValue(4, $p->name_url, PDO::PARAM_STR);
	$q->execute();
}

// ban a user from a community 
function community_ban($cid, $uid, $ban, $hours=48) {
	global $site;
	$sql = "UPDATE community_membership SET
				banned = ?,
				ban_expire_date = DATE_ADD(NOW(), INTERVAL ? HOUR)
			WHERE community = ? AND user = ? 
			LIMIT 1";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $ban, PDO::PARAM_INT);
	$q->bindValue(2, $hours, PDO::PARAM_INT);
	$q->bindValue(3, $cid, PDO::PARAM_INT);
	$q->bindValue(4, $uid, PDO::PARAM_INT);
	$q->execute();
}

function community_create($obj) {
	global $site;
	
	try {
		db_begin();
		$sql = "INSERT INTO community (
					name, name_url, game, parent, date_created, creator, approved
				) VALUES (
					?, ?, ?, ?, NOW(), ?, ?
				)";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $obj->name, PDO::PARAM_STR);
		$q->bindValue(2, $obj->name_url, PDO::PARAM_STR);
		$q->bindValue(3, $obj->game, PDO::PARAM_INT);
		$q->bindValue(4, $obj->parent, PDO::PARAM_INT);
		$q->bindValue(5, $obj->creator, PDO::PARAM_INT);
		$q->bindValue(6, $obj->approved, PDO::PARAM_INT);
		$q->execute();
		$cid = $site->db->lastInsertId();
		
		$sql = "UPDATE user SET 
					community_count = community_count + 1,
					community_date_last = NOW()
				WHERE id = ?
				LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $obj->creator, PDO::PARAM_INT);
		$q->execute();
		
		community_join($cid, $obj->creator);
		
		db_commit();	// may be committed downstream
	} catch (PDOException $e) {
		db_die($e);
	}
}

function community_define() {
	define("LEVEL_ADMIN", 100);
	define("LEVEL_MODERATOR", 50);
	define("LEVEL_CONTRIBUTOR", 25);
	define("LEVEL_MEMBER", 0);
	
	define("COMMUNITY_MEMBER", 1);
	define("COMMUNITY_CONTRIBUTOR", 2);
	define("COMMUNITY_MODERATOR", 4);
	define("COMMUNITY_ADMIN", 8);
	define("COMMUNITY_HELPER", 16);
}

function community_delete_player($uid, $cid) {
	global $site;
	
	try {
		$sql = "SELECT id FROM player WHERE user = ? AND community = ? LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $uid, PDO::PARAM_INT);
		$q->bindValue(2, $cid, PDO::PARAM_INT);
		$q->execute();
		
		while ($r = $q->fetch(PDO::FETCH_OBJ)) {
			$sql = "DELETE FROM player WHERE id = ? LIMIT 1";
			$q1 = $site->db->prepare($sql);
			$q1->bindValue(1, $r->id, PDO::PARAM_INT);
			$q1->execute();
			return;
		}
	} catch(PDOException $e) {
		die($e->getMessage());
	}
}

function community_edit($obj) {
	global $site;
	
	slash($obj);
	try {
		db_begin();
		$sql = "UPDATE community SET
					name = ?,
					name_url = ?,
					description = ?,
					game = ?,
					type = ?, 
					parent = ?,
					date_created = ?,
					creator = ?,
					creator_player = ?,
					approved = ?,
					disabled = ?,
					member_count = ?,
					group_count = ?,
					features = ?
				WHERE id = ?
				LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $obj->name, PDO::PARAM_STR);
		$q->bindValue(2, $obj->name_url, PDO::PARAM_STR);
		$q->bindValue(3, $obj->description, PDO::PARAM_STR);
		$q->bindValue(4, $obj->game, PDO::PARAM_INT);
		$q->bindValue(5, $obj->type, PDO::PARAM_INT);
		$q->bindValue(6, $obj->parent, PDO::PARAM_INT);
		$q->bindValue(7, $obj->date_created, PDO::PARAM_STR);
		$q->bindValue(8, $obj->creator, PDO::PARAM_INT);
		$q->bindValue(9, $obj->creator_player, PDO::PARAM_INT);
		$q->bindValue(10, $obj->approved, PDO::PARAM_INT);
		$q->bindValue(11, $obj->disabled, PDO::PARAM_INT);
		$q->bindValue(12, $obj->member_count, PDO::PARAM_INT);
		$q->bindValue(13, $obj->group_count, PDO::PARAM_INT);
		$q->bindValue(14, $obj->features, PDO::PARAM_INT);
		$q->bindValue(15, $obj->id, PDO::PARAM_INT);
		$q->execute();
		
		db_commit();	// may be committed downstream
		return return_obj_success();
	} catch (PDOException $e) {
		db_rollback();
		return return_obj_fail($e->getMessage());
	}
}

function community_edit_player($p) {
	global $site;
	$sql = "UPDATE player SET
				name = ?,
				name_url = ?
			WHERE user = ?
				AND community = ?
			LIMIT 1";
	$q = $site->db->prepare($sql);
	
	$q->bindValue(1, $p->name, PDO::PARAM_STR);
	$q->bindValue(2, $p->name_url, PDO::PARAM_STR);
	$q->bindValue(3, $p->user, PDO::PARAM_INT);
	$q->bindValue(4, $p->community, PDO::PARAM_INT);
	$q->execute();
}

function community_get($id) {
	global $site;
	if (isset($id) && is_numeric($id)){
		$sql = "SELECT * 
				FROM community
				WHERE id = ? 
				LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $id, PDO::PARAM_INT);
		$q->execute();
		
		while ($r = $q->fetch(PDO::FETCH_OBJ)) {
			return $r;
		}
		
		return null;
	}
}

function community_get_by_name($name) {
	global $site;
	if (isset($name) && $name != ""){
		$sql = "SELECT c.*,
					ctx.name AS context,
					g.name AS game_name,
					g.name_url AS game_name_url
				FROM community c
				JOIN context ctx ON ctx.id = c.type
				LEFT OUTER JOIN game g ON g.id = c.game
				WHERE c.name_url = ? 
				LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $name, PDO::PARAM_STR);
		$q->execute();
		
		while ($r = $q->fetch(PDO::FETCH_OBJ)) {
			$r->member = community_is_member($r->id, $site->user->player_id);
			return $r;
		}
		
		return null;
	}
}

function community_group_add($obj) {
	global $site;
	
	try {
		db_begin();
		$sql = "INSERT INTO community_group (
					community, access_key, name, name_url, creator, creation_date
				) VALUES (
					?,?,?,?,?,NOW()
				)";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $obj->community, PDO::PARAM_INT);
		$q->bindValue(2, $obj->key, PDO::PARAM_STR);
		$q->bindValue(3, $obj->name, PDO::PARAM_STR);
		$q->bindValue(4, $obj->name_url, PDO::PARAM_STR);
		$q->bindValue(5, $obj->creator, PDO::PARAM_INT);
		$q->execute();
		
		$sql = "UPDATE community SET group_count = group_count + 1 WHERE id = ? LIMIT 1";
		$q = $db->prepare($sql);
		$q->bindValue(1, $obj->community, PDO::PARAM_INT);
		$q->execute();
		
		db_commit();
		return true;
	} catch (PDOException $e) {
		db_die($e->getMessage());
	}
	
	return false;
}

function community_group_get($id) {
	global $site;
	if (isset($id) && is_numeric($id)){
		$sql = "SELECT * 
				FROM community_group
				WHERE id = ? 
				LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $id, PDO::PARAM_INT);
		$q->execute();
		
		while ($r = $q->fetch(PDO::FETCH_OBJ)) {
			return $r;
		}
		
		return null;
	}
}

function community_group_is_member($gid, $uid) {
	global $site;
	$sql = "SELECT * 
			FROM community_group_membership 
			WHERE community_group = ? 
				AND user = ? 
			LIMIT 1";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $gid, PDO::PARAM_INT);
	$q->bindValue(2, $uid, PDO::PARAM_INT);
	$q->execute();
	
	while ($r = $q->fetch(PDO::FETCH_OBJ)) {
		if ($r->banned)
			return 3;
		
		if ($r->muted)
			return 2;
		
		return 1;
	}
	
	return 0;
}

function community_group_join($obj) {
	global $site;
	try {
		$sql = "INSERT INTO community_group_membership (
					community_group, user, level, join_date
				) VALUES (
					?,?,?,NOW()
				)";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $obj->group, PDO::PARAM_INT);
		$q->bindValue(2, $obj->user, PDO::PARAM_INT);
		$q->bindValue(3, $obj->level, PDO::PARAM_INT);
		$q->execute();
		
	} catch (PDOException $e) {
		db_die($e->getMessage());
	}
}

function community_group_part($gid, $uid) {
	global $site;
	try {
		$sql = "SELECT id FROM community_group_membership WHERE community_group = ? AND user = ? LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $gid, PDO::PARAM_INT);
		$q->bindValue(2, $uid, PDO::PARAM_INT);
		$q->execute();
		
		while ($r = $q->fetch(PDO::FETCH_OBJ)) {
			$sql = "DELETE FROM community_group_membership WHERE id = ? LIMIT 1";
			$q = $site->db->prepare($sql);
			$q->bindValue(1, $r->id, PDO::PARAM_INT);
			$q->execute();
			return true;
		}
	} catch (PDOException $e) {
		db_die($e->getMessage());
	}
	
	return false;
}

function community_index() {
	global $site;
	
	if (isset($site->args->feature)) {
		$feature = "{$site->args->feature}_index";
		$feature();
		die();
	}
?>
	<div class="container">
		<h1 class="header-title">
			<?=$site->community->name?> Home
			<div class="pull-right">
<?php if (community_is_admin($site->community->id, $site->user->player_id)) {?>
				<a href="<?=$site->community->name_url?>/admin"><span class="glyphicon glyphicon-cog icon-sm"></span></a>
<?php } ?>
<?php if (user_is_logged_in()) {?>
				<a href="#" data-toggle="modal" data-target="#new-content"><span class="glyphicon glyphicon-plus icon-sm"></span></a>
<?php } ?>
			</div>
		</h1>
		
		<div class="row">
			<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 col-xl-10">
				<div class="well">
					main content
					<?=community_show_content()?>
				</div>
			</div>
			<div class="hidden-xs col-sm-2 col-md-2 col-lg-2 col-xl-2">
				<div class="well">
					<?=community_sidebar()?>
				</div>
			</div>
		</div>
	</div>
	
<?php
}

function community_is_admin($cid, $pid) {
	if (community_is_member($cid, $pid) & COMMUNITY_ADMIN) {
		return true;
	}
	return false;
}

function community_is_member($cid, $pid) {
	global $site;
	$sql = "SELECT * FROM community_membership WHERE community = ? AND player = ? LIMIT 1";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $cid, PDO::PARAM_INT);
	$q->bindValue(2, $pid, PDO::PARAM_INT);
	$q->execute();
	
	while ($r = $q->fetch(PDO::FETCH_OBJ)) {
		
		if ($r->banned)
			return -2;
		
		if ($r->muted)
			return -1;
		
		return $r->level_mask;
	}
	
	return 0;
}

// mute a user in a community (they can't post anything)
function community_mute($cid, $uid, $mute, $hours=48) {
	global $site;
	$sql = "UPDATE community_membership SET
				muted = ?,
				mute_expire_date = DATE_ADD(NOW(), INTERVAL ? HOUR)
			WHERE community = ? AND user = ? 
			LIMIT 1";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $mute, PDO::PARAM_INT);
	$q->bindValue(2, $hours, PDO::PARAM_INT);
	$q->bindValue(3, $cid, PDO::PARAM_INT);
	$q->bindValue(4, $uid, PDO::PARAM_INT);
	$q->execute();
}

// become a member of a community
function community_join($cid, $uid) {
	global $site;
	
	db_begin();
	try {
		$sql = "INSERT INTO community_membership (community, user, join_date) VALUES (?,?, NOW())";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $cid, PDO::PARAM_INT);
		$q->bindValue(2, $uid, PDO::PARAM_INT);
		$q->execute();
		
		$sql = "UPDATE community SET member_count = member_count + 1 WHERE id = ?";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $cid, PDO::PARAM_INT);
		$q->execute();
	} catch (PDOExecption $e) {
		db_die($e->getMessage());
	}
	
	db_commit();
}

function community_list($offset=0, $limit=10) {
	global $site;
	
	try {
		$sql = "SELECT c.*, 
					g.name AS game_name,
					g.name_url AS game_name_url,
					p.name AS player_name,
					p.name_url AS player_name_url
				FROM community c
				LEFT OUTER JOIN game g ON c.game = g.id
				JOIN player p ON p.id = c.creator_player
				ORDER BY name
				LIMIT ? OFFSET ?";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $limit, PDO::PARAM_INT);
		$q->bindValue(2, $offset, PDO::PARAM_INT);
		$q->execute();
		
		while ($r = $q->fetchAll(PDO::FETCH_OBJ)) {
			return $r;
		}
	} catch (PDOException $e) {
		db_die($e->getMessage());
	}
}

function community_lookup($key) {
	global $site;
	
	try {
		$sql = "SELECT *
				FROM community
				WHERE name_url = ?
				LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $key, PDO::PARAM_STR);
		$q->bindValue(2, $key, PDO::PARAM_STR);
		$q->execute();
		
		while ($r = $q->fetch(PDO::FETCH_OBJ)) {
			return $r;
		}
	} catch (PDOException $e) {
		db_die($e->getMessage());
	}
}

// stop being a member of a community
function community_part($cid, $uid) {
	global $site;
	
	db_begin();
	try {
		$sql = "DELETE FROM community_membership WHERE community = ? AND user = ? LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $cid, PDO::PARAM_INT);
		$q->bindValue(2, $uid, PDO::PARAM_INT);
		$q->execute();
		
		$sql = "UPDATE community SET member_count = member_count - 1 WHERE id = ? LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $cid, PDO::PARAM_INT);
		$q->execute();
	} catch (PDOExecption $e) {
		db_die($e->getMessage());
	}
	
	db_commit();
}

function community_setup() {
	global $site;
	
	/*if (!isset($site->get->community)) {
		$site->get->community = $site->settings->community_default;
	}*/
	
	return community_get_by_name($site->args->community);
}

function community_sidebar() {
	global $site;
	$add = (community_is_admin($site->community->id, $site->user->id)) ? "<button class=\"btn btn-sm\"><span class=\"glyphicon glyphicon-plus\"></span></button>" : "";
	$features = feature_list($site->community->features);
?>
	<ul class="nav-list">
<?php foreach ((array) $features as $f) { ?>
		<li><a href="/<?=$site->community->name_url?>/<?=$f->name_url?>"><?=$f->name?></a></li>
<?php } ?>
	</ul>
<?php
}

function community_show_content() {
	global $site;
	
	$content = content_get_list($site->community->id, 0, 5);
	
	if (isset($content->result)) {
		for ($i=0; $i<sizeof($content->result); $i++ ) {
		?>
	<div class="content-item">
		<div class="content-title"><a href="<?=$site->community->name_url?>/<?=$content->result[$i]->name_url?>"><?=$content->result[$i]->name?></a></div>
		<div class="content-byline"><a href="/members/<?=$content->result[$i]->pname?>"><?=$content->result[$i]->pname?></a> // <?=$content->result[$i]->date_added?></div>
		<div class="content-body">
			<?=$content->result[$i]->body?>
		</div>
	</div>
		<?php
		}
	}
}

function community_member_list($c, $mask=0, $offset=0, $limit=25) {
	global $site;
	
	$wheremask = "1";
	
	// select everyone
	if ($mask) {
		$wheremask = " (m.level_mask & ?) > 0";
	}
	
	$sql = "SELECT m.*,
				p.name,
				p.name_url,
				u.id AS user_id
			FROM community_membership m
			JOIN player p ON p.id = m.player
			JOIN user u ON u.id = p.user
			WHERE m.community = ? AND $wheremask
			LIMIT ? OFFSET ?";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $c, PDO::PARAM_INT);
	if ($mask) {
		$q->bindValue(2, $mask, PDO::PARAM_INT);
		$q->bindValue(3, $limit, PDO::PARAM_INT);
		$q->bindValue(4, $offset, PDO::PARAM_INT);
	} else {
		$q->bindValue(2, $limit, PDO::PARAM_INT);
		$q->bindValue(3, $offset, PDO::PARAM_INT);
	}
	$q->execute();
	
	while ($r = $q->fetchAll(PDO::FETCH_OBJ)) {
		return $r;
	}
	
	return null;
}

function community_role_to_string($bitmask) {
	global $site;
	$str = "";
	
	if ($bitmask & COMMUNITY_ADMIN)
		$str .= "admin, ";
	
	if ($bitmask & COMMUNITY_MEMBER)
		$str .= "member, ";
	
	if ($bitmask & COMMUNITY_CONTRIBUTOR)
		$str .= "contributor, ";
	
	if ($bitmask & COMMUNITY_MODERATOR)
		$str .= "moderator, ";
	
	if ($bitmask & COMMUNITY_HELPER)
		$str .= "helper, ";
	
	return substr($str, 0, -2);
}

function community_membership_get($id) {
	global $site;
	if (isset($id) && is_numeric($id)){
		$sql = "SELECT * 
				FROM community_membership
				WHERE id = ? 
				LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $id, PDO::PARAM_INT);
		$q->execute();
		
		while ($r = $q->fetch(PDO::FETCH_OBJ)) {
			return $r;
		}
		
		return null;
	}
}

function community_membership_edit($obj) {
	global $site;
	
	slash($obj);
	
	try {
		db_begin();
		$sql = "UPDATE community_membership SET
					community = ?,
					player = ?,
					level_mask = ?,
					join_date = ?,
					muted = ?, 
					mute_expire_date = ?,
					banned = ?,
					ban_expire_date = ?
				WHERE id = ?
				LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $obj->community, PDO::PARAM_INT);
		$q->bindValue(2, $obj->player, PDO::PARAM_INT);
		$q->bindValue(3, $obj->level_mask, PDO::PARAM_INT);
		$q->bindValue(4, $obj->join_date, PDO::PARAM_STR);
		$q->bindValue(5, $obj->muted, PDO::PARAM_INT);
		$q->bindValue(6, $obj->mute_expire_date, PDO::PARAM_STR);
		$q->bindValue(7, $obj->banned, PDO::PARAM_INT);
		$q->bindValue(8, $obj->ban_expire_date, PDO::PARAM_STR);
		$q->bindValue(9, $obj->id, PDO::PARAM_INT);
		$q->execute();
		
		db_commit();	// may be committed downstream
		return return_obj_success();
	} catch (PDOException $e) {
		db_rollback();
		return return_obj_fail($e->getMessage());
	}
}

function community_membership_add($p) {
	global $site;
	$sql = "INSERT INTO community_membership (
				community, player, level_mask, join_date
			) VALUES (
				?, ?, ?, NOW()
			)";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $p->community, PDO::PARAM_INT);
	$q->bindValue(2, $p->player, PDO::PARAM_INT);
	$q->bindValue(3, $p->level_mask, PDO::PARAM_STR);
	$q->execute();
}

function community_membership_delete($id) {
	global $site;
	$sql = "DELETE FROM community_membership WHERE id = ? LIMIT 1";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $id, PDO::PARAM_INT);
	$q->execute();
}
?>
