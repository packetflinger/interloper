<?php

function discussion_create($obj) {
	global $site;
	
	$sql = "INSERT INTO discussion (
				community, community_group, parent, creator, creation_date, access_key, private, name, name_url
			) VALUES (
				?,?,?,?,NOW(),?,?,?,?
			)";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $obj->community, PDO::PARAM_INT);
	$q->bindValue(2, $obj->group, PDO::PARAM_INT);
	$q->bindValue(3, $obj->parent, PDO::PARAM_INT);
	$q->bindValue(4, $obj->creator, PDO::PARAM_INT);
	$q->bindValue(5, $obj->key, PDO::PARAM_STR);
	$q->bindValue(6, $obj->private, PDO::PARAM_INT);
	$q->bindValue(7, $obj->name, PDO::PARAM_STR);
	$q->bindValue(8, $obj->name_url, PDO::PARAM_STR);
	$q->execute();
}

function discussion_get($id) {
	global $site;
	if (isset($id) && is_numeric($id)){
		$sql = "SELECT * 
				FROM discussion
				WHERE id = ? 
				LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $id, PDO::PARAM_INT);
		$q->execute();
		
		while ($r = $q->fetch(PDO::FETCH_OBJ)) {
			return $r;
		}
		
		return null;
	}
}

function discussion_get_by_key($key) {
	global $site;
	if (isset($key) && $key != ""){
		$sql = "SELECT * 
				FROM discussion
				WHERE access_key = ? 
				LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $key, PDO::PARAM_STR);
		$q->execute();
		
		while ($r = $q->fetch(PDO::FETCH_OBJ)) {
			return $r;
		}
		
		return null;
	}
}

function discussion_list($community, $group) {
	global $site;
	
	$sql = "SELECT *
			FROM discussion
			WHERE community = ?
				AND community_group = ?
			ORDER BY sortorder DESC, 
				name ASC";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $community, PDO::PARAM_INT);
	$q->bindValue(2, $group, PDO::PARAM_INT);
	$q->execute();
	
	$r = $q->fetchAll(PDO::FETCH_OBJ);
	
	return $r;
}

function discussion_topic_add($obj) {
	global $site;
	
	$obj = slash($obj);
	try {
		db_begin();
		$sql = "INSERT INTO discussion_topic (
					discussion, creator, creation_date, access_key, name, name_url, body
				) VALUES (
					?,?,NOW(),?,?,?,?
				)";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $obj->discussion, PDO::PARAM_INT);
		$q->bindValue(2, $obj->creator, PDO::PARAM_INT);
		$q->bindValue(3, $obj->key, PDO::PARAM_STR);
		$q->bindValue(4, $obj->name, PDO::PARAM_STR);
		$q->bindValue(5, $obj->name_url, PDO::PARAM_STR);
		$q->bindValue(6, $obj->body, PDO::PARAM_STR);
		$q->execute();
	
		$sql = "UPDATE discussion SET topic_count = topic_count + 1 WHERE id = ? LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $obj->discussion, PDO::PARAM_INT);
		$q->execute();
		
		db_commit();
	} catch (PDOException $e) {
		db_die($e->getMessage());
	}
}

function discussion_topic_edit($obj) {
	global $site;
	
	$obj = slash($obj);
	try {
		db_begin();
		
		// save the old version
		$sql = "INSERT INTO discussion_topic_edit (
					topic, name, name_url, body, editor, edit_date
				) 
				SELECT id, name, name_url, body, creator, NOW()
				FROM discussion_topic
				WHERE id = ?
				LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $obj->id, PDO::PARAM_INT);
		$q->execute();
		
		$sql = "UPDATE discussion_topic SET
					name = ?,
					name_url = ?,
					body = ?,
					edited = 1, 
					edit_date = NOW(), 
					editor = ? 
				WHERE id = ? 
				LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $obj->name, PDO::PARAM_STR);
		$q->bindValue(2, $obj->name_url, PDO::PARAM_STR);
		$q->bindValue(3, $obj->body, PDO::PARAM_STR);
		$q->bindValue(4, $obj->editor, PDO::PARAM_INT);
		$q->bindValue(5, $obj->id, PDO::PARAM_INT);
		$q->execute();
		
		db_commit();
		return true;
	} catch (PDOException $e) {
		db_die($e->getMessage());
	}
	
	return false;
}

function discussion_topic_get($key) {
	global $site;
	
	$sql = "SELECT t.*,
				u.username
			FROM discussion_topic t
			JOIN user u ON u.id = t.creator
			WHERE access_key = ? 
			LIMIT 1";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $key, PDO::PARAM_STR);
	$q->execute();
	
	while ($r = $q->fetch(PDO::FETCH_OBJ)) {
		$r = unslash($r);
		return $r;
	}
	
	return null;
}

function discussion_topic_list($key, $offset, $length) {
	global $site;
	
	$sql = "SELECT t.*,
				u.username
			FROM discussion_topic t
			JOIN discussion d ON d.id = t.discussion
			JOIN user u ON u.id = t.creator
			WHERE d.access_key = ?
				AND t.removed = 0
			LIMIT ? OFFSET ?";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $key, PDO::PARAM_STR);
	$q->bindValue(2, $length, PDO::PARAM_INT);
	$q->bindValue(3, $offset, PDO::PARAM_INT);
	$q->execute();
	
	$r = $q->fetchAll(PDO::FETCH_OBJ);
	
	return $r;
}


function discussion_topic_remove($obj) {
	global $site;
	
	$sql = "UPDATE discussion_topic SET 
				removed = 1, 
				removed_date = NOW(), 
				remover = ? 
			WHERE id = ? 
			LIMIT 1";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $obj->remover, PDO::PARAM_INT);
	$q->bindValue(2, $obj->id, PDO::PARAM_INT);
	$q->execute();
}

?>