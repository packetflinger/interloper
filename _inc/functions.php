<?php
// generic functions

function activity_add($activity, $target, $target_context, $private=0) {
	global $db, $user, $section;
	
	$sql = "INSERT INTO activity (
				user, section, activity, private, date_added, target, target_context
			) VALUES (
				?, ?, ?, ?, NOW(), ?, ?
			)";
	try {
		$q = $db->prepare($sql);
		$q->bindValue(1, $user->id, PDO::PARAM_INT);
		$q->bindValue(2, $section->id, PDO::PARAM_INT);
		$q->bindValue(3, $activity, PDO::PARAM_INT);
		$q->bindValue(4, $private, PDO::PARAM_INT);
		$q->bindValue(5, $target, PDO::PARAM_INT);
		$q->bindValue(6, $target_context, PDO::PARAM_INT);
		$q->execute();
	} catch (PDOException $e) {
		die_gracefully("<h3>Exception Inserting Activity</h3><p>{$e->getMessage()}</p>");
	}
}

function admin_denied() {
	global $site;
	//include("header.php");
	//navigation();
	die_gracefully("Nope...", "You're account doesn't have access to this");
	//include("footer.php");
	//die();
}

function context_define() {
	global $db;
	$replace = array("-", " ", "/");
	try {
		$sql = "SELECT * FROM context";
		$q = $db->query($sql);
		while ($r = $q->fetch(PDO::FETCH_OBJ)){
			$ctx = strtoupper(str_replace($replace, "_", $r->name));
			define("CTX_$ctx", $r->id);
		}
	} catch (PDOException $e) {
		die_gracefully("<p>Problems defining contexts</p><p><strong>{$e->getMessage()}</strong></p>");
	}
}

// UTC time in the database, converts to user's timezone
function format_date($date, $timetoo = 0, $format = "M j, Y") {
	global $site;
	$changetime = new DateTime($date, new DateTimeZone('UTC'));
	$changetime->setTimezone(new DateTimeZone($site->user->zone));
	$time24 = "H:i";
	$time12 = "g:i a";
	$time = ($site->user->timeformat) ? $time12 : $time24;
	$time = ($timetoo) ? " - " . $time : "";
	return $changetime->format($format . $time);
}

// begin a transaction
function db_begin(){
	global $db;
	if (!isset($GLOBALS['open_transaction']) || (!$GLOBALS['open_transaction'])) {
		$db->beginTransaction();
		$GLOBALS['open_transaction'] = true;
	}
}

// commit a transaction
function db_commit(){
	global $db;
	if (isset($GLOBALS['open_transaction']) && $GLOBALS['open_transaction']) {
		$db->commit();
		$GLOBALS['open_transaction'] = false;
	}
}

function db_connect() {
	global $settings;
	if (!isset($settings)){
		die("Unable to connect to database...");
	}

	try {
		// open a connection
		$db = new PDO(
			"mysql:host={$settings->db_host};dbname={$settings->db_schema};charset=utf8mb4", 
			$settings->db_user, 
			$settings->db_pass
		);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		
		// set default timezone to something other than the system zone
		$q = $db->prepare("SET time_zone = ?");
		$q->bindValue(1, $settings->db_timezone, PDO::PARAM_STR);
		$q->execute();
		
		return $db;
	} catch(PDOException $e) {
		echo "<h2>{$e->getMessage()}</h2>";
		echo "<pre>{$e->getTraceAsString()}</pre>";
	}
}

// die and rollback
function db_die($msg){
	if (isset($GLOBALS['open_transaction']) && $GLOBALS['open_transaction']){
		db_rollback();
		die_gracefully("<h3>Database Problem</h3><p>Transaction rolled back - $msg</p>");
	} else {
		die_gracefully("<h3>Database Problems</h3><p>$msg</p>");
	}
}

function db_error($sql) {
	db_die(mysql_error() . ": <p><strong>$sql</strong></p>");
}

// rollback a failed transaction
function db_rollback(){
	global $db;
	$db->rollBack();
	$GLOBALS['open_transaction'] = false;
}

function die_gracefully($header, $body = ""){
	global $site;
	include("_inc/header.php");
	navigation();
	echo "<div class=\"container\"><div class=\"well\"><h2>$header</h2><p>$body</p></div></div>";
	include("_inc/footer.php");
	die();
}

function die_gracefully_redirect($msg, $url, $delay=2000){
	global $user, $settings;
	include("header.php");
	?>
	<div class="container">
		<div class="well trans"><?=$msg?></div>
	</div>
	<script type="text/javascript">
		setTimeout(function() {
			window.location.href = "<?=$url?>";
		}, <?=$delay?>);
	</script>
	<?php
	include("footer.php");
	die();
}

function error_define() {
	define("ERR_NOPE", "<h3>Nope...</h3><p>nope nope nope</p>");
	define("ERR_NOACCESS", "<h3>Access Denied</h3><p>Your account doesn't have the appropriate permissions to do this.</p>");
	define("ERR_NOTIMPL", "<h3>Error</h3><p>This functionality hasn't be implimented yet.</p>");
	define("ERR_CANTREPORT", "<h3>Can't Report</h3><p>Either you don't have access or this match has already been reported.</p>");
}

function follow_add($ctx, $val){
	global $db, $section, $user;
	if (is_loggedin()){
		try {
			$sql = "INSERT INTO follow (
						user, context, context_value, follow_date
					) VALUES (
						?, ?, ?, NOW()
					)";
			$q = $db->prepare($sql);
			$q->bindValue(1, $user->id, PDO::PARAM_INT);
			$q->bindValue(2, $ctx, PDO::PARAM_INT);
			$q->bindValue(3, $val, PDO::PARAM_INT);
			$q->execute();
			$id = $db->lastInsertId();
			return $id;
		} catch (PDOException $e) {
			return $e->getMessage();
		}
	}
	return 0;
}

function follow_button($context, $id) {
	global $section, $settings;
	$direction = (is_following($context, $id)) ? "Unfollow" : "Follow";
	?>
	<button id="follow" class="btn btn-primary" data-op="<?=$direction?>" data-context="<?=$context?>" data-value="<?=$id?>"><?=$direction?></button>
	<?php
}

function follow_remove($ctx, $val){
	global $db, $section, $user;
	if (is_loggedin()){
		try {
			$sql = "SELECT id
					FROM follow
					WHERE user = ? AND context = ? AND context_value = ?";
			$q = $db->prepare($sql);
			$q->bindValue(1, $user->id, PDO::PARAM_INT);
			$q->bindValue(2, $ctx, PDO::PARAM_INT);
			$q->bindValue(3, $val, PDO::PARAM_INT);
			$q->execute();
			if ($q->rowCount() > 0) {
				$r = $q->fetch(PDO::FETCH_OBJ);
				$sql = "DELETE FROM follow WHERE id = {$r->id} LIMIT 1";
				$db->query($sql);
				return 1;
			}
		} catch (PDOException $e) {
			return $e->getMessage();
		}
		return 0;
	}
}

function email_send($recipient, $msg, $headers) {
        global $settings;
		
        ini_set("include_path", get_include_path() . PATH_SEPARATOR . "{$settings->site_path}/_inc");
        ini_set("include_path", get_include_path() . PATH_SEPARATOR . "{$settings->site_path}/_inc/pear-1.9.2");

        require_once("Mail/Mail.php");
        require_once("Mail/mime.php");
        require_once("Net/SMTP.php");
        require_once("Net/Socket.php");

        $eparams['host'] = $settings->email_smtp;
        $eparams['port'] = $settings->email_port;
        $eparams['localhost'] = $settings->email_ehlo;
        $eparams['persist'] = "0";

        $headers['From'] = $settings->email_from;
        $headers['Date'] = date('r'); // rfc2822 format
		$headers['To'] = $recipient;
		$headers['Return-Path'] = $settings->email_rp;

		$params['eol'] = "\r\n";	// default
		$params['html_encoding'] = "7bit";
		$params['html_charset'] = "utf-8";
        $mime = new Mail_mime($params);
		
		if (isset($msg->html)){
			$mime->setHTMLBody($msg->html);
		}
		
		if (isset($msg->txt)){
			$mime->setTXTBody($msg->txt);
		}
		
		$e_msg = $mime->get();

		$headers1 = $mime->headers($headers);

        $mail =& Mail::factory("smtp",$eparams);
		$mail->send($recipient, $headers1, $e_msg);
}

function generate_key($len=6) {
	$k = uniqid();
	if ($len > strlen($k)) {
		$len = strlen($k);
	}
	
	$key = "";
	for ($i=0; $i<(strlen($k)/2)-1; $i++) {
		$key .= $k[strlen($k) - 1 - $i] . $k[$i];
	}
	
	return substr($key, 0, $len);
}

function get_return_url(){
	return base64_encode($_SERVER["REQUEST_URI"]);
}

function get_user($id){
	global $db;
	if (isset($id) && is_numeric($id)){
		$sql = "SELECT 
					u.*,
					t.zone
				FROM user u
				JOIN timezone t ON t.id = u.timezone
				WHERE u.id = ? 
				LIMIT 1";
		$q = $db->prepare($sql);
		$q->bindValue(1, $id, PDO::PARAM_INT);
		$q->execute();
		if ($q->rowCount() == 1){
			$u = $q->fetch(PDO::FETCH_OBJ);
			return $u;
		}
	}
}

function avatar_get_url($u, $size="100") {
	global $site;
	if (isset($u->avatar) && $u->avatar == "gravatar"){
		$hash = md5(trim(strtolower($u->email)));
		return "https://s.gravatar.com/avatar/$hash?s=$size";
	} else {
		return sprintf("%s/%s_%d.jpg", $site->settings->uri_avatar, $u->avatar, $size);
	}
}

function group_define() {
	global $db;
	$replace = array("-", " ", "/");
	try {
		$sql = "SELECT id, name, level FROM user_group";
		$q = $db->query($sql);
		while ($r = $q->fetch(PDO::FETCH_OBJ)){
			$grp = strtoupper(str_replace($replace, "_", $r->name));
			define("GRP_{$grp}", $r->level);
			define("GRP_{$grp}_ID", $r->id);
		}
	} catch (PDOException $e) {
		die_gracefully("<p>Problems defining groups</p><p><strong>{$e->getMessage()}</strong></p>");
	}
}

function index_main() {
	global $site;
	
	$ctxmain = "{$site->community->context}_index";
	if (function_exists($ctxmain)) {
		$ctxmain();
	} else {
		echo "<h1>No context index defined</h1>";
	}
}

function is_following($ctx, $id){
	global $user, $db;
	$sql = "SELECT id 
			FROM follow
			WHERE user = ?
				AND context = ?
				AND context_value = ?";
	$q = $db->prepare($sql);
	$q->bindValue(1, $user->id, PDO::PARAM_INT);
	$q->bindValue(2, $ctx, PDO::PARAM_INT);
	$q->bindValue(3, $id, PDO::PARAM_INT);
	$q->execute();
	if ($q->rowCount() > 0){
		return true;
	}
	return false;
}

function is_loggedin(){
	global $site;
	if (isset($site->user->id) && user_is_valid_id($site->user->id)){
		return true;
	}
	return false;
}

// return val 2=member/captain, 1=member, 0=nonmember 
function is_teammember($teamobj){
	global $user;
	if (isset($teamobj->id)){
		foreach ($teamobj->members as $member){
			if ($member->user == $user->id){
				if (isset($member->captain)) {
					return 2;
				}
				return 1;
			}
		}
	}
	return 0;
}

function membership_check($group){
	global $user;
	if (is_array($user->membership)){
		for ($i=0; $i<sizeof($user->membership); $i++){
			
		}
	}
	return false;
}

function membership_create($uid, $ctx, $ctx_val, $sec){
	global $db;
	$sql = "INSERT INTO membership (
				user, context, context_value, section
			) VALUES (
				?, ?, ?, ?
			)";
	$q = $db->prepare($sql);
	$q->bindValue(1, $uid, PDO::PARAM_INT);
	$q->bindValue(2, $ctx, PDO::PARAM_INT);
	$q->bindValue(3, $ctx_val, PDO::PARAM_INT);
	$q->bindValue(4, $sec, PDO::PARAM_INT);
	$q->execute();
}

function membership_get($uid){
	global $db;
	$sql = "SELECT id, context, context_value, section
			FROM membership
			WHERE user = ?";
	$q = $db->prepare($sql);
	$q->bindValue(1, $uid, PDO::PARAM_INT);
	$q->execute();
	if ($q->rowCount() > 0){
		return $q->fetchAll(PDO::FETCH_OBJ);
	}
}

function membership_remove($uid, $ctx, $ctx_val){
	global $db;
	$sql = "SELECT id
			FROM membership
			WHERE user = ?
				AND context = ?
				AND context_value = ?
			LIMIT 1";
	$q = $db->prepare($sql);
	$q->bindValue(1, $uid, PDO::PARAM_INT);
	$q->bindValue(2, $ctx, PDO::PARAM_INT);
	$q->bindValue(3, $ctx_val, PDO::PARAM_INT);
	$q->execute();
	if ($q->rowCount() == 1){
		$row = $q->fetch(PDO::FETCH_OBJ);
		$id = $row->id;
		$sql = "DELETE FROM MEMBERSHIP WHERE id = ? LIMIT 1";
		$q = $db->prepare($sql);
		$q->bindValue(1, $id, PDO::PARAM_INT);
		$q->execute();
	}
}

function match_contains($matcharr, $m) {
	foreach ($matcharr AS $match) {
		if ($m->team1 == $match->team1 || $m->team1 == $match->team2) {
			return true;
		}
		
		if ($m->team2 == $match->team1 || $m->team2 == $match->team2) {
			return true;
		}
	}
	
	return false;
}

function navigation(){
	global $site;
	?>
	<nav class="navbar navbar-inverse navbar-static-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/"><?=$site->settings->site_name?></a>
			</div>
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="/communities">Communities</a></li>
					<li><a href="/members">Members</a></li>
				</ul>

				<ul class="nav navbar-nav navbar-right">
<?php 
	if (user_is_logged_in()) { 
		$c = message_unread_count($site->user->id);
		$unread = ($c > 0) ? " <span class=\"badge badge-error\">$c</span>" : "";
?>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?=$site->user->name . $unread?> <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?=$site->settings->uri_msg?>">Inbox<?=$unread?></a></li>
							<li><a href="<?=$site->settings->uri_profile?>">Profile</a></li>
							<li><a href="<?=$site->settings->uri_settings?>">Settings</a></li>
<!--
							<li role="separator" class="divider"></li>
							<li><a href="<?=$site->settings->uri_myteams?>">My Teams</a></li>
-->
							<li role="separator" class="divider"></li>
							<li><a href="<?=$site->settings->uri_logout?>">Signout</a></li>
						</ul>
					</li>
<?php } else { ?>
					<li class=""><a href="<?=$site->settings->uri_login?>">Sign in</a></li>
<?php } ?>
				</ul>
			</div>
		</div>
	</nav>
	<?php
	
}

/*
function parse_args($str, $context=0) {
	$o = new StdClass();
	$o->context = $context;
	$o->league_name = "";
	$o->match_id = 0;
	
	if (!isset($arr[0]))
		return $o;
	
	if ($str[0] == "/")
		$str = substr($str, 1);
	
	if ($str[strlen($str)-1] == "/")
		$str = substr($str, 0, -1);
	
	$arr = explode("/", $str);
	
	
	switch ($context) {
		case CTX_LEAGUE:
			$o->context = CTX_LEAGUE;
			$o->league_name = (isset($arr[0])) ? $arr[0] : "";
			$o->match_id = (isset($arr[1])) ? $arr[1] : 0;
			return $o;
	}
	
	return $arr;
}
*/

// break up the url to figure out what we should do.
// should be in format of /community/feature/feature-specific-stuff
function parse_args() {
	global $site;
	
	$o = new StdClass();
	$o->community = $site->settings->community_default;
	
	if (isset($site->get->args)) {
		$cmdarray = explode("/", $site->get->args);	
		
		// in case args started with a /
		if (sizeof($cmdarray) > 1 && $cmdarray[0] == "")
			array_shift($cmdarray);
		
		// in case args ended with a /
		if (sizeof($cmdarray) > 1 && $cmdarray[sizeof($cmdarray) - 1] == "")
			array_pop($cmdarray);
		
		// community
		if (sizeof($cmdarray) > 0) {
			$o->community = array_shift($cmdarray);
		}
		
		// feature
		if (sizeof($cmdarray) > 0) {
			$o->feature = array_shift($cmdarray);
		}
		
		// feature args
		if (sizeof($cmdarray) > 0) {
			switch ($o->feature) {
			case FEATURE_LEAGUES_URL:
				if (sizeof($cmdarray) > 0){
					$o->feature_cmd = array_shift($cmdarray);
				}
				break;
			}
		}
	}
	
	// override url with query string
	if (isset($site->get->community)) {
		$o->community = $site->get->community;
	}
	
	return $o;
}

// get the user id associated with a player id
function player_to_user($pid) {
	global $site;
	$sql = "SELECT user FROM player WHERE id = ? LIMIT 1";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $pid, PDO::PARAM_INT);
	$q->execute();
	
	while ($r = $q->fetch(PDO::FETCH_OBJ)) {
		return $r->user;
	}
	
	return 0;
}

function permissions_allow($userobj, $context, $id) {
	global $site;
	
}

function policy_delete($id) {
	global $site;
	
	$sql = "DELETE FROM policy WHERE id = ? LIMIT 1";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $id, PDO::PARAM_INT);
	$q->execute();
}

function policy_get($id){
	global $site;

	$sql = "SELECT p.*,
				UNIX_TIMESTAMP(date_created) AS cstamp,
				UNIX_TIMESTAMP(date_updated) AS ustamp
			FROM policy p 
			WHERE id = ?
			LIMIT 1";	
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $id, PDO::PARAM_INT);
	$q->execute();
	
	while ($r = $q->fetch(PDO::FETCH_OBJ)) {
		$sql = "SELECT name AS creator_name, 
					name_url AS creator_name_url 
				FROM player 
				WHERE user = ? 
					AND (community = ? OR community = 0) 
				ORDER BY community DESC LIMIT 1";
		$q2 = $site->db->prepare($sql);
		$q2->bindValue(1, $r->creator, PDO::PARAM_INT);
		$q2->bindValue(2, $r->community, PDO::PARAM_INT);
		$q2->execute();
		while ($r2 = $q2->fetch(PDO::FETCH_OBJ)) {
			$r->creator_name = $r2->creator_name;
			$r->creator_name_url = $r2->creator_name_url;
			return $r;
		}
		return $r;
	}
	
	return null;
}

function policy_get_by_name($name){
	global $site;

	$sql = "SELECT p.*,
				UNIX_TIMESTAMP(date_created) AS cstamp,
				UNIX_TIMESTAMP(date_updated) AS ustamp,
				pl.name AS creator_name,
				pl.name_url AS creator_name_url
			FROM policy p 
			JOIN player pl ON p.creator_player = pl.id
			WHERE p.name_url = ?
			LIMIT 1";	
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $name, PDO::PARAM_STR);
	$q->execute();
	
	while ($r = $q->fetch(PDO::FETCH_OBJ)) {
		return $r;
	}
	
	return null;
}

function policy_view($id){
	global $db, $user, $settings, $section, $get;
	$sql = "SELECT p.*, s.name AS sec_name, s.uri AS sec_uri
			FROM policy p 
			JOIN section s ON s.id = p.section 
			WHERE p.lookup = ? 
			LIMIT 1";
	$q = $db->prepare($sql);
	$q->bindValue(1, $id, PDO::PARAM_STR);
	$q->execute();
	
	include("header.php");
	
	if ($q->rowCount() > 0){
		$r = $q->fetch(PDO::FETCH_OBJ);
		$r = unslash($r);

		$edit_url = "/policy/{$get->id}/{$get->title}/edit";
		$edit = (section_admin(GRP_MODERATOR)) ? " <span class=\"pull-right\"><a class=\"btn btn-primary btn-xs\" href=\"$edit_url\" title=\"edit\"><span class=\"glyphicon glyphicon-edit\"></span></a></span>" : "";
	?>
	<h2>Policy View - <a href="<?=$r->sec_uri?>"><?=$r->sec_name?></a></h2>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 style="margin-top: 0; margin-bottom: 0;"><?=$r->name?><?=$edit?></h3>
		</div>
		<div class="panel-body">
			<?=$r->body?>
		</div>
	</div>
	
	<?php
	}
	include("footer.php");
}

function redirect($url){
	header("Location: $url");
	die();
}

function redirect_return() {
	global $post;
	$url = base64_decode($post->return);
	header("Location: $url");
	die();
}

function redirect_return_msg($msg) {
	global $settings, $post;
	
	$url = base64_decode($post->return);
	die_gracefully_redirect($msg, $url, 2000);
}

function return_obj_fail($str) {
	$o = new StdClass();
	$o->result = "failure";
	$o->value = 0;
	$o->message = $str;
	return $o;
}

function return_obj_success() {
	$o = new StdClass();
	$o->result = "success";
	$o->value = 1;
	return $o;
}

function section_get_admins($section){
	global $db;
	$sql = "SELECT m.id, m.user, m.section, u.level, u.name 
			FROM membership m 
			JOIN user_group u ON u.id = m.context_value 
			WHERE m.context = 11 
				AND (m.section = 0 OR m.section = ?)";
	$q = $db->prepare($sql);
	$q->bindValue(1, $section->id, PDO::PARAM_INT);
	$q->execute();
	if ($q->rowCount() > 0 ){
		return $q->fetchAll(PDO::FETCH_OBJ);
	}
}

function section_get_url($id) {
	global $db;
	
	$sql = "SELECT uri FROM section WHERE id = ? LIMIT 1";
	$q = $db->prepare($sql);
	$q->bindValue(1, $id, PDO::PARAM_INT);
	$q->execute();
	if ($q->rowCount() > 0) {
		$r = $q->fetch(PDO::FETCH_OBJ);
		return $r->uri;
	}
	
	return "/";
}

function section_get_navigation($section){
	
}

// make sure the section uri (from mod_rewrite) starts with / and doesn't end with one
function section_uri_format($s){
	// check for main section
	if ($s == "/"){
		return $s;
	}
	
	// add leading slash if needed
	if (substr($s, 0, 1) != "/"){
		$s = "/" . $s;
	}
	
	// trim trailing slash if needed
	if (substr($s, -1) == "/"){
		$s = substr($s, 0, strlen($s)-1);
	}
	return $s;
}

// get a section object from the name supplied in the url (from mod_rewrite) or ID
function section_setup($s){
	global $db, $user;
	$sec = "";
	$property = "uri";
	if (is_numeric($s)) {
		$sec = $s;
		$property = "id";
	} else if (is_array($s)) {
		$sec = "/" . implode("/", $s);
	} else {
		$sec = section_uri_format($s);	// make sure it starts with /
	}

	$sql = "SELECT s.*, c.name AS context_name
			FROM section s
			JOIN context c ON c.id = s.context
			WHERE s.$property = ? 
			LIMIT 1";
	$q = $db->prepare($sql);
	$q->bindValue(1, $sec, PDO::PARAM_STR);
	$q->execute();
	if ($q->rowCount() == 1){
		$s = $q->fetch(PDO::FETCH_OBJ);
		$s->admins = section_get_admins($s);
		
		if (isset($user->id)){
			$q = $db->query("SELECT user FROM membership WHERE section = {$s->id} AND context = " . CTX_FOLLOW);
			$r = $q->fetch(PDO::FETCH_OBJ);
			$s->member = $r->user;
		}
		return $s;
	} else {
		$tmp = new stdClass();
		$tmp->id = 0;
		$tmp->name = "none";
		return $tmp;
	}
}

function select_values($options, $currentvalue) {
	$buf = "";
	for ($i=0; $i<sizeof($options); $i++) {
		if ($i == $currentvalue) 
			continue;
		
		$buf .= "<option value=\"{$i}\">{$options[$i]}</option>";
	}
	
	$buf = "<option value=\"{$currentvalue}\">{$options[$currentvalue]}</option>" . $buf;
	return $buf;
}

function server_status_get($srv) {
	$server = new StdClass();
	
	$qcmd = "./qcmd";
	$output = `$qcmd $srv->ip $srv->port status`;
	$outarray = explode("\n", $output);
	$cvars = substr($outarray[0], 1);
	$cvararray = explode("\\", $cvars);
	for ($i=0; $i<sizeof($cvararray); $i++) {
		if (($i & 1) == 0) {
			$server->$cvararray[$i] = $cvararray[$i+1];
			$i++;
		}
	}
	
	$players = array();
	for ($i=1; $i < sizeof($outarray); $i++) {
		$p = explode(" ", $outarray[$i]);
		if (sizeof($p) == 3) {
			$player = new StdClass();
			$player->score = $p[0];
			$player->ping = $p[1];
			$player->name = substr($p[2],1, -1);
			$players[] = $player;
		}
	}
	
	$server->playercount = sizeof($players);
	$server->players = $players;
	return $server;
}

function team_add(){
	global $user;
	include("header.php");
	?>
	<h2>Create a Team</h2>
	<div class="well">
		<form role="form" action="/admin" method="post">
			<div class="form-group">
				<label for="name">Full Team Name</label>
				<input class="form-control" type="text" name="name" id="name" placeholder="ex: Anus Annihilators">
			</div>
			<div class="form-group">
				<label for="urlname">URL Name</label>
				<input class="form-control" type="text" name="urlname" id="urlname" placeholder="ex: anus-annihilators">
			</div>
			<div class="form-group">
				<label for="country">Country</label>
				<div class="row">
					<div class="col-xs-6 col-sm-8 col-md-10">
						<select class="form-control" name="country" id="country">
						<?=user_list_countries_select($user->country)?>
						</select>
					</div>
					<div class="col-xs-6 col-sm-4 col-md-2">
						<img id="flag">
					</div>
				</div>
			</div>
			<div class="form-group">
				<button class="btn btn-primary" type="submit">Create Team</button>
			</div>
			<input type="hidden" name="op" value="team.create">
		</form>
	</div>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$.get("<?=$settings->uri_rpc?>?op=GetFlagURL&id=" + $("#country").val(), function(data){
				$("#flag").attr("src", data);
			});
			
			$("#country").change(function() {
				$.get("<?=$settings->uri_rpc?>?op=GetFlagURL&id=" + $("#country").val(), function(data){
					$("#flag").attr("src", data);
				});
			});
		});
	</script>
	<?php
	include("footer.php");
	die();
}



function team_join_request($teamname){
	global $db, $user, $settings, $get;
	try {
		$team = team_get($teamname);
		$caps = team_get_captains($team);
		
		if (isset($get->approve) && is_numeric($get->approve)){
			if ($team->join_key != ((isset($get->key) ? $get->key : 0))){
				die_gracefully("<h3>Approval Failed</h3><p>Invalid join key</p>");
			}
			
			$sql = "SELECT m.*, u.username 
					FROM team_membership m 
					JOIN user u ON u.id = m.user 
					WHERE m.id = ? 
					LIMIT 1";
			$q = $db->prepare($sql);
			$q->bindValue(1, $get->approve, PDO::PARAM_INT);
			$q->execute();
			if ($q->rowCount() == 1){
				$r = $q->fetch(PDO::FETCH_OBJ);
				team_join_approve($team->id, $r->user, $user->id);
				die_gracefully_redirect(
					"<h3>Teammate Approved</h3><p>You approved <b>{$r->username}</b></p>",
					"/teams/{$team->urlname}"
				);
			}
			die();
		}
		db_begin();
		
		
		$id = team_join($team->id, $user->id, 2, 0, 0);
		
		include("_inc/message.php");
		$approve = "<a class=\"btn btn-primary\" href=\"/teams/$teamname/join?approve=$id&key={$team->join_key}\">Approve</a>";
		$msg = "<p><b>{$user->username}</b> has requested to join the team <i>{$team->name}</i>. You are a captain of this team and must approve this request. Ignore this if you don't approve.</p><p>$approve</p>";
		for ($i=0; $i<sizeof($caps); $i++) {
			message_send_($user->id, $caps[$i]->user, "{$team->name} join request", $msg);
		}
		db_commit();
	} catch (PDOException $e) {
		db_die($e->getMessage());
	}
	include("header.php");
	?>
	<div class="well trans">
		<h3>Join Request Submitted</h3>
		<div>Your request to join the team <i><?=$team->name?></i> has been submitted to its captains for approval.</div>
	</div>
	<?php
	include("footer.php");
}



function team_list() {
	global $db, $user, $settings;
	
	$sql = "SELECT t.*, c.flag, DATE_FORMAT(date_created, '%b %D, %Y') AS cdate
			FROM team t
			JOIN country c ON c.id = t.country
			ORDER BY name";
	$q = $db->prepare($sql);
	$q->execute();
	$num = $q->rowCount();
	$newbutton = (is_loggedin()) ? "<span class=\"pull-right\"><a class=\"btn btn-primary\" href=\"{$settings->uri_admin}?op=team.add\"><span class=\"glyphicon glyphicon-plus\"></span> Create One</a></span>" : "";
	include("header.php");
	?>
	<h2>Teams <span class="badge"><?=$num?></span><?=$newbutton?></h2>
	<div class="well">
		<div class="row" style="font-weight: bold;">
			<div class="col-xs-6 col-sm-5 col-md-5">Name</div>
			<div class="col-xs-2 col-sm-3 col-md-3">Members</div>
			<div class="col-xs-4 col-sm-4 col-md-4">Created</div>
		</div>
		<hr>
		<?php
		while ($r = $q->fetch(PDO::FETCH_OBJ)) {
			$r = unslash($r);
		?>
		<div class="row">
			<div class="col-xs-6 col-sm-5 col-md-5"><a href="/teams/<?=$r->urlname?>"><?=$r->name?></a></div>
			<div class="col-xs-2 col-sm-3 col-md-3"><?=$r->members?></div>
			<div class="col-xs-4 col-sm-4 col-md-4"><?=$r->cdate?></div>
		</div>
		<?php } ?>
	</div>
	<?php
	include("footer.php");
}

function team_view($name){
	global $db, $user, $settings;
	include("header.php");
	$team = team_get($name);
	if (!isset($team->id)){
		die_gracefully("<h3>Invalid Team</h3><p>The team <b>$name</b> doesn't exist. Maybe it did at one time, but it doesn't anymore.</p>");
	}
	$joinbutton = (is_loggedin() && !is_teammember($team)) ? "<span class=\"pull-right\"><a class=\"btn btn-primary\" href=\"/teams/{$team->urlname}/join\">Request to Join</a></span>" : "";
	?>
	<h2><?=$team->name?> - Team View <?=$joinbutton?></h2>
	<div class="row">
		<div class="col-xs-5 col-sm-5 col-md-5">
			<div class="well trans">
				Members
				<ul id="team-members">
					<?php foreach ($team->members as $member){ ?>
					<li><a href="<?=$settings->uri_user . "/" . $member->username?>"><img class="small-flag" src="<?=$settings->flag_path . "/" . $member->flag?>" title="<?=$member->cname?>"> <?=$member->username?></a></li>
					<?php } ?>
				</ul>
			</div>
			<div class="well trans">
				<div><img src="<?=$settings->flag_path . "/" . $team->flag?>" title="<?=$team->cname?>"></div>
			</div>
		</div>
		<div class="col-xs-7 col-sm-7 col-md-7">
			<div class="well trans">
				Activity
			</div>
		</div>
	</div>
	<?php
	comment_list($team->id, CTX_TEAM);
	include("footer.php");
}

function team_role_define() {
	global $site;
	$replace = array("-", " ", "/");
	try {
		$sql = "SELECT * FROM team_role";
		$q = $site->db->query($sql);
		$rs = $q->fetchAll(PDO::FETCH_OBJ);
		
		foreach((array) $rs as $r){
			$role = strtoupper(str_replace($replace, "_", $r->name));
			define("TEAM_ROLE_{$role}", $r->value);
		}
		
		// save for later use
		$GLOBALS['roles'] = $rs;
	} catch (PDOException $e) {
		die_gracefully("<p>Problems defining groups</p><p><strong>{$e->getMessage()}</strong></p>");
	}
}

// UTC time in the database, converts to user's timezone
function time_format($time, $timezone, $twelve=false) {
	global $user;
	$changetime = new DateTime($time, new DateTimeZone('UTC'));
	$changetime->setTimezone(new DateTimeZone($timezone));
	//$format24 = "D., M. j, Y \a\\t H:i";
	//$format12 = "D., M. j, Y \a\\t g:i a";
	$format24 = "M j, Y - H:i";
	$format12 = "M j, Y - g:i a";
	$format = ($twelve) ? $format12 : $format24;
	return $changetime->format($format);
}

// recursively add slashes to all text fields
function slash($obj){
	$obj2 = new stdClass();
	foreach($obj as $key => $value) {
		if (is_object($value)){
			$obj2->key = slash($value);
		}
		
		if (is_string($value)){
			$obj2->$key = addslashes($value);
		} else {
			$obj2->$key = $value;
		}
	}
	return $obj2;
}

// recursively remove slashes from all text fields of the object
function unslash($obj){
	$obj2 = new stdClass();
	foreach($obj as $key => $value) {
		if (is_object($value)){
			$obj2->key = unslash($value);
		}
		
		if (is_string($value)) {
			$obj2->$key = stripslashes($value);
		} else {
			$obj2->$key = $value;
		}
	}
	return $obj2;
}

function url($path=""){
	
	return $path;
	
	global $site;
	if (!isset($site->community->name_url)){
		return $path;
	}
	$tmp = sprintf(
		"/%s%s",
		$site->community->name_url,
		$path
	);
	return $tmp;
}

function url_string($str){
	$remove = array(
		"/", "\\", "`", "~", "!", "@", "#", "\$", "%", "^", 
		"&", "*", "(", ")", "+", "=", "?", ",", ".", "<", ">", "'", "\""
	);
	$out = str_replace($remove, "", strtolower($str));
	$replace = array(" ", "_");
	return str_replace($replace, "-", $out);
}





function user_list_countries_select($id){
	global $user, $db;
	$buffer = "";
	$q = $db->query("SELECT * FROM country ORDER BY sortorder DESC, name ASC");
	while ($r = $q->fetch(PDO::FETCH_OBJ)){
		$name = stripslashes($r->name);
		if ($r->id == $id){	// the currently selected one
			$buffer = "<option value=\"{$r->id}\">$name</option>" . $buffer;
		} else {
			$buffer .= "<option value=\"{$r->id}\">$name</option>";
		}
	}
	return $buffer;
}

function user_list_timezones_select($id){
	global $user, $db;
	$buffer = "";
	$q = $db->query("SELECT id,name FROM timezone ORDER BY id ASC");
	while ($r = $q->fetch(PDO::FETCH_OBJ)){
		$name = stripslashes($r->name);
		if ($r->id == $id){	// the currently selected one
			$buffer = "<option value=\"{$r->id}\">$name</option>" . $buffer;
		} else {
			$buffer .= "<option value=\"{$r->id}\">$name</option>";
		}
	}
	return $buffer;
}

function user_get_membership($user_id, $section_id){
	global $db;
	$sql = "SELECT id, context, context_value, section
			FROM membership
			WHERE user = ?
				AND (section = ? OR section = 0)";
	$q = $db->prepare($sql);
	$q->bindValue(1, $user_id, PDO::PARAM_INT);
	$q->bindValue(2, $section_id, PDO::PARAM_INT);
	$q->execute();
	if ($q->rowCount() > 0){
		return $q->fetchAll(PDO::FETCH_OBJ);
	}
}

function user_change_emailaddr_email($id){
	global $settings;
	$u = get_user($id);
	$headers['subject'] = "Confirm Email Address Change";
	$msg = "A request to change email addresses to this one ({$u->email_tmp}) was made at q2players.org. Click the link below to confirm this change:\n\n";
	$msg .= "{$settings->site_url}/changeemail/{$u->id}/{$u->reset_token}\n\n";
	$msg .= "If you don't wish to change emails or did this in error, just ignore this message.";
	
	email_send($u->email_tmp, $msg, $headers);
}

function user_lostpassword_email($id){
	global $settings;
	$u = get_user($id);
	$headers['subject'] = "Password Reset";
	include_once("_inc/mail_template/msg_lostpass.php");
	email_send($u->email, $msg, $headers);
}

function user_join_email($id){
	global $settings;
	$u = get_user($id);
	$headers['subject'] = "Welcome to {$settings->site_name} - Continue Signing Up...";
	include_once("_inc/mail_template/msg_join.php");
	email_send($u->email, $msg, $headers);
}

function user_make_anchor($name, $classes=""){
	global $settings;
	return "<a class=\"$classes\" href=\"{$settings->uri_user}/$name\">$name</a>";
}

function user_message_email($from, $recip, $messageid){
	global $settings, $user;
	$fr = get_user($from);
	$to = get_user($recip);
	$headers['subject'] = "New message from {$fr->username}";
	include_once("_inc/mail_template/msg_message.php");
	email_send($to->email, $msg, $headers);
}

function user_require_auth(){
	global $user, $settings;
	if (isset($user->id) && user_is_valid_id($user->id)){
		return;
	} else {
		redirect($settings->uri_login . "?r=" .get_return_url());
	}
}


function user_setup(){
	global $settings;
	$user = new stdClass();
	if (isset($_SESSION['uid']) && $_SESSION['uid'] > 0) {
		
		$user = user_get($_SESSION['uid']);
		if (isset($user->id)){
			unset($user->password);
			//$user->membership = user_get_membership($user->id, $section->id);
			return $user;
		}
	}
	
	// no user logged in, use defaults
	$user->id = -1;
	$user->username = "Anonymous";
	$user->muted = 1;
	$user->language = $settings->user_default_lang;
	$user->zone = $settings->user_default_tz;
	$user->timeformat = 0;
	$user->player_id = 0;
	return $user;
}


function user_to_player($uid, $cid) {
	global $site;

	$sql = "SELECT * 
			FROM player 
			WHERE user = ? 
				AND (community = ? OR community = 0)
			ORDER BY community DESC";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $uid, PDO::PARAM_INT);
	$q->bindValue(2, $cid, PDO::PARAM_INT);
	$q->execute();
	
	while ($r = $q->fetch(PDO::FETCH_OBJ)) {
		return $r;
	}
	
	return 0;
}

function username_to_urlname($username) {
	$out = strtolower($username);
	$out = str_replace(" ", "-", $out);
}

function name_to_urlname($name) {
	$z = strtolower($name);
    $z = preg_replace('/[^a-z0-9 -]+/', '', $z);
    $z = str_replace(' ', '-', $z);
    return trim($z, '-');
}
?>
