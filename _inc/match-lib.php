<?php

function match_create($o) {
	global $site;
	
	if (($id = match_exists($o)) > 0) {
		$m = match_get($id);
		$m->team_home = $o->team_home;
		$m->team_away = $o->team_away;
		$m->round = $o->round;
		match_edit($m);
		return;
	}
	
	$sql = "INSERT INTO league_match (
				access_key, league_id, group_id, 
				round, team_home, team_away, 
				team_type, date_created, creator
			) VALUES (
				?, ?, ?,
				?, ?, ?, 
				?, NOW(), ?
			)";
	try {
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $o->access_key, PDO::PARAM_STR);
		$q->bindValue(2, $o->league_id, PDO::PARAM_INT);
		$q->bindValue(3, $o->group_id, PDO::PARAM_INT);
		
		$q->bindValue(4, $o->round, PDO::PARAM_INT);
		$q->bindValue(5, $o->team_home, PDO::PARAM_INT);
		$q->bindValue(6, $o->team_away, PDO::PARAM_INT);
		
		$q->bindValue(7, $o->team_type, PDO::PARAM_INT);
		$q->bindValue(8, $o->creator, PDO::PARAM_INT);
		
		$q->execute();
		
		$r = return_obj_success();
		$r->match_id = $site->db->lastInsertId();

		return $r; 
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}

function match_delete($id) {
	global $site;
	
	$sql = "DELETE FROM league_match WHERE id = ? LIMIT 1";
	
	try {
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $id, PDO::PARAM_INT);
		$q->execute();

		$r = return_obj_success();
		return $r; 
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}

function match_edit($o) {
	global $site;
	
	$sql = "UPDATE league_match SET
				access_key = ?, league_id = ?, group_id = ?, round = ?,
				team_home = ?, team_away = ?, winner = ?, forfeit = ?,
				played = ?, date_played = ?, scheduled = ?, date_scheduled = ?,
				forced = ?, date_forced = ?, forcedby = ?, date_created = ?,
				creator = ?, server = ?, description = ?, team_type = ?
			WHERE id = ?
			LIMIT 1";
	
	try {
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $o->access_key, PDO::PARAM_STR);
		$q->bindValue(2, $o->league_id, PDO::PARAM_INT);
		$q->bindValue(3, $o->group_id, PDO::PARAM_INT);
		$q->bindValue(4, $o->round, PDO::PARAM_INT);
		
		$q->bindValue(5, $o->team_home, PDO::PARAM_INT);
		$q->bindValue(6, $o->team_away, PDO::PARAM_INT);
		$q->bindValue(7, $o->winner, PDO::PARAM_INT);
		$q->bindValue(8, $o->forfeit, PDO::PARAM_INT);
		
		$q->bindValue(9, $o->played, PDO::PARAM_INT);
		$q->bindValue(10, $o->date_played, PDO::PARAM_STR);
		$q->bindValue(11, $o->scheduled, PDO::PARAM_INT);
		$q->bindValue(12, $o->date_scheduled, PDO::PARAM_STR);
		
		$q->bindValue(13, $o->forced, PDO::PARAM_INT);
		$q->bindValue(14, $o->date_forced, PDO::PARAM_STR);
		$q->bindValue(15, $o->forcedby, PDO::PARAM_INT);
		$q->bindValue(16, $o->date_created, PDO::PARAM_STR);
		
		$q->bindValue(17, $o->creator, PDO::PARAM_INT);
		$q->bindValue(18, $o->server, PDO::PARAM_STR);
		$q->bindValue(19, $o->description, PDO::PARAM_STR);
		$q->bindValue(20, $o->team_type, PDO::PARAM_INT);
		
		$q->bindValue(21, $o->id, PDO::PARAM_INT);
		
		$q->execute();
		
		$r = return_obj_success();

		return $r; 
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}

function match_get($id) {
	global $site;
	
	try {
		$sql = "SELECT * FROM league_match WHERE id = ? LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $id, PDO::PARAM_INT);
		$q->execute();
		
		while ($r = $q->fetch(PDO::FETCH_OBJ)) {
			return $r;
		}
		
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}

/*
function match_get_by_key($k) {
	global $site;
	
	try {
		$sql = "SELECT * FROM league_match WHERE access_key = ? LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $k, PDO::PARAM_STR);
		$q->execute();
		
		while ($r = $q->fetch(PDO::FETCH_OBJ)) {
			return $r;
		}
		
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}
*/

function match_get_by_key($key) {
	global $site;
	
	try {
		$sql = "SELECT m.*, 
					p1.name AS player_home_name, 
					p2.name AS player_away_name,
					t1.name AS team_home_name,
					t2.name AS team_away_name,
					'' AS name_home,
					'' AS name_away
				FROM league_match m
				LEFT OUTER JOIN player p1 ON p1.id = m.team_home
				LEFT OUTER JOIN player p2 ON p2.id = m.team_away
				LEFT OUTER JOIN team t1 ON t1.id = m.team_home
				LEFT OUTER JOIN team t2 ON t2.id = m.team_away
				WHERE m.access_key = ?";
		
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $key, PDO::PARAM_STR);
		$q->execute();
		
		while ($r = $q->fetch(PDO::FETCH_OBJ)) {
			if ($r->team_type == LEAGUE_TEAM_TYPE_PLAYER) {
				$r->name_home = $r->player_home_name;
				$r->name_away = $r->player_away_name;
			} else {
				$r->name_home = $r->team_home_name;
				$r->name_away = $r->team_away_name;
			}
			return $r;
		}
		
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}
function match_get_with_names($id, $type) {
	global $site;
	
	try {
		if ($type == LEAGUE_TEAM_TYPE_PLAYER) {
			$sql = "SELECT m.*, p1.name AS team_home_name, p2.name AS team_away_name  
					FROM league_match m 
					JOIN player p1 ON p1.id = m.team_home
					JOIN player p2 ON p2.id = m.team_away
					WHERE m.id = ? 
					LIMIT 1";
		} else {
			$sql = "SELECT m.*, p1.name AS team_home_name, p2.name AS team_away_name  
					FROM league_match m 
					JOIN team p1 ON p1.id = m.team_home
					JOIN team p2 ON p2.id = m.team_away
					WHERE m.id = ? 
					LIMIT 1";
		}
		
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $id, PDO::PARAM_INT);
		$q->execute();
		
		while ($r = $q->fetch(PDO::FETCH_OBJ)) {
			return $r;
		}
		
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}

function match_autocreate($group_id) {
	global $site;
	
	$group = group_get($group_id);
	$league = league_get($group->league);
	$gm = group_get_members($group->id);
	$num = sizeof($gm);
	
	if ($num < 2) {
		redirect_return();
	}
	
	$round_total = ($num % 2 == 0) ? $num - 1 : $num;
	$total_per_round = floor($num / 2);
	
	
	// build array of all possible matches
	$matches = array();
	$i = 0;
	foreach ($gm as $player1) {
		foreach ($gm as $player2) {
			if ($player1->member_id == $player2->member_id) {
				continue;
			}

			$match = new StdClass();
			$match->team_home = $player1->member_id;
			$match->team_away = $player2->member_id;
			if (!match_repeat($matches, $match)) {
				$match = ($i % 2 == 0) ? $match : match_swap($match);
				$matches[] = $match;
			}
			$i++;
		}
	}

	$rounds = null;
	
	// keep trying until we can fit all the matches
	while ($rounds == null) {
		shuffle($matches);
		
		// init the array
		$rounds = array();
		for ($i=0; $i<$round_total; $i++) {
			$rounds[$i] = array();
		}
		
		// create rounds
		foreach ($matches as $m) {
			$rounds = match_to_round($rounds, $m);
		}
	}
	
	// create the matches
	$match_slug = new StdClass();
	$match_slug->league_id = $group->league;
	$match_slug->group_id = $group->id;
	$match_slug->creator = 0;
	$match_slug->team_type = $league->team_type;
	
	for ($i=0; $i<$round_total; $i++) {
		for ($j=0; $j<sizeof($rounds[$i]); $j++){
			$m = $rounds[$i][$j];
			
			$match_slug->access_key = generate_key();
			$match_slug->team_home = $m->team_home;
			$match_slug->team_away = $m->team_away;
			$match_slug->round = $i+1;
			
			$match_slug = ($j % 2 == 0) ? $match_slug : match_swap($match_slug);
			
			match_create($match_slug);
		}
	}
}

function match_swap($match) {
	$tmp = $match->team_home;
	$match->team_home = $match->team_away;
	$match->team_away = $tmp;
	return $match;
}

// checks of supplied matches have same teams/players even in different positions
function match_repeat($matcharr, $m) {

	foreach ($matcharr as $match) {
		if ($m->team_home == $match->team_home && $m->team_away == $match->team_away) {
			return true;
		}

		if ($m->team_home == $match->team_away && $m->team_away == $match->team_home) {
			return true;
		}
	}

	return false;
}

function match_to_round($rounds, $m) {
	for ($i=0; $i<sizeof($rounds); $i++) {
		$round = $rounds[$i];

		if (sizeof($round) == 0) {
			$m->round = $i;
			$rounds[$i][] = $m;
			return $rounds;
		}

		if (!match_players_in_round($round, $m)) {
			$m->round = $i;
			$rounds[$i][] = $m;
			return $rounds;
		}
	}

	// a match couldn't be placed, have parent reshuffle and try again
	return null;
}

function match_players_in_round($round, $match) {
	foreach ($round as $m) {
		if ($m->team_home == $match->team_home || $m->team_home == $match->team_away) {
			return true;
		}

		if ($m->team_away == $match->team_home || $m->team_away == $match->team_away) {
			return true;
		}
	}

	return false;
}

function match_exists($o) {
	global $site;
	
	$sql = "SELECT id 
			FROM league_match
			WHERE group_id = ?
				AND (team_home = ? OR team_away = ?)
				AND (team_away = ? OR team_home = ?)
			LIMIT 1";
	
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $o->group_id, PDO::PARAM_INT);
	$q->bindValue(2, $o->team_home, PDO::PARAM_INT);
	$q->bindValue(3, $o->team_home, PDO::PARAM_INT);
	$q->bindValue(4, $o->team_away, PDO::PARAM_INT);
	$q->bindValue(5, $o->team_away, PDO::PARAM_INT);
	$q->execute();
	
	while ($r = $q->fetch(PDO::FETCH_OBJ)) {
		return $r->id;
	}
	
	return 0;
}

function match_list($group_id, $league_type, $offset = 0, $limit = 50) {
	global $site;
	
	try {
		if ($league_type == LEAGUE_TEAM_TYPE_PLAYER) {
			$sql = "SELECT m.*, p1.name AS team_home_name, p2.name AS team_away_name 
					FROM league_match m
					JOIN player p1 ON p1.id = m.team_home
					JOIN player p2 ON p2.id = m.team_away
					WHERE m.group_id = ? 
					ORDER BY m.round ASC, m.played
					LIMIT ? 
					OFFSET ?";
		} else {
			$sql = "SELECT m.*, p1.name AS team_home_name, p2.name AS team_away_name 
					FROM league_match m
					JOIN team p1 ON p1.id = m.team_home
					JOIN team p2 ON p2.id = m.team_away
					WHERE m.group_id = ? 
					ORDER BY m.round ASC, m.played
					LIMIT ? 
					OFFSET ?";
		}
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $group_id, PDO::PARAM_INT);
		$q->bindValue(2, $limit, PDO::PARAM_INT);
		$q->bindValue(3, $offset, PDO::PARAM_INT);
		$q->execute();
		
		while ($r = $q->fetchAll(PDO::FETCH_OBJ)) {
			return $r;
		}
		
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}

function match_swap_teams($match_id) {
	global $site;
	
	$m = match_get($match_id);
	
	$tmp = $m->team_home;
	$m->team_home = $m->team_away;
	$m->team_away = $tmp;
	
	match_edit($m);
}
?>