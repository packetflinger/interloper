<?php

function team_create($obj){
	global $site;
	$post = slash($site->post);
	$sql = "INSERT INTO team (
				name, name_url, creator, creator_player, date_created, join_key, country
			) VALUES (
				?, ?, ?, NOW(), ?, ?
			)";
	try {
		db_begin();
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $post->name, PDO::PARAM_STR);
		$q->bindValue(2, $post->urlname, PDO::PARAM_STR);
		$q->bindValue(3, $site->user->id, PDO::PARAM_INT);
		$q->bindValue(4, $post->player, PDO::PARAM_INT);
		$q->bindValue(5, uniqid(), PDO::PARAM_STR);
		$q->bindValue(6, $post->country, PDO::PARAM_INT);
		$q->execute();
		$teamid = $site->db->lastInsertId();
		
		team_join($teamid, $post->player, 1, 1, 1);
		
		db_commit();
	} catch (PDOException $e) {
		db_die($e->getMessage());
	}
}

function team_get($name){
	global $site;
	try {
		$sql = "SELECT t.*, c.flag, c.name AS country_name
				FROM team t 
				JOIN country c ON c.id = t.country 
				WHERE t.name_url = ? 
				LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $name, PDO::PARAM_INT);
		$q->execute();
		while ($r = $q->fetch(PDO::FETCH_OBJ)) {
			$m = team_get_member_list($r->id);
			if (sizeof($m) > 0){
				$r->members = $m;
			}
			return unslash($r);
		}
		throw new Exception("Unknown Team");
	} catch (PDOException $e) {
		db_die($e->getMessage());
	} catch (Exception $e) {
		db_die($e->getMessage());
	}
}

function team_edit($o) {
	global $site;
	$sql = "UPDATE team SET
				name = ?, name_url = ?, creator = ?, creator_player = ?, 
				date_created = ?, join_key = ?, member_count = ?, country = ?
			WHERE id = ?
			LIMIT 1";
	try {
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $o->name, PDO::PARAM_STR);
		$q->bindValue(2, $o->name_url, PDO::PARAM_STR);
		$q->bindValue(3, $o->creator, PDO::PARAM_INT);
		$q->bindValue(4, $o->creator_player, PDO::PARAM_INT);
		$q->bindValue(5, $o->date_created, PDO::PARAM_STR);
		$q->bindValue(6, $o->join_key, PDO::PARAM_STR);
		$q->bindValue(7, $o->member_count, PDO::PARAM_INT);
		$q->bindValue(8, $o->country, PDO::PARAM_INT);
		$q->bindValue(9, $o->id, PDO::PARAM_INT);
		$q->execute();
		
		$r = return_obj_success();

		return $r; 
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}

function team_membership_get($id){
	global $site;
	try {
		$sql = "SELECT m.*, 
					p.name AS player_name, 
					p.name_url AS player_name_url,
					t.name AS team_name,
					t.name_url AS team_name_url
				FROM team_membership m  
				JOIN player p ON p.id = m.player
				JOIN team t ON t.id = m.team
				WHERE m.id = ? 
				LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $id, PDO::PARAM_INT);
		$q->execute();
		while ($r = $q->fetch(PDO::FETCH_OBJ)) {
			return $r;
		}
		throw new Exception("Unknown membership");
	} catch (PDOException $e) {
		db_die($e->getMessage());
	} catch (Exception $e) {
		db_die($e->getMessage());
	}
}

function team_membership_edit($o) {
	global $site;
	$sql = "UPDATE team_membership SET
				team = ?, player = ?, role = ?, join_date = ?, 
				approved = ?, approved_by = ?, approved_date = ?
			WHERE id = ?
			LIMIT 1";
	try {
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $o->team, PDO::PARAM_INT);
		$q->bindValue(2, $o->player, PDO::PARAM_INT);
		$q->bindValue(3, $o->role, PDO::PARAM_INT);
		$q->bindValue(4, $o->join_date, PDO::PARAM_STR);
		$q->bindValue(5, $o->approved, PDO::PARAM_INT);
		$q->bindValue(6, $o->approved_by, PDO::PARAM_INT);
		$q->bindValue(7, $o->approved_date, PDO::PARAM_STR);
		$q->bindValue(8, $o->id, PDO::PARAM_INT);
		$q->execute();
		
		$r = return_obj_success();

		return $r; 
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}

function team_roles_get(){
	global $site;
	try {
		$sql = "SELECT * FROM team_role";
		$q = $site->db->prepare($sql);
		$q->execute();
		while ($r = $q->fetchAll(PDO::FETCH_OBJ)) {
			return $r;
		}
	} catch (PDOException $e) {
		db_die($e->getMessage());
	} catch (Exception $e) {
		db_die($e->getMessage());
	}
}

function team_get_captains($teamobj){
	global $site;
	if (isset($teamobj->id)){
		$sql = "SELECT player FROM team_membership WHERE team = ? AND captain = 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $teamobj->id, PDO::PARAM_INT);
		$q->execute();
		if ($q->rowCount() > 0) {
			$r = $q->fetchAll(PDO::FETCH_OBJ);
			return $r;
		}
	}
}

function team_get_list($name = "", $offset = 0, $length = 20) {
	global $site;
	
	$sql = "SELECT t.*, c.flag, c.name AS country_name
			FROM team t
			JOIN country c ON t.country = c.id
			ORDER BY t.name ASC, t.name_url ASC
			LIMIT ? 
			OFFSET ?";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $length, PDO::PARAM_INT);
	$q->bindValue(2, $offset, PDO::PARAM_INT);
	$q->execute();
	
	while ($r = $q->fetchAll(PDO::FETCH_OBJ)) {
		return $r;
	}
}

function team_get_member_list($id){
	global $site;
	$sql = "SELECT 
				m.*, 
				p.name AS playername,
				p.name_url AS playername_url,
				/* r.name AS rolename, */ 
				c.flag, 
				c.name AS cname
			FROM team_membership m
			/* JOIN team_role r ON m.role = r.id */
			JOIN player p ON m.player = p.id
			JOIN user u ON u.id = p.user
			JOIN country c ON c.id = u.country
			WHERE m.team = ? 
				AND m.approved = 1";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $id, PDO::PARAM_INT);
	$q->execute();
	
	$r = $q->fetchAll(PDO::FETCH_OBJ);
	return $r;
}

function team_is_member($tid, $pid) {
	global $site;
	
	$sql = "SELECT role FROM team_membership WHERE team = ? AND player = ? LIMIT 1";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $tid, PDO::PARAM_INT);
	$q->bindValue(2, $pid, PDO::PARAM_INT);
	$q->execute();
	
	while ($r = $q->fetch(PDO::FETCH_OBJ)) {
		return $r->role;
	}
	
	return 0;
}

// role 2 is player
function team_join($teamid, $playerid, $role=2, $approved=1, $approver=0){
	global $site;
	
	db_begin();
	$sql = "INSERT INTO team_membership (
				team, player, role, join_date
			) VALUES (
				?, ?, ?, NOW()
			)";
	try {
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $teamid, PDO::PARAM_INT);
		$q->bindValue(2, $playerid, PDO::PARAM_INT);
		$q->bindValue(3, $role, PDO::PARAM_INT);
		$q->execute();
		$id = $site->db->lastInsertId();
		
		team_update_member_count($teamid);
		
		if ($approved){
			team_join_approve($teamid, $playerid, $approver);
		}
		
		db_commit();
		return id;
		
	} catch (PDOException $e) {
		db_die($e->getMessage());
	}
	
	return null;
}

function team_join_approve($teamid, $playerid, $approver){
	global $site;
	
	try {
		$sql = "SELECT id FROM team_membership WHERE team = ? AND player = ? LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $teamid, PDO::PARAM_INT);
		$q->bindValue(2, $playerid, PDO::PARAM_INT);
		$q->execute();
		if ($q->rowCount() > 0){
			$r = $q->fetch(PDO::FETCH_OBJ);
			$sql = "UPDATE team_membership SET
						approved = 1,
						approved_by = ?, 
						approved_date = NOW()
					WHERE id = ?
					LIMIT 1";
			$q = $site->db->prepare($sql);
			$q->bindValue(1, $approver, PDO::PARAM_INT);
			$q->bindValue(2, $r->id, PDO::PARAM_INT);
			$q->execute();	
		}
		return true;
	} catch (PDOException $e) {
		db_die($e->getMessage());
	}
	
	return false;
}

function team_update_member_count($teamid) {
	global $site;
	
	$sql = "SELECT COUNT(id) AS total FROM team_membership WHERE team = ?";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $teamid, PDO::PARAM_INT);
	$q->execute();
	$r = $q->fetch(PDO::FETCH_OBJ);
		
	$sql = "UPDATE team SET member_count = ? WHERE id = ? LIMIT 1";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $r->total, PDO::PARAM_INT);
	$q->bindValue(2, $teamid, PDO::PARAM_INT);
	$q->execute();
}


function team_part($teamid, $playerid){
	global $site;
	
	try {
		db_begin();
		$sql = "SELECT id FROM team_membership WHERE team = ? AND player = ? LIMIT 1";
	
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $teamid, PDO::PARAM_INT);
		$q->bindValue(2, $playerid, PDO::PARAM_INT);
		$q->execute();
		
		while ($r = $q->fetch(PDO::FETCH_OBJ)) {
			$sql = "DELETE FROM team_membership WHERE id = ? LIMIT 1";
			$qd = $site->db->prepare($sql);
			$qd->bindValue(1, $r->id, PDO::PARAM_INT);
			$qd->execute();
			
			team_update_member_count($teamid);
			
			db_commit();
			return true;
		}
		db_commit();
		return false;
		
	} catch (PDOException $e) {
		db_rollback();
		return false;
	}
}

function team_is_captain($teamobj, $player_id) {
	if (!isset($teamobj->members)) {
		return false;
	}
	
	foreach ((array) $teamobj->members as $member) {
		if (($member->role & TEAM_ROLE_CAPTAIN) && $member->player == $player_id) {
			return true;
		}
	}
	
	return false;
}

function team_role_to_string($bitmask) {
	global $site;
	$str = "";
	
	foreach ((array) $GLOBALS['roles'] as $role) {
		if ($bitmask & (int)$role->value) {
			$str .= $role->name . ", ";
		}
	}
	
	return substr($str, 0, -2);
}


function team_index() {
	global $site;
	$teams = team_get_list();
	include_once("_inc/header.php");
	navigation();
?>
<div class="container">
	<h1 class="header-title">Teams 
		<div class="pull-right">
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#new-team">Create</button>
		</div>
	</h1>
	<div class="well">
		<div class="row">
			<div class="col-md-4">Team Name</div>
			<div class="col-md-4">Nationality</div>
			<div class="col-md-4">Members</div>
		</div>
	<?php foreach ((array) $teams as $team) { ?>
		<div class="row">
			<div class="col-md-4"><a href="<?=$site->settings->uri_team?>/<?=$team->name_url?>"><?=$team->name?></a></div>
			<div class="col-md-4"><img class="flag" src="<?=$site->settings->flag_path?>/<?=$team->flag?>" title="<?=$team->country_name?>"></div>
			<div class="col-md-4"><?=$team->member_count?></div>
		</div>
	<?php } ?>
	</div>
</div>

<div id="new-team" class="modal fade">
	<form role="form" action="" method="post">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Create a team</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" name="createteam">Create</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
		<input type="hidden" name="context" value=""/>
		<input type="hidden" name="id" value=""/>
		<input type="hidden" name="return" value="L2Fzc2V0LnBocD9vcD1wcm9maWxlLm1hbmFnZSZpZD0zMw=="/>
	</form>
</div>
		
<?php
	include_once("_inc/footer.php");
	die();
}

function team_home($t) {
	global $site;
	
	include_once("_inc/header.php");
	navigation();
?>
<div class="container">
	<h1 class="header-title">Team - <?=$t->name?> <img class="flag" title="<?=$t->country_name?>" src="<?=$site->settings->flag_path?>/<?=$t->flag?>">
<?php if (user_is_logged_in() && team_is_captain($t, $site->user->player_id)) { ?>
		<div class="pull-right">
			<a href="<?=sprintf("%s/%s/admin", $site->settings->uri_team, $t->name_url)?>" title="Team Settings"><span class="glyphicon glyphicon-cog"></span></a>
		</div>
<?php } ?>
	</h1>
	<div class="well">
		<div class="row">
			<div class="col-header col-xs-4 col-sm-3 col-md-2 col-lg-2 col-xl-2">Player</div>
			<div class="col-header col-xs-8 col-sm-9 col-md-10 col-lg-10 col-xl-10">Role</div>
		</div>
<?php foreach ((array) $t->members as $member) { ?>
		<div class="row">
			<div class="col-xs-4 col-sm-3 col-md-2 col-lg-2 col-xl-2"><a href="<?=sprintf("%s/%s", $site->settings->uri_user, $member->playername_url)?>"><?=$member->playername?></a> <img class="flag" title="<?=$member->cname?>" src="<?=sprintf("%s/%s", $site->settings->flag_path, $member->flag)?>"></div>
			<div class="col-xs-8 col-sm-9 col-md-10 col-lg-10 col-xl-10"><?=team_role_to_string($member->role)?></div>
		</div>
<?php } ?>	
	</div>
</div>

<div id="new-team" class="modal fade">
	<form role="form" action="" method="post">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Create a team</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" name="createteam">Create</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
		<input type="hidden" name="context" value=""/>
		<input type="hidden" name="id" value=""/>
		<input type="hidden" name="return" value="L2Fzc2V0LnBocD9vcD1wcm9maWxlLm1hbmFnZSZpZD0zMw=="/>
	</form>
</div>
		
<?php
	include_once("_inc/footer.php");
	die();
}

function team_admin($t) {
	global $site;

	if (!(user_is_logged_in() && team_is_captain($t, $site->user->player_id))) {
		redirect(sprintf("%s/%s", $site->settings->uri_team, $t->name_url));
	}
	
	include_once("_inc/header.php");
	navigation();
?>
<div class="container">
	<h1 class="header-title">Team Settings - <?=$t->name?> <img class="flag" title="<?=$t->country_name?>" src="<?=$site->settings->flag_path?>/<?=$t->flag?>"></h1>
	<div class="well">
		<button class="btn btn-primary" data-toggle="modal" data-target="#rename-team">Change team name/url</button>
		<button class="btn btn-primary" data-toggle="modal" data-target="#team-avatar">Team Avatar</button>
		<hr>
		<form action="/team.php" method="post">
			<div class="form-group">
				<div class="row">
					<div class="col-header col-xs-4 col-sm-3 col-md-2 col-lg-2 col-xl-2">Player</div>
					<div class="col-header col-xs-8 col-sm-9 col-md-10 col-lg-10 col-xl-10">Role</div>
				</div>
<?php foreach ((array) $t->members as $member) { ?>
				<div class="row">
					<div class="col-xs-4 col-sm-3 col-md-2 col-lg-2 col-xl-2"><input type="checkbox" name="members[]" value="<?=$member->player?>"> <a href="<?=sprintf("%s/%s", $site->settings->uri_user, $member->playername_url)?>"><?=$member->playername?></a> <img class="flag" title="<?=$member->cname?>" src="<?=sprintf("%s/%s", $site->settings->flag_path, $member->flag)?>"></div>
					<div class="col-xs-8 col-sm-9 col-md-10 col-lg-10 col-xl-10"><?=team_role_to_string($member->role)?> <a onclick="loadRoles('<?=$member->id?>')" href="#team-roles" data-toggle="modal"><span class="glyphicon glyphicon-edit"></span></a></div>
				</div>
<?php } ?>	
			</div>
			<div class="form-group">
				<button type="submit" name="removemembers" value="1" class="btn btn-xs btn-warning">Remove</button>
			</div>
			<input type="hidden" name="op" value="team.members.update">
			<input type="hidden" name="team" value="<?=$t->name_url?>">
			<input type="hidden" name="return" value="<?=get_return_url()?>">
		</form>
		
		<form action="/team.php" method="post">
			<div class="form-group">
				<div class="row">
					<div class="col-xs-8 col-sm-8 col-md-8 col-lg-9 col-xl-9">
						<input id="addmember" class="form-control" name="members" type="text" placeholder="Add Players"/>
					</div>
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-3 col-xl-3">
						<button class="btn btn-warning" type="submit">Add</button>
					</div>
				</div>
			</div>
			
			<input type="hidden" name="op" value="team.members.add">
			<input type="hidden" name="team" value="<?=$t->name_url?>">
			<input type="hidden" name="return" value="<?=get_return_url()?>">
		</form>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function($) {
		$("#addmember").magicSuggest({
			allowFreeEntries: false,
			data: '/async',
			dataUrlParams: {op: 'NameLookup'},
			hideTrigger: true,
			highlight: false,
			maxDropHeight: 145,
			method: 'get',
			minChars: 2
		});
	});
	
	function loadRoles(id) {
		$("#roleplayername").html("<img class=\"flag-xs\" src=\"<?=$site->settings->uri_loading?>\">");
		
		$.get("<?=$site->settings->uri_rpc?>?op=TeamMembershipRoles&id=" + id, function(data) {
			var r = JSON.parse(data);
			console.log(r);
			if (r) {
				$("#roleplayerid").val(r.id);
				$("#roleplayername").html(r.player_name);
				$(".role-item").each(function(index) {
					
					// unset everything
					$(this).prop("checked", false);
					
					if ($(this).val() & r.role){
						$(this).prop("checked", true);
					}
				});
			}
		});
	}
</script>
	
<style>
	.ui-autocomplete-loading {
		background: white url("<?=$site->settings->uri_loading?>") right center no-repeat;
	}
	
	/* Required when using autocomplete in modals, so it shows up on top of the modal */
	.ui-autocomplete {
		z-index: 5000;
	}
</style>

<div id="rename-team" class="modal fade">
	<form role="form" action="/team.php" method="post">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Rename</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<div class="row">
							<div class="col-xs-4 col-sm-3 col-md-2 col-lg-2 col-xl-2">Name</div>
							<div class="col-xs-8 col-sm-9 col-md-10 col-lg-10 col-xl-10">
								<input class="form-control" type="text" name="team_name" value="<?=$t->name?>">
							</div>
						</div>
					</div>
					<!--
					<div class="form-group">
						<div class="row">
							<div class="col-xs-4 col-sm-3 col-md-2 col-lg-2 col-xl-2">URL Name</div>
							<div class="col-xs-8 col-sm-9 col-md-10 col-lg-10 col-xl-10"><input class="form-control" type="text" value="<?=$t->name_url?>"></div>
						</div>
					</div>
					-->
					<div class="form-group">
						<div class="row">
							<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2 col-xl-2">Flag</div>
							<div class="col-xs-7 col-sm-7 col-md-8 col-lg-8 col-xl-8">
								<select class="form-control" name="country" id="country">
								<?=user_list_countries_select($t->country)?>
								</select>
							</div>
							<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xl-2">
								<img class="flag-sm" id="flag">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" name="saveteamname">Save</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
		<input type="hidden" name="op" value="team.name.save"/>
		<input type="hidden" name="team" value="<?=$t->name_url?>"/>
		<input type="hidden" name="return" value="<?=get_return_url()?>"/>
	</form>
	
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$.get("<?=$site->settings->uri_rpc?>?op=GetFlagURL&id=<?=$t->country?>", function(data){
				$("#flag").attr("src", data);
			});
			
			$("#country").change(function() {
				$("#flag").attr("src", "<?=$site->settings->uri_loading?>");
				$.get("<?=$site->settings->uri_rpc?>?op=GetFlagURL&id=" + $("#country").val(), function(data){
					$("#flag").attr("src", data);
				});
			});
		});
	</script>
</div>
		
<div id="team-avatar" class="modal fade">
	<form role="form" action="/team.php" method="post">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Upload Avatar</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<div id="imgcrop"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
		<input type="hidden" name="op" value="team.avatar.upload"/>
		<input type="hidden" name="team" value="<?=$t->name_url?>"/>
		<input type="hidden" name="return" value="<?=get_return_url()?>"/>
	</form>
	
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			var croptions = {
				zoomFactor: 10,
				doubleZoomControls: false,
				rotateFactor: 10,
				rotateControls: false,
				uploadUrl:'/teams.php',
				uploadData: {
					"op": "team.avatar.upload"
				}
			}
			
			var cropheader = new Croppic("imgcrop", croptions);
		});
	</script>
</div>

<?php $roles = team_roles_get(); ?>
<div id="team-roles" class="modal fade">
	<form role="form" action="/team.php" method="post">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title"><?=$t->name?> roles for <span id="roleplayername">...</span></h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<ul class="role-list">
<?php foreach ((array) $roles as $r) { ?>
							<li><input class="role-item" id="role<?=$r->value?>" type="checkbox" name="selectedroles[]" value="<?=$r->value?>"> <?=$r->name?></li>
<?php } ?>
						</ul>
					</div>
					
					<div class="form-group">
						<p>* <em>Captain</em> is the only role that offers additional website functionality</p>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" name="saveteamname">Save</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
		<input type="hidden" name="op" value="team.roles.save"/>
		<input id="roleplayerid" type="hidden" name="roleid" value="0"/>
		<input type="hidden" name="return" value="<?=get_return_url()?>"/>
	</form>
</div>

<?php
	include_once("_inc/footer.php");
	die();
}
?>