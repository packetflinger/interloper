<?php

// just while developing
error_reporting(E_ALL);
ini_set('display_errors', 1);

// include all libraries
include_once("_inc/exceptions-lib.php");
include_once("_inc/settings.php");
include_once("_inc/functions.php");
include_once("_inc/user-lib.php");
include_once("_inc/game-lib.php");
include_once("_inc/community-lib.php");
include_once("_inc/discuss-lib.php");
include_once("_inc/team-lib.php");
include_once("_inc/message-lib.php");
include_once("_inc/comment-lib.php");
include_once("_inc/admin-lib.php");
include_once("_inc/league-lib.php");
include_once("_inc/content-lib.php");
include_once("_inc/group-lib.php");
include_once("_inc/match-lib.php");
include_once("_inc/feature-lib.php");

// connect to the database
$db = db_connect();

// make life easier
$get = (object)$_GET;
$post = (object)$_POST;

// revive any existing sessions
session_start();

$site = new stdClass();
$site->db = $db;
$site->get = $get;
$site->post = $post;
$site->settings = $settings;

$user = user_setup();
$site->user = $user;

context_define();
error_define();
team_role_define();
message_define();
user_define();
community_define();
league_define();
feature_define();
//group_define();

define("RESULT_SUCCESS", 1);
define("RESULT_FAIL", 0);

$site->args = parse_args();
$site->community = community_setup();
?>
