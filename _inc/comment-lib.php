<?php
function comment_allowed(){
	global $site;
	if (!user_is_logged_in()) {
		return false;
	}
	
	if ($site->user->muted || $site->user->banned || $site->user->disabled) {
		return false;
	}
	
	if (!$site->user->confirmed) {
		return false;
	}
	
	return true;
}

function comment_block($context, $id) {
	global $site;
	//echo "context: $context, id: $id";
	comment_button();
	//comment_modal($context, $id);
	comment_list($context, $id);
}

function comment_button(){
	global $site;
	if (user_is_logged_in()){
		return "<button class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#commentModal\"><span class=\"glyphicon glyphicon-comment\"></span> Comment</button>";
	}
	return "";
}

// the "flag, mod up, mod down" links for each comment
function comment_controls() {
	if (comment_allowed()){
	?>
		<div>
			<span class="pull-right comment-controls">
				<a href="#"><span title="Thumbs Up" class="glyphicon glyphicon-thumbs-up"></span></a>
				<a href="#"><span title="Thumbs Down" class="glyphicon glyphicon-thumbs-down"></span></a>
				<a href="#"><span title="Report this Comment (flag it)" class="glyphicon glyphicon-flag"></span></a>
			</span>
		</div>
	<?php
	}
}
// get the depth level of the supplied comment (usually a parent)
function comment_depth($id){
	global $site;
	$sql = "SELECT level FROM comment WHERE id = ? LIMIT 1";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $id, PDO::PARAM_INT);
	$q->execute();
	if ($q->rowCount() == 1){
		$r = $q->fetch(PDO::FETCH_OBJ);
		return $r->level;
	}
	return -1;
}

function comment_get($id){
	global $site;
	$sql = "SELECT c.*, 
				u.username,
				if(NOW() > DATE_ADD(comment_date, INTERVAL ? DAY), 0, 1) AS editable
			FROM comment c
			JOIN user u ON c.user = u.id
			WHERE c.id = ?
			LIMIT 1";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $site->settings->comment_editage, PDO::PARAM_INT);
	$q->bindValue(2, $id, PDO::PARAM_INT);
	$q->execute();
	if ($q->rowCount() == 1) {
		return $q->fetch(PDO::FETCH_OBJ);
	}
}

// async
function comment_insert(){
	global $site;
	$out = new stdclass();
	
	$post = slash($site->post);
	
	$level = ($site->post->parent > 0) ? comment_depth($site->post->parent) + 1: 0;
	try {
		db_begin();
		$sql = "INSERT INTO comment (
					context, context_value, user, parent, comment_date, comment
				) VALUES (
					?, ?, ?, ?, NOW(), ?
				)";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $site->post->context, PDO::PARAM_INT);
		$q->bindValue(2, $site->post->id, PDO::PARAM_INT);
		$q->bindValue(3, $site->user->id, PDO::PARAM_INT);
		$q->bindValue(4, $site->post->parent, PDO::PARAM_INT);
		$q->bindValue(5, $site->post->comment, PDO::PARAM_STR);
		$q->execute();
		$cid = $site->db->lastInsertId();	
		
		// insert the nest
		$sql = "SELECT nest FROM comment WHERE id = ? LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $site->post->parent, PDO::PARAM_INT);
		$q->execute();
		$r = $q->fetch(PDO::FETCH_OBJ);
		if (isset($r->nest)){
			$nest = sprintf("%s-%07d", $r->nest, $cid);
		} else {
			$nest = sprintf("%07d", $cid);
		}
		$sql = "UPDATE comment SET nest = '$nest' WHERE id = $cid LIMIT 1";
		$site->db->query($sql);
		
		// insert the closure records
		comment_insert_closure($cid, $cid, 0);
		
		$sql = "UPDATE user SET comments = comments + 1 WHERE id = ? LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $site->user->id, PDO::PARAM_INT);
		$q->execute();
		
		// do context specific stuff
		/*switch ($site->community->type) {
			case CTX_CONTENT:	// increment comment counter
				$sql = "UPDATE content SET comments = comments + 1 WHERE id = ? LIMIT 1";
				$q = $site->db->prepare($sql);
				$q->bindValue(1, $site->post->id, PDO::PARAM_INT);
				$q->execute();
				break;
		}*/
		
		db_commit();
		/*die_gracefully_redirect(
			"<h2>Comment Added</h2><p>Your comment was successfully added, you'll be redirected in a second.</p>",
			base64_decode($site->post->return)
		);*/
		$out->result = 1;
		$out->message = "Comment added successfully";
		$out->redirect_url = base64_decode($site->post->return);
		$out->commend_id = $cid;
		$out->redirect_timeout = 2500;
		echo json_encode($out);
		
	} catch (PDOException $e){
		db_rollback();
		$out->result = 0;
		$out->message = "Comment error: {$e->getMessage()}";
		echo json_encode($out);
	}
}

// recursive
function comment_insert_closure($anc, $des, $depth){
	global $site;
	$sql = "INSERT INTO comment_closure (
				ancestor, descendant, depth
			) VALUES (
				?, ?, ?
			)";
	$q = $site->db->prepare($sql);
	
	// self
	$q->bindValue(1, $anc, PDO::PARAM_INT);
	$q->bindValue(2, $des, PDO::PARAM_INT);
	$q->bindValue(3, $depth, PDO::PARAM_INT);
	$q->execute();
	
	if ($depth == 0){
		// root
		$q->bindValue(1, 0, PDO::PARAM_INT);
		$q->bindValue(2, $des, PDO::PARAM_INT);
		$q->bindValue(3, $depth, PDO::PARAM_INT);
		$q->execute();
	}
	
	$d = $depth + 1;	
	$sql = "SELECT id, parent FROM comment WHERE id = ? LIMIT 1";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $anc, PDO::PARAM_INT);
	$q->execute();
	$r = $q->fetch(PDO::FETCH_OBJ);
	if (isset($r->parent) && $r->parent != 0){
		comment_insert_closure($r->parent, $des, $d);
	}
}

function comment_list($ctx, $id) {
	global $site;
	$sql = "SELECT c.*,
				if(NOW() > DATE_ADD(c.comment_date, INTERVAL ? DAY), 0, 1) AS editable
			FROM comment c
			WHERE c.context = ? 
				AND c.context_value = ? 
			ORDER BY c.nest ASC";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $site->settings->comment_editage, PDO::PARAM_INT);
	$q->bindValue(2, $ctx, PDO::PARAM_INT);
	$q->bindValue(3, $id, PDO::PARAM_INT);
	$q->execute();	
	
	$comment_count = $q->rowCount();
	$button = comment_button();
	if ($comment_count > 0) {
		comment_modal($ctx, $id);
		echo "<h2 style=\"color: white;\">Comments <span class=\"badge\">$comment_count</span> <span class=\"pull-right\">$button</span></h2>";
		echo "<div id=\"comments\">";
		while ($r = $q->fetch(PDO::FETCH_OBJ)) {
			$r = unslash($r);
			$creator = get_user($r->user);
			$avatar_url = avatar_get_url($creator, 50);
			$mine = ($r->user == $site->user->id) ? true : false;
			$author = ($mine) ? "<b>you</b>" : user_make_anchor($creator->username);
			$depth = sizeof(explode("-", $r->nest)) - 1;
			
			$editbutton = "";
			if ($mine && $r->editable && comment_allowed()) {
				$editbutton = "<button type=\"button\" data-target=\"#commentEditModal{$r->id}\" class=\"btn btn-default btn-xs\" data-toggle=\"modal\"><span class=\"glyphicon glyphicon-edit\"></span> Edit</button>";
			}
			
			$replybutton = "";
			if (comment_allowed()) {
				$replybutton = "<button data-name=\"{$creator->username}\" data-parent=\"{$r->id}\" data-target=\"#replyModal\" class=\"btn btn-default btn-xs reply-button\" data-toggle=\"modal\"><span class=\"glyphicon glyphicon-comment\"></span> Reply</button>";
			}
			
			$commentdate = format_date($r->comment_date, 1, "M j, Y");
			$indent = ($depth > 0) ? " style=\"margin-left: " . 20 * $depth . "px;\"" : ""; 
			?>
			
			<!-- the actual comment -->
			<div class="row"<?=$indent?>>
				<div class="col-xs-2 col-sm-1 col-md-1">
					<img class="img-rounded" src="<?=$avatar_url?>"/>
				</div>
				<div class="col-xs-10 col-sm-11 col-md-11">
					<div class="panel panel-comment">
						<div class="panel-heading">
							<?=$commentdate?> - <?=$author?> said: <span class="pull-right"><?=$replybutton . " " . $editbutton?></span>
						</div>
						<div class="panel-body">
							<?=$r->comment?>
							<?=comment_controls()?>
						</div>
					</div>
				</div>
			</div>
			
<?php
			if ($r->editable) {
?>
				<div id="commentEditModal<?=$r->id?>" class="modal fade">
					<form role="form" action="<?=$site->settings->uri_rpc?>" method="post">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title">Edit Your Comment</h4>
								</div>
								<div class="modal-body">
									<div class="form-group">
										<textarea id="editcomment<?=$r->id?>" name="comment" class="form-control edittxtarea" placeholder="" required><?=$r->comment?></textarea> 
										<script type="text/javascript">
											$(document).ready(function($) {
												$('.edittxtarea').summernote({ 
													height: 200
												});
											});
										</script>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-primary" id="editcomment" name="editcomment">Save Comment</button>
									<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
								</div>
							</div>
						</div>
						<input type="hidden" id="edit-id<?=$r->id?>" name="id" value="<?=$r->id?>"/>
						<input type="hidden" id="edit-return<?=$r->id?>" name="return" value="<?=get_return_url()?>"/>
						<input type="hidden" name="edit-op<?=$r->id?>" value="EditComment"/>
					</form>
				</div>
				<script type="text/javascript">
					jQuery(document).ready(function($) {
						
						$("#editcomment").click(function() {
							if ($("editcomment<?=$r->id?>").val() != "") {
								$.post("<?=$site->settings->uri_rpc?>", 
									{op: $("#edit-op<?=$r->id?>").val(), id: $("#edit-id<?=$r->id?>").val(), return: $("#edit-return<?=$r->id?>").val(), comment: $("#editcomment<?=$r->id?>").val()},
									function(result) {
										alert(result);
										
										var obj = JSON.parse(result);
										if (obj.result == 1) {
											toastr.success(obj.message);
											setTimeout(function() {
												window.location.href = obj.redirect_url;
											}, obj.redirect_timeout);
										}
										
										if (obj.result == 0) {
											toastr.error(obj.message);
										}
									}
								);
							}
						});
					});
				</script>
<?php
			}
		}
?>
		<div id="replyModal" class="modal fade">
			<form role="form" action="<?=$site->settings->uri_rpc?>" method="post">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 id="replytitle" class="modal-title">Reply to %s's Comment</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<textarea id="replycomment" name="comment" class="form-control edittxtarea" placeholder="" required></textarea> 
								<script type="text/javascript">
									$(document).ready(function($) {
										$('.edittxtarea').summernote({ 
											height: 200
										});
										
										$(".reply-button").click(function(){
											$("#replytitle").html("Reply to " + $(this).data("name")+ "'s comment");
											$("#parentval").val($(this).data("parent"));
										});
									});
								</script>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary" name="editcomment">Comment</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						</div>
					</div>
				</div>
				<input type="hidden" name="context" value="<?=$ctx?>"/>
				<input type="hidden" name="id" value="<?=$id?>"/>
				<input type="hidden" name="return" value="<?=get_return_url()?>"/>
				<input type="hidden" name="op" value="leavecomment"/>
				<input type="hidden" name="parent" value="" id="parentval"/>
			</form>
		</div>
<?php
	} else {
		comment_modal($ctx, $id);
		echo "<p>$button</p>";
	}
}

function comment_modal($ctx, $id, $parent=0){
	global $site;
	?>
	<div id="commentModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Leave a Comment</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<textarea id="comment" name="comment" required focus></textarea>
					</div>
					<input type="hidden" id="op" name="op" value="AddComment">
					<input type="hidden" id="context" name="context" value="<?=$ctx?>">
					<input type="hidden" id="id" name="id" value="<?=$id?>">
					<input type="hidden" id="return" name="return" value="<?=get_return_url()?>">
					<input type="hidden" id="parent" name="parent" value="<?=$parent?>">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" name="send" id="addcomment"><span class="glyphicon glyphicon-comment"></span> Comment</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$("#comment").summernote({
				height: '200px'
			});
			
			$("#addcomment").click(function() {
				if ($("#comment").val() != "") {
					$.post("<?=$site->settings->uri_rpc?>", 
						{op: $("#op").val(), context: $("#context").val(), id: $("#id").val(), return: $("#return").val(), parent: $("#parent").val(), comment: $("#comment").val()},
						function(result) {
							var obj = JSON.parse(result);
							if (obj.result == 1) {
								toastr.success(obj.message);
								setTimeout(function() {
									window.location.href = obj.redirect_url;
								}, obj.redirect_timeout);
							}
							
							if (obj.result == 0) {
								toastr.error(obj.message);
							}
						});
				}
			});
		});
	</script>
	<?php
}

// takes a flat array of comment objects and nests them based on parent attribute
function comment_nested_sort($arr){
	$out = array();
	for ($i=0; $i<sizeof($arr); $i++) {
		if ($arr[$i]->parent == 0){
			$out['toplevel'][] = $arr[$i];
		} else {
			$p = $arr[$i]->parent;
			$out['parent$p'][] = $arr[$i];
		}
	}
	return $out;
}

function comment_save(){
	global $site;
	var_dump($site->post);
	die();
	$site->post = slash($site->post);
	$c = comment_get($site->post->id);
	if ($site->user->id == $c->user && $c->editable) {
		$sql = "UPDATE comment SET
					comment = ?,
					edited = 1,
					edited_user = ?,
					edited_date = NOW()
				WHERE id = ?
				LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $site->post->comment, PDO::PARAM_STR);
		$q->bindValue(2, $site->user->id, PDO::PARAM_INT);
		$q->bindValue(3, $c->id, PDO::PARAM_INT);
		$q->execute();
		die_gracefully_redirect(
			"<h2>Comment Saved</h2><p>The edits to your comment were saved, you'll be redirected in a second.</p>",
			base64_decode($site->post->return)
		);	
	} else {
		die_gracefully("<h2>Error Saving Comment</h2><p>This comment cannot be edited.</p>");
	}
}
?>
