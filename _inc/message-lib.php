<?php

function message_define() {
	/*
	define("MSG_INBOX",		0);
	define("MSG_DELETED",	1);
	define("MSG_SENT", 		2);
	define("MSG_DRAFT",		3);
	define("MSG_SEEN",		4);
	define("MSG_COMPOSE",	5);
	define("MSG_VIEW",		6);
	*/
	define("MSG_INBOX",		"inbox");
	define("MSG_DELETED",	"garbage");
	define("MSG_SENT", 		"sent");
	define("MSG_DRAFT",		"drafts");
	define("MSG_SEEN",		4);
	define("MSG_COMPOSE",	5);
	define("MSG_VIEW",		6);
	
	define("MSG_BOX_INBOX",		1);
	define("MSG_BOX_SENT",		2);
	define("MSG_BOX_TRASH",		3);
	define("MSG_BOX_DRAFTS",	4);
}

function message_deliver($obj) {
	global $site;
	
	try {
		$obj = slash($obj);
		
		$sql = "INSERT INTO message (
					user, box, lookup, sender_player, 
					recipient_player, subject, body, seen, 
					sent, sent_date
				) VALUES (
					?,?,?,?,
					?,?,?,?,
					?,NOW()
				)";
		db_begin();
		$q = $site->db->prepare($sql);
		
		$q->bindValue(1, $obj->userid, PDO::PARAM_INT);
		$q->bindValue(2, $obj->box, PDO::PARAM_INT);
		$q->bindValue(3, generate_key(8), PDO::PARAM_STR);
		$q->bindValue(4, $obj->sender_id, PDO::PARAM_INT);
		$q->bindValue(5, $obj->recipient_id, PDO::PARAM_INT);
		$q->bindValue(6, $obj->subject, PDO::PARAM_STR);
		$q->bindValue(7, $obj->body, PDO::PARAM_STR);
		$q->bindValue(8, ($obj->box == MSG_BOX_SENT) ? 1 : 0, PDO::PARAM_INT);
		$q->bindValue(9, ($obj->box == MSG_BOX_DRAFTS) ? 0 : 1, PDO::PARAM_INT);
		$q->execute();
		
		db_commit();
	} catch(PDOException $e) {
		db_die($e->getMessage());
	}
}

function message_get($hash) {
	global $site;
	
	$sql = "SELECT m.*,
				u1.name AS name_sender,
				u1.name_url AS name_sender_url,
				u2.name AS name_recipient,
				u2.name_url AS name_recipient_url
			FROM message m 
			JOIN player u1 ON m.sender_player = u1.id
			JOIN player u2 ON m.recipient_player = u2.id
			WHERE m.lookup = ? 
			LIMIT 1";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $hash, PDO::PARAM_STR);
	$q->execute();
	while ($r = $q->fetch(PDO::FETCH_OBJ)) {
		return unslash($r);
	}
}

function message_inbox($user, $offset, $length, $folder) {
	global $site;
	
	$players = user_get_players($user);
	$plist = "";
	for ($i=0; $i<sizeof($players); $i++) {
		$plist .= $players[$i]->id . ",";
	}
	
	$sql = "SELECT m.*,
				u1.name AS name_sender,
				u2.name AS name_recipient 
			FROM message m 
			JOIN player u1 ON m.sender_player = u1.id
			JOIN player u2 ON m.recipient_player = u2.id
			WHERE m.user = ?
				AND m.box = ? ";
	
	$sql .= "ORDER BY sent_date DESC
			LIMIT ? OFFSET ?";
	
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $user, PDO::PARAM_INT);
	$q->bindValue(2, $folder, PDO::PARAM_INT);
	//$q->bindValue(3, $plist, PDO::PARAM_STR);
	$q->bindValue(3, $length, PDO::PARAM_INT);
	$q->bindValue(4, $offset, PDO::PARAM_INT);
	$q->execute();
	
	while ($r = $q->fetchAll(PDO::FETCH_OBJ)) {
		return $r;
	}
}

function message_toggle_deleted($hash) {
	global $site;
	
	$sql = "UPDATE message SET deleted = !deleted, deleted_date = NOW() WHERE lookup = ? LIMIT 1";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $hash, PDO::PARAM_STR);
	$q->execute();
}

function message_toggle_draft($hash) {
	global $site;
	
	$sql = "UPDATE message SET draft = !draft, draft_date = NOW() WHERE lookup = ? LIMIT 1";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $hash, PDO::PARAM_STR);
	$q->execute();
}

function message_toggle_seen($hash) {
	global $site;
	
	$sql = "UPDATE message SET seen = !seen, seen_date = NOW() WHERE lookup = ? LIMIT 1";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $hash, PDO::PARAM_STR);
	$q->execute();
}

function message_unread_count($userid) {
	global $site;
	
	try {
		$sql = "SELECT COUNT(id) AS unread 
				FROM message 
				WHERE user = ?
					AND BOX = ?
					AND seen = 0";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $userid, PDO::PARAM_INT);
		$q->bindValue(2	, MSG_BOX_INBOX, PDO::PARAM_INT);
		$q->execute();
		while ($r = $q->fetch(PDO::FETCH_OBJ)) {
			return $r->unread;
		}
	} catch(PDOException $e) {
		return 0;
	}
}
?>