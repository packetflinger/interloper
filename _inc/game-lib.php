<?php

function game_add($name, $name_url) {
	global $site;
	
	$sql = "INSERT INTO game (name, name_url) VALUES (?, ?)";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $name, PDO::PARAM_STR);
	$q->bindValue(2, $name_url, PDO::PARAM_STR);
	$q->execute();
}

function game_get($id) {
	global $site;
	if (isset($id) && is_numeric($id)){
		$sql = "SELECT * 
				FROM game
				WHERE id = ? 
				LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $id, PDO::PARAM_INT);
		$q->execute();
		
		while ($r = $q->fetch(PDO::FETCH_OBJ)) {
			return $r;
		}
		
		return null;
	}
}

function game_get_by_name($name) {
	global $site;
	if (isset($name) && $name != ""){
		$sql = "SELECT * 
				FROM game
				WHERE name_url = ? 
				LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $name, PDO::PARAM_STR);
		$q->execute();
		
		while ($r = $q->fetch(PDO::FETCH_OBJ)) {
			return $r;
		}
		
		return null;
	}
}

function game_remove($id) {
	global $site;
	
	$sql = "DELETE FROM game WHERE id = ? LIMIT 1";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $id, PDO::PARAM_INT);
	$q->execute();
}

function game_update($id, $name, $name_url) {
	global $site;
	
	$sql = "UPDATE game SET 
				name = ?,
				name_url = ?
			WHERE id = ?
			LIMIT 1";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $name, PDO::PARAM_STR);
	$q->bindValue(2, $name_url, PDO::PARAM_STR);
	$q->bindValue(3, $id, PDO::PARAM_INT);
	$q->execute();
}

?>