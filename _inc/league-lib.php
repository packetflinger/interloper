<?php

function league_create($o) {
	global $db;
	
	$sql = "INSERT INTO league (
				access_key, name, name_url, name_short,
				community, date_created, creator
			) VALUES (
				?, ?, ?, ?,
				?, NOW(), ?
			)";
	try {
		$q = $db->prepare($sql);
		$q->bindValue(1, $o->key, PDO::PARAM_STR);
		$q->bindValue(2, $o->name, PDO::PARAM_STR);
		$q->bindValue(3, $o->name_url, PDO::PARAM_STR);
		$q->bindValue(4, $o->name_short, PDO::PARAM_STR);
		$q->bindValue(5, $o->community, PDO::PARAM_INT);
		$q->bindValue(6, $o->creator, PDO::PARAM_INT);
		$q->execute();
		
		$r = return_obj_success();
		$r->league_id = $db->lastInsertId();

		return $r; 
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}

function league_define() {
	define("LEAGUE_TYPE_STANDARD",	0);
	define("LEAGUE_TYPE_KOTH", 		1);
	define("LEAGUE_TYPE_TIME", 		2);
	
	define("LEAGUE_TEAM_TYPE_PLAYER",	0);
	define("LEAGUE_TEAM_TYPE_TEAM",		1);
	
	define("LEAGUE_STAFF_CREATOR",		1000);
	define("LEAGUE_STAFF_ADMIN",		500);
	define("LEAGUE_STAFF_REFEREE",		100);
	define("LEAGUE_STAFF_CONTRIBUTOR",	50);
	define("LEAGUE_STAFF_MASCOT",		10);

}

function league_staff_level_to_string($level) {
	switch ($level) {
		case LEAGUE_STAFF_CREATOR:		return "creator";
		case LEAGUE_STAFF_ADMIN:		return "admin";
		case LEAGUE_STAFF_REFEREE:		return "referee";
		case LEAGUE_STAFF_CONTRIBUTOR:	return "contributor";
		case LEAGUE_STAFF_MASCOT:		return "mascot";
	}
	
	return "unknown";
}

function league_staff_level_list($level) {
	$list = array(
		LEAGUE_STAFF_CREATOR => "<option value=\"" . LEAGUE_STAFF_CREATOR . "\">" . league_staff_level_to_string(LEAGUE_STAFF_CREATOR) . "</option>",
		LEAGUE_STAFF_ADMIN => "<option value=\"" . LEAGUE_STAFF_ADMIN . "\">" . league_staff_level_to_string(LEAGUE_STAFF_ADMIN) . "</option>",
		LEAGUE_STAFF_REFEREE => "<option value=\"" . LEAGUE_STAFF_REFEREE . "\">" . league_staff_level_to_string(LEAGUE_STAFF_REFEREE) . "</option>",
		LEAGUE_STAFF_CONTRIBUTOR => "<option value=\"" . LEAGUE_STAFF_CONTRIBUTOR . "\">" . league_staff_level_to_string(LEAGUE_STAFF_CONTRIBUTOR) . "</option>",
		LEAGUE_STAFF_MASCOT => "<option value=\"" . LEAGUE_STAFF_MASCOT . "\">" . league_staff_level_to_string(LEAGUE_STAFF_MASCOT) . "</option>",
	);
	
	$buf = "";
	foreach($list as $key => $value) {
		if ($key > $level)
			continue;
		
		$buf .= $value;
	}
	
	return $buf;
}

function league_delete($id) {
	global $db;
	
	try {
		db_begin();
		
		$sql = "DELETE FROM league WHERE id = ? LIMIT 1";
		$q = $db->prepare($sql);
		$q->bindValue(1, $id, PDO::PARAM_INT);
		$q->execute();
		
		db_commit();
		return return_obj_success();
		
	} catch (PDOException $e) {
		db_rollback();
		return return_obj_fail($e->getMessage());
	}
}


function league_edit($o) {
	global $site;
	$sql = "UPDATE league SET
				access_key = ?, league_type = ?, name = ?, name_url = ?, name_short = ?,
				community = ?, date_created = ?, creator = ?, team_type = ?, team_count = ?,
				match_count = ?, match_played_count = ?, invite_only = ?, 
				disabled = ?, invisible = ?, policy_required = ?,
				enrollment_open = ?, enrollment_start = ?, enrollment_end = ?, enrollment_limit = ?, locked = ?
			WHERE id = ?
			LIMIT 1";
	try {
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $o->access_key, PDO::PARAM_STR);
		$q->bindValue(2, $o->league_type, PDO::PARAM_INT);
		$q->bindValue(3, $o->name, PDO::PARAM_STR);
		$q->bindValue(4, $o->name_url, PDO::PARAM_STR);
		$q->bindValue(5, $o->name_short, PDO::PARAM_STR);
		
		$q->bindValue(6, $o->community, PDO::PARAM_INT);
		$q->bindValue(7, $o->date_created, PDO::PARAM_STR);
		$q->bindValue(8, $o->creator, PDO::PARAM_INT);
		$q->bindValue(9, $o->team_type, PDO::PARAM_INT);
		$q->bindValue(10, $o->team_count, PDO::PARAM_INT);
		
		$q->bindValue(11, $o->match_count, PDO::PARAM_INT);
		$q->bindValue(12, $o->match_played_count, PDO::PARAM_INT);
		$q->bindValue(13, $o->invite_only, PDO::PARAM_INT);
		
		$q->bindValue(14, $o->disabled, PDO::PARAM_INT);
		$q->bindValue(15, $o->invisible, PDO::PARAM_INT);
		$q->bindValue(16, $o->policy_required, PDO::PARAM_INT);
		
		$q->bindValue(17, $o->enrollment_open, PDO::PARAM_INT);
		$q->bindValue(18, $o->enrollment_start, PDO::PARAM_STR);
		$q->bindValue(19, $o->enrollment_end, PDO::PARAM_STR);
		$q->bindValue(20, $o->enrollment_limit, PDO::PARAM_INT);
		$q->bindValue(21, $o->locked, PDO::PARAM_INT);
		
		$q->bindValue(22, $o->id, PDO::PARAM_INT);
		$q->execute();
		
		$r = return_obj_success();

		return $r; 
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}


function league_get($id) {
	global $db;
	
	try {
		$sql = "SELECT * FROM league WHERE id = ? LIMIT 1";
		$q = $db->prepare($sql);
		$q->bindValue(1, $id, PDO::PARAM_INT);
		$q->execute();
		
		while ($r = $q->fetch(PDO::FETCH_OBJ)) {
			return $r;
		}
		
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}

function league_get_by_key($k) {
	global $db;
	
	try {
		$sql = "SELECT * FROM league WHERE access_key = ? LIMIT 1";
		$q = $db->prepare($sql);
		$q->bindValue(1, $k, PDO::PARAM_INT);
		$q->execute();
		
		while ($r = $q->fetch(PDO::FETCH_OBJ)) {
			return $r;
		}
		
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}

function league_get_by_name($name) {
	global $site;
	
	try {
		$sql = "SELECT * FROM league WHERE name_url = ? LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $name, PDO::PARAM_INT);
		$q->execute();
		
		while ($r = $q->fetch(PDO::FETCH_OBJ)) {
			return $r;
		}
		
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}

function league_list($community) {
	global $site;
	
	try {
		$sql = "SELECT id, access_key, league_type, name, name_url, name_short, date_created FROM league WHERE community = ?";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $community, PDO::PARAM_INT);
		$q->execute();
		
		while ($r = $q->fetchAll(PDO::FETCH_OBJ)) {
			return $r;
		}
		return null;
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}

function league_staff($league_id) {
	global $site;
	
	try {
		$sql = "SELECT s.*, p.name, p.name_url 
				FROM league_staff s 
				JOIN player p ON p.id = s.player_id
				WHERE league_id = ?
				ORDER BY p.name_url";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $league_id, PDO::PARAM_INT);
		$q->execute();
		
		while ($r = $q->fetchAll(PDO::FETCH_OBJ)) {
			return $r;
		}
		return null;
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}

function league_staff_add($o) {
	global $db;
	
	try {
		$sql = "INSERT INTO league_staff (
					league_id, player_id, level, added_by, date_added
				) VALUES (
					?, ?, ?, ?, NOW()
				)";
		$q = $db->prepare($sql);
		$q->bindValue(1, $o->league_id, PDO::PARAM_INT);
		$q->bindValue(2, $o->player_id, PDO::PARAM_INT);
		$q->bindValue(3, $o->level, PDO::PARAM_INT);
		$q->bindValue(4, $o->added_by, PDO::PARAM_INT);
		$q->execute();
		
		$r = return_obj_success();
		$r->id = $db->lastInsertId();
		
		return $r;
		
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}

function league_staff_delete($id) {
	global $site;
	
	try {
		$sql = "DELETE FROM league_staff WHERE id = ? LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $id, PDO::PARAM_INT);
		$q->execute();
		
		$r = return_obj_success();
		$r->message = "League staff removed";
		return $r;
		
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}

function league_setlock($id, $lock) {
	global $site;
	try {
		$sql = "UPDATE league SET locked = ? WHERE id = ? LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $lock, PDO::PARAM_INT);
		$q->bindValue(2, $id, PDO::PARAM_INT);
		$q->execute();
		return return_obj_success();
		
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}

function league_member_add($o) {
	global $site;
	
	try {
		$sql = "INSERT INTO league_membership (
					league, team, participant, permission, sort_order, date_added
				) VALUES (
					?, ?, 1, ?, ROUND(RAND(), 4), NOW()
				)";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $o->league_id, PDO::PARAM_INT);
		$q->bindValue(2, $o->member_id, PDO::PARAM_INT);
		$q->bindValue(3, $o->level, PDO::PARAM_INT);
		$q->execute();
		
		$r = return_obj_success();
		$r->id = $site->db->lastInsertId();
		
		return $r;
		
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}

function league_member_delete($id) {
	global $db;
	
	try {
		$sql = "DELETE FROM league_membership WHERE id = ? LIMIT 1";
		$q = $db->prepare($sql);
		$q->bindValue(1, $id, PDO::PARAM_INT);
		$q->execute();
		return return_obj_success();
		
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}

function league_player_add($league_id, $player_id) {
	global $site;
	
	$sql = "INSERT INTO league_player (
				league, team, date_added
			) VALUES (
				?, ?, NOW()
			)";
	try {
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $league_id, PDO::PARAM_INT);
		$q->bindValue(2, $player_id, PDO::PARAM_INT);
		$q->execute();
		
		$r = return_obj_success();
		$r->id = $site->db->lastInsertId();
		
		return $r;
		
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}

function league_player_remove($league_id, $player_id) {
	global $site;
	
	$sql = "SELECT id
			FROM league_player
			WHERE league = ? AND team = ?
			LIMIT 1";
	try {
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $league_id, PDO::PARAM_INT);
		$q->bindValue(2, $player_id, PDO::PARAM_INT);
		$q->execute();
		
		while ($r = $q->fetch(PDO::FETCH_OBJ)) {
			$site->db->query("DELETE FROM league_player WHERE id = {$r->id} LIMIT 1");
			$r = return_obj_success();
			return $r;
		}
		
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}

function league_player_list($league_key) {
	global $site;
	
	$l = league_get_by_key($league_key);
	
	if ($l->team_type == LEAGUE_TEAM_TYPE_PLAYER) {
		$sql = "SELECT lp.*, p.name, p.name_url, p.user
				FROM league_player lp
				JOIN player p ON p.id = lp.team
				WHERE lp.league = ?";
	} else {
		$sql = "SELECT lp.*, t.name, t.name_url
				FROM league_player lp
				JOIN team t ON t.id = lp.team
				WHERE lp.league = ?";
	}
	
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $l->id, PDO::PARAM_INT);
	$q->execute();
	
	while ($r = $q->fetchAll(PDO::FETCH_OBJ)) {
		//$r->team_type = $l->team_type;
		return $r;
	}
}

function league_get_available_members($id) {
	global $site;
	
	try {
		$sql = "SELECT l.league_type 
				FROM league l
				WHERE l.id = ?
				LIMIT 1";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $id, PDO::PARAM_INT);
		$q->execute();
			
		while ($r = $q->fetch(PDO::FETCH_OBJ)) {
			
			if ($r->league_type == LEAGUE_TEAM_TYPE_PLAYER) {
				$sql = "SELECT lp.*, 
							p.id AS member_id, 
							p.name AS member_name, 
							p.name_url AS member_name_url
						FROM league_player lp
						JOIN player p ON p.id = lp.team
						WHERE p.id NOT IN (
							SELECT lgm.team_id 
							FROM league_group_membership lgm
							WHERE lgm.group_id IN (
								SELECT id FROM league_group WHERE league = ?
							)
						)";
						
			} else {
				$sql = "SELECT lp.*, 
							p.id AS member_id, 
							p.name AS member_name, 
							p.name_url AS member_name_url
						FROM league_player lp
						JOIN team t ON p.id = lp.team
						WHERE p.id NOT IN (
							SELECT team_id
							FROM league_group_membership
							WHERE group_id IN (
								SELECT id FROM league_group WHERE league = ?
							)
						)";
			}
			$qm = $site->db->prepare($sql);
			$qm->bindValue(1, $id, PDO::PARAM_INT);
			$qm->execute();
			
			while ($rm = $qm->fetchAll(PDO::FETCH_OBJ)) {
				return $rm;
			}
		}
	} catch (PDOException $e) {
		return return_obj_fail($e->getMessage());
	}
}

function leagues_index() {
	global $site;
	
	echo "hi";
}
?>