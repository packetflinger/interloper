<!--
<?=var_dump($site)?>
-->
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="/assets/img/favicon.ico">
		<title>Dashboard</title>
		<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
		<link href="/assets/css/main.css" rel="stylesheet">
		<link href="/assets/css/jquery-ui-1.12.1.min.css" rel="stylesheet">
		<link href="/assets/css/summernote.css" rel="stylesheet">
		<link href="/assets/css/toastr.min.css" rel="stylesheet">
		<link href="/assets/css/magicsuggest-min.2.1.4.css" rel="stylesheet">
		<link href="/assets/css/croppic.css" rel="stylesheet">
		
		<script src="/assets/js/jquery-3.2.1.min.js"></script>
		<script src="/assets/js/jquery-ui-1.12.1.min.js"></script>
		<script src="/assets/js/bootstrap-3.3.7.min.js"></script>
		
		<script src="/assets/js/summernote.min.js"></script>
		<script src="/assets/js/toastr.min.js"></script>
		<script src="/assets/js/bootstrap-datepicker.min.js"></script>
		<script src="/assets/js/magicsuggest.2.1.4.js"></script>
		<script src="/assets/js/croppic.js"></script>
	</head>

<body>
