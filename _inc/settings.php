<?php
error_reporting(E_ALL & ~E_STRICT);
date_default_timezone_set('UTC');

$settings = new stdClass();

// site settings
$settings->site_name	= "Q2 Players";
$settings->site_path	= "/var/www/domains/interloper";
$settings->site_url		= "http://interloper.evilserv.com";


// database settings
$settings->db_host 		= "localhost";
$settings->db_user 		= "root";
$settings->db_pass 		= "";
$settings->db_schema 	= "interloper";
$settings->db_timezone	= "+00:00";	// store everything in GMT


// email settings
$settings->email_from 	= "\"Q2Players Notify\" <notify@q2players.org>";
$settings->email_smtp	= "localhost";
$settings->email_port	= 25;
$settings->email_ehlo	= "notify.q2players.org";
$settings->email_rp		= "joe@joereid.com";	// return-path


// section settings
$settings->sec_max_depth 	= 5;	// only 5 nested levels allowed


// user settings
//$settings->user_default_tz 		= 38;	// UTC in the timezone database table
$settings->user_default_tz 		= "UTC";
$settings->user_default_lang	= "en";

// flags
$settings->flag_path	= "/assets/img/flags";

// messages
$settings->msg_pagelimit 	= 10;

// comments
$settings->comment_maxdepth	= 5;	// max nest levels for comments
$settings->comment_editage	= 7;	// max days before comment cannot be edited

// uri paths
$settings->uri_community	= "/community";
$settings->uri_team			= "/teams";
$settings->uri_admin		= "/admin";
$settings->uri_join			= "/join.php";
$settings->uri_login		= "/signin";
$settings->uri_logout		= "/signin?op=logout";
$settings->uri_profile 		= "/profile";
$settings->uri_profile_edit	= $settings->uri_profile . "/edit";
$settings->uri_user			= "/members";
$settings->uri_rpc			= "/async";
$settings->uri_post			= "/post";
$settings->uri_lostpass		= "/lostpassword";
$settings->uri_msg			= "/messages";
$settings->uri_msgcompose	= "/messages/compose";
$settings->uri_msgreply		= "/messages/reply";
$settings->uri_avatar		= "/assets/img/avatar";
$settings->uri_flag			= "/assets/img/flags";
$settings->uri_comment		= "/comment";
$settings->uri_settings		= "/settings";
$settings->uri_loading		= "/assets/img/loading.svg";
$settings->uri_myteams		= "/my-teams";

// ladder stuff
$settings->ladder_max_challenge	= 3;
$settings->ladder_win = 1;
$settings->ladder_draw = 0.5;
$settings->ladder_lose = 0;

// community stuff
$settings->community_default = "gaming";

// discussion stuff
$settings->discussion_key_length = 6;
$settings->discussion_topic_key_length = 8;

?>
