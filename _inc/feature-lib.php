<?php

function feature_list($bitmask) {
	global $site;
	
	$sql = "SELECT * 
			FROM feature
			WHERE value & ? > 0
			ORDER BY sort_order";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $bitmask, PDO::PARAM_INT);
	$q->execute();
	
	while ($r = $q->fetchAll(PDO::FETCH_OBJ)) {
		return $r;
	}
}

function feature_list_all() {
	global $site;
	
	$sql = "SELECT * 
			FROM feature
			WHERE disabled = 0
			ORDER BY sort_order";
	$q = $site->db->prepare($sql);
	$q->execute();
	
	while ($r = $q->fetchAll(PDO::FETCH_OBJ)) {
		return $r;
	}
}

function feature_define() {
	global $db;
	$replace = array("-", " ", "/");
	try {
		$sql = "SELECT * FROM feature";
		$q = $db->query($sql);
		$r = $q->fetchAll(PDO::FETCH_OBJ);
		
		foreach ((array) $r as $feature){
			$f = strtoupper(str_replace($replace, "_", $feature->name));
			define("FEATURE_$f", $feature->value);
			define("FEATURE_{$f}_URL", $feature->name_url);
		}
	} catch (PDOException $e) {
		die_gracefully("<p>Problems defining features</p><p><strong>{$e->getMessage()}</strong></p>");
	}
}
?>