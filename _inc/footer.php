	
	<script>
		toastr.options = {
			"closeButton": false,
			"debug": false,
			"newestOnTop": false,
			"progressBar": false,
			"positionClass": "toast-bottom-center",
			"preventDuplicates": false,
			"onclick": null,
			"showDuration": "150",
			"hideDuration": "1000",
			"timeOut": "2000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}
		//toastr.info("This is a message <img src=\"http://news.bbcimg.co.uk/media/images/71977000/jpg/_71977649_71825880.jpg\" />", "Toaster Popup");
		//toastr["success"]("Confirmation email sent!");
	</script>
	<!-- <div id="toast"></div> -->
</body>
</html>