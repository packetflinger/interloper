<?php

function user_auth($id, $pass){
	global $site;
	$p = urlencode($pass);
	$q = $site->db->prepare("SELECT password FROM user_password WHERE user = ? order by date_set DESC LIMIT 1");
	$q->bindValue(1, $id, PDO::PARAM_INT);
	$q->execute();
	
	while ($r = $q->fetch(PDO::FETCH_OBJ)) {
		if ($r->password == crypt($pass, $r->password)){
			user_auth_log($id);
			return true;
		} else {
			return false;
		}
	}
}

function user_auth_log($uid) {
	global $site;
	
	$sql = "UPDATE user SET logincount = logincount + 1, date_lastlogin = NOW() WHERE id = ? LIMIT 1";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $uid, PDO::PARAM_INT);
	$q->execute();
}

function user_create($email){
	global $site;
	$userid = uniqid();
	$token = md5(mt_rand());
	
	try {
		db_begin();
		$sql = "INSERT INTO user (
					userid, email, date_registered, register_ip, 
					register_hostname, timezone, language, 
					reset_token, date_resetrequest
				) VALUES (
					?, ?, NOW(), ?,
					?, ?, ?, 
					?, NOW()
				)";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $userid, PDO::PARAM_STR);
		$q->bindValue(2, $email, PDO::PARAM_STR);
		$q->bindValue(3, $_SERVER['REMOTE_ADDR'], PDO::PARAM_STR);
		$q->bindValue(4, gethostbyaddr($_SERVER['REMOTE_ADDR']), PDO::PARAM_STR);
		$q->bindValue(5, $site->settings->user_default_tz, PDO::PARAM_INT);
		$q->bindValue(6, $site->settings->user_default_lang, PDO::PARAM_STR);
		$q->bindValue(7, $token, PDO::PARAM_STR);
		
		$q->execute();
		$id = $site->db->lastInsertId();
		
		$p = new StdClass();
		$p->community = 0;
		$p->user = $id;
		$p->name = "unnamedplayer" . $id;
		$p->name_url = "unnamedplayer" . $id;
		community_add_player($p);
		
		db_commit();
		return $id;
	} catch (PDOException $e) {
		db_die(__FUNCTION__ . $e->getMessage());
	}
}

function user_define() {
	define("LOG_LOGIN", 1);
	define("LOG_COMMENT", 2);
	define("LOG_COMMUNITY_CREATE", 3);
	define("LOG_COMMUNITY_JOIN", 4);
	define("LOG_COMMUNITY_PART", 5);
	define("LOG_JOIN", 6);
}

function user_get($uid){
	global $site;
	
	$c = $site->settings->community_default;
	$c = (isset($site->get->community)) ? $site->get->community : $c;
	$c = (isset($site->post->community)) ? $site->post->community : $c;
	$community = community_lookup($c);
	
	$sql = "SELECT u.*, t.name AS zonename, t.zone, c.name AS cname, c.flag
			FROM user u
			JOIN timezone t ON t.id = u.timezone
			JOIN country c ON u.country = c.id
			WHERE u.id = ? 
			LIMIT 1";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, trim($uid), PDO::PARAM_INT);
	$q->execute();
	
	while ($r1 = $q->fetch(PDO::FETCH_OBJ)) {
		
		// see if user has a player linked with this community
		$sql = "SELECT * 
				FROM player 
				WHERE (community = ? OR community = 0) 
					AND user = ?
				ORDER BY community DESC";
		$q = $site->db->prepare($sql);
		$q->bindValue(1, $community->id, PDO::PARAM_INT);
		$q->bindValue(2, trim($uid), PDO::PARAM_INT);
		$q->execute();
		while ($r = $q->fetch(PDO::FETCH_OBJ)) {
			$r1->player_id = $r->id;
			$r1->name = $r->name;
			$r1->name_url = $r->name_url;
			return $r1;
		}
		
		$r1->player_id = 0;
		$r1->name = "unnamedplayer";
		$r1->name_url = "unnamedplayer";
		return $r1;
	}

	return null;
}

function user_get_by_email($email){
	global $site;
	$sql = "SELECT 
				u.*,
				p.id AS player_id,
				p.name AS player_name,
				p.name_url AS player_name_url,
				c.name AS cname,
				c.flag,
				t.zone
			FROM user u
			JOIN player p ON p.user = u.id
			JOIN country c ON u.country = c.id
			JOIN timezone t ON t.id = u.timezone
			WHERE p.community = 0
				AND u.email = ?
			LIMIT 1";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, trim($email), PDO::PARAM_STR);
	$q->execute();
	
	while ($r = $q->fetch(PDO::FETCH_OBJ)) {
		return $r;
	}
	
	return null;
}

function user_get_by_player_id($pid){
	global $site;
	$sql = "SELECT 
				u.*,
				p.id AS player_id,
				p.name AS player_name,
				p.name_url AS player_name_url,
				c.name AS cname,
				c.flag,
				t.zone
			FROM user u
			JOIN player p ON p.user = u.id
			JOIN country c ON u.country = c.id
			JOIN timezone t ON t.id = u.timezone
			WHERE p.id = ? 
			LIMIT 1";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, trim($pid), PDO::PARAM_INT);
	$q->execute();
	
	while ($r = $q->fetch(PDO::FETCH_OBJ)) {
		return $r;
	}
	
	return null;
}

function user_get_by_name($name){
	global $site;
	$sql = "SELECT 
				u.*,
				p.id AS player_id,
				p.name AS player_name,
				p.name_url AS player_name_url,
				c.name AS cname,
				c.flag,
				t.zone
			FROM user u
			JOIN player p ON p.user = u.id
			JOIN country c ON u.country = c.id
			JOIN timezone t ON t.id = u.timezone
			WHERE p.name = ? 
			LIMIT 1";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, trim($name), PDO::PARAM_STR);
	$q->execute();
	
	while ($r = $q->fetch(PDO::FETCH_OBJ)) {
		return $r;
	}
	
	return null;
}

function user_get_by_urlname($name){
	global $site;
	$sql = "SELECT 
				u.*,
				p.id AS player_id,
				p.name AS player_name,
				p.name_url AS player_name_url,
				c.name AS cname,
				c.flag,
				t.zone
			FROM user u
			JOIN player p ON p.user = u.id
			JOIN country c ON u.country = c.id
			JOIN timezone t ON t.id = u.timezone
			WHERE p.name_url = ? 
			LIMIT 1";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, trim($name), PDO::PARAM_STR);
	$q->execute();
	
	while ($r = $q->fetch(PDO::FETCH_OBJ)) {
		return $r;
	}
	
	return null;
}

function user_get_names($userobj){
	global $site;
	$sql = "SELECT 
				p.*,
				COALESCE(c.name, 'All Communities') AS community_name
			FROM player p
			LEFT OUTER JOIN community c ON c.id = p.community
			WHERE p.user = ?";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, trim($userobj->id), PDO::PARAM_STR);
	$q->execute();
	
	while ($r = $q->fetchAll(PDO::FETCH_OBJ)) {
		return $r;
	}
	
	return null;
}

function user_get_names_list($userobj, $rich=1) {
	global $site;
	$names = user_get_names($userobj);
	$len = sizeof($names);
	$buf = "";
	for ($i=0; $i<$len; $i++) {
		if ($rich) {
			$buf .= "<span title=\"{$names[$i]->community_name}\">{$names[$i]->name}</span>";
		} else {
			$buf .= $names[$i]->name;
		}
		
		if ($i != $len - 1) {
			$buf .= ", ";
		}
	}
	return $buf;
}

function user_get_players($userid) {
	global $site;
	$sql = "SELECT * FROM player WHERE user = ? ORDER BY community ASC";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $userid, PDO::PARAM_INT);
	$q->execute();
	
	while ($r = $q->fetchAll(PDO::FETCH_OBJ)) {
		return $r;
	}
}


function user_is_logged_in(){
	global $site;
	return isset($site->user->id) && is_numeric($site->user->id) && $site->user->id > 0;
}

function user_is_valid_id($id){
	return isset($id) && is_numeric($id) && $id > 0;
}

function user_list($offset, $count) {
	global $site;
	
	$sql = "SELECT u.id, u.userid, u.date_registered, u.avatar, u.email, c.name AS cname, c.flag, p.name, p.name_url
			FROM user u
			JOIN country c ON c.id = u.country
			JOIN player p ON p.user = u.id
			WHERE u.confirmed = 1 AND u.disabled = 0
			LIMIT ? OFFSET ?";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $count, PDO::PARAM_INT);
	$q->bindValue(2, $offset, PDO::PARAM_INT);
	$q->execute();
	
	while ($r = $q->fetchAll(PDO::FETCH_OBJ)) {
		return $r;
	}
}

function user_log($log) {
	global $site;
	
}

function user_login_required() {
	global $site;
	
	if (!user_is_logged_in()) {
		redirect($site->settings->uri_login . "?r=" . get_return_url());
	}
}

function user_logout() {
	setcookie(session_name(), '', 100);
	session_unset();
	$_SESSION = array();
	session_destroy();
}

function user_setpassword($id, $p) {
	global $site;
	
	$sql = "INSERT INTO user_password (user, password, date_set) VALUES (?, ?, NOW())";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, $id, PDO::PARAM_INT);
	$q->bindValue(2, crypt(urlencode($p)), PDO::PARAM_STR);
	$q->execute();
	
	$sql = "UPDATE user SET 
				confirmed = 1,
				reset_token = ''
			WHERE id = ? 
			LIMIT 1";
	$q = $site->db->prepare($sql);
	$q->bindValue(1, crypt(urlencode($p)), PDO::PARAM_STR);
	$q->bindValue(2, $id, PDO::PARAM_INT);
	$q->execute();	
}
?>