<?php

function admin_community_general() {
	global $site;
?>
	<style>
		.row { margin-bottom: 20px;}
	</style>
	<div class="panel panel-default">
		<div class="panel-heading">General Community Options</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-xl-1 col-lg-2 col-md-4 col-sm-4">Name</div>
				<div class="col-xl-11 col-lg-10 col-md-8 col-sm-8"><input class="form-control" type="text" id="com_name" value="<?=$site->community->name?>"/></div>
			</div>
			<div class="row">
				<div class="col-xl-1 col-lg-2 col-md-4 col-sm-4">URL Name</div>
				<div class="col-xl-11 col-lg-10 col-md-8 col-sm-8"><input class="form-control" type="text" id="com_name_url" value="<?=$site->community->name_url?>"/></div>
			</div>
			<div class="row">
				<div class="col-xl-1 col-lg-2 col-md-4 col-sm-4">Brief Description</div>
				<div class="col-xl-11 col-lg-10 col-md-8 col-sm-8"><textarea class="form-control" id="com_description"><?=$site->community->description?></textarea></div>
			</div>
			<div class="row">
				<div class="col-xl-1 col-lg-2 col-md-4 col-sm-4">Disabled</div>
				<div class="col-xl-11 col-lg-10 col-md-8 col-sm-8"><input class="form-control" type="checkbox" id="com_disabled"<?=$dis?>/></div>
			</div>
		</div>
	</div>
	<script>
		jQuery(document).ready(function($) {	
			$("#com_description").summernote({
				height: '100px',
				callbacks: {
					onBlur: function(event) {
						$.post("/async", {op: 'Admin.Com.SetDescription', description: $('#com_description').summernote('code'), id: <?=$site->community->id?>}, function(result) {
							var r = JSON.parse(result);
							if (r.value == 1) {
								toastr.success(r.message);
							} else {
								toastr.error(r.message);
							}
						});
					}
				}
			});
			$("#com_name").blur(function() {
				$.post("<?=url($site->settings->uri_rpc)?>", {op: 'Admin.Com.SetName', name: $("#com_name").val(), id: <?=$site->community->id?>}, function(result) {
					var r = JSON.parse(result);
					if (r.value == 1) {
						toastr.success(r.message);
					} else {
						toastr.error(r.message);
					}
				});
			});
			
			$("#com_name_url").blur(function() {
				$.post("<?=url($site->settings->uri_rpc)?>", {op: 'Admin.Com.SetNameURL', name: $("#com_name_url").val(), id: <?=$site->community->id?>}, function(result) {
					var r = JSON.parse(result);
					if (r.value == 1) {
						toastr.success(r.message);
					} else {
						toastr.error(r.message);
					}
				});
			});
			
			$("#com_disabled").change(function() {
				$.post("<?=url($site->settings->uri_rpc)?>", {op: 'Admin.Com.SetDisabled', disabled: $("#com_disabled").is(':checked') ? 1 : 0, id: <?=$site->community->id?>}, function(result) {
					var r = JSON.parse(result);
					if (r.value == 1) {
						toastr.success(r.message);
					} else {
						toastr.error(r.message);
					}
				});
			});
		});
	</script>
<?php	
}

function admin_community_features() {
	global $site;
	$f = feature_list_all();
?>
	<div class="panel panel-default">
		<div class="panel-heading">Community Features</div>
		<div class="panel-body">
			<form action="<?=admin_url()?>" method="post">
				<div class="form-group">
<?php foreach ((array) $f as $feature) { 
		$checked = ((int)$feature->value & $site->community->features) ? " checked=\"checked\"" : "";
?>
					<div class="row">
						<div class="col-md-4"><input type="checkbox" name="features[]" value="<?=$feature->value?>"<?=$checked?>> <?=$feature->name?></div>
					</div>
<?php } ?>	
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary">Save</button>
					<input type="hidden" name="op" value="Admin.Features.Save">
					<input type="hidden" name="community" value="<?=$site->community->id?>">
				</div>
			</form>
		</div>
	</div>
<?php
}

function admin_community_staff() {
	global $site;
	$m = community_member_list($site->community->id, 0, 0, 50);
?>
	<div class="panel panel-default">
		<div class="panel-heading">Important Players</div>
		<div class="panel-body">
			<form action="<?=admin_url()?>" method="post">
				<div class="form-group">
					<div class="row">
						<div class="col-header col-xs-4 col-sm-3 col-md-2 col-lg-2 col-xl-2">Player</div>
						<div class="col-header col-xs-8 col-sm-9 col-md-10 col-lg-10 col-xl-10">Role</div>
					</div>
<?php foreach ((array) $m as $member) { 
		$disable = ($site->user->player_id == $member->player) ? " disabled=\"disabled\"" : "";
?>
					<div class="row">
						<div class="col-xs-4 col-sm-3 col-md-2 col-lg-2 col-xl-2"><input type="checkbox" name="members[]" value="<?=$member->id?>"<?=$disable?>> <a href="<?=sprintf("%s/%s", $site->settings->uri_user, $member->name_url)?>"><?=$member->name?></a></div>
						<div class="col-xs-8 col-sm-9 col-md-10 col-lg-10 col-xl-10"><?=community_role_to_string($member->level_mask)?> <a onclick="loadRoles('<?=$member->name?>', <?=$member->player?>, <?=$member->id?>, <?=$member->level_mask?>, <?=$site->community->id?>)" href="#staff-roles" data-toggle="modal"><span class="glyphicon glyphicon-edit"></span></a></div>
					</div>
<?php } ?>	
				</div>
				<div class="form-group">
					<button type="submit" name="removestaff" value="1" class="btn btn-xs btn-warning">Remove</button>
					<button type="button" class="btn btn-xs btn-warning" data-toggle="modal" data-target="#staff-add">Add</button>
				</div>
				<input type="hidden" name="op" value="community.staff.update">
				<input type="hidden" name="community" value="<?=$site->community->id?>">
				<input type="hidden" name="return" value="<?=get_return_url()?>">
			</form>
		</div>
	</div>
	
	<script type="text/javascript">
		$(document).ready(function($) {
			$("#addstaff").magicSuggest({
				allowFreeEntries: false,
				data: '<?=$site->settings->uri_rpc?>',
				dataUrlParams: {op: 'NameLookup'},
				hideTrigger: true,
				highlight: false,
				maxDropHeight: 145,
				method: 'get',
				minChars: 2
			});
		})
		
		function loadRoles(name,playerid,id,mask,community) {			
			$("#memberid").val(id);
			$("#rolestaffname").html(name);
			$(".role-item").each(function(index) {
				
				// unset everything
				$(this).prop("checked", false);
				$(this).prop("readonly", false);
				
				if ($(this).val() & mask){
					$(this).prop("checked", true);
				}
				
				if (playerid == <?=$site->user->player_id?> && $(this).val() == <?=COMMUNITY_ADMIN?>) {
					$(this).prop("readonly", true);
				}
			});
		}
	</script>

	<div id="staff-roles" class="modal fade">
		<form role="form" action="<?=admin_url()?>" method="post">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Community roles for <span id="rolestaffname">...</span></h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<ul class="role-list">
								<li><input class="role-item" id="roleadmin" type="checkbox" name="staffroles[]" value="<?=COMMUNITY_ADMIN?>"> Admin</li>
								<li><input class="role-item" id="roleadmin" type="checkbox" name="staffroles[]" value="<?=COMMUNITY_MODERATOR?>"> Moderator</li>
								<li><input class="role-item" id="roleadmin" type="checkbox" name="staffroles[]" value="<?=COMMUNITY_CONTRIBUTOR?>"> Contributor</li>
								<li><input class="role-item" id="roleadmin" type="checkbox" name="staffroles[]" value="<?=COMMUNITY_HELPER?>"> Helper</li>
								<li><input class="role-item" id="roleadmin" type="checkbox" name="staffroles[]" value="<?=COMMUNITY_MEMBER?>"> Member</li>
							</ul>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary" name="save">Save</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					</div>
				</div>
			</div>
			<input type="hidden" name="op" value="community.staff.roles.save"/>
			<input id="memberid" type="hidden" name="memberid" value="0"/>
			<input type="hidden" name="return" value="<?=get_return_url()?>"/>
		</form>
	</div>
	
	<div id="staff-add" class="modal fade">
		<form role="form" action="<?=admin_url()?>" method="post">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Add a community staff member</h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<div class="row">
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-9 col-xl-9">
									<input id="addstaff" class="form-control" name="members" type="text" placeholder="Add Players"/>
								</div>
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-3 col-xl-3">
					
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<ul class="role-list">
								<li><input class="role-item" id="roleadmin" type="checkbox" name="staffroles[]" value="<?=COMMUNITY_ADMIN?>"> Admin</li>
								<li><input class="role-item" id="roleadmin" type="checkbox" name="staffroles[]" value="<?=COMMUNITY_MODERATOR?>"> Moderator</li>
								<li><input class="role-item" id="roleadmin" type="checkbox" name="staffroles[]" value="<?=COMMUNITY_CONTRIBUTOR?>"> Contributor</li>
								<li><input class="role-item" id="roleadmin" type="checkbox" name="staffroles[]" value="<?=COMMUNITY_HELPER?>"> Helper</li>
								<li><input class="role-item" id="roleadmin" type="checkbox" name="staffroles[]" value="<?=COMMUNITY_MEMBER?>"> Member</li>
							</ul>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary" name="add">Add</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					</div>
				</div>
			</div>
			<input type="hidden" name="op" value="community.staff.add"/>
			<input type="hidden" name="community" value="<?=$site->community->id?>"/>
			<input type="hidden" name="return" value="<?=get_return_url()?>"/>
		</form>
	</div>
<?php
}

function admin_community_league() {
	global $site;
?>
<div class="panel panel-default">
	<div class="panel-heading">Leagues <span class="pull-right"><button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#createleague"><span class="glyphicon glyphicon-plus"></span></button></span></div>
	<div class="panel-body" id="leagues"></div>
</div>

<script>
	jQuery(document).ready(function($) {	
		refresh_leagues();
	});
	
	function refresh_leagues() {
		$("#leagues").html("<img src=\"<?=$site->settings->uri_loading?>\">");
		$.get("<?=url($site->settings->uri_rpc)?>?op=ListLeagues&c=<?=$site->community->id?>", function(data) {
			var r = JSON.parse(data);
			$("#leagues").html("");
			if (r.resultset) {
				var l = r.resultset;
				for (var i=0; i<l.length; i++) {
					$("#leagues").append("<a href=\"/<?=$site->community->name_url?>/admin?op=league&sop=setup&k=" + l[i].access_key + "\">" + l[i].name + "</a><br>");
				}
			}
		});
	}
</script>

<div id="createleague" class="modal fade">
	<form role="form" action="<?=url($site->settings->uri_rpc)?>" method="post">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Create a League</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<p>Full Name<br><input class="form-control" id="fullname" name="fullname" placeholder="Super Awesome Duel League" required></p>
						<p>Short Name<br><input class="form-control" id="shortname" name="shortname" placeholder="SADL" required></p>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" name="postcomment">Create</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
		<input type="hidden" name="community_id" value="<?=$site->community->id?>"/>
		<input type="hidden" name="op" value="Admin.League.Create"/>
	</form>
</div>
<?php
}


function admin_community_save_description() {
	global $site;
	
	if ($site->community->member >= LEVEL_ADMIN) {
		$com = community_get($site->post->id);
		$com->description = $site->post->description;
		$r = community_edit($com);
		if ($r->value) {
			$r->message = "Community description saved.";
		}
		echo json_encode($r);
	} else {
		echo json_encode(return_obj_fail("Permission denied"));
	}
}

function admin_community_save_disabled() {
	global $site;
	
	if ($site->community->member >= LEVEL_ADMIN) {
		$com = community_get($site->post->id);
		$com->disabled = $site->post->disabled;
		$r = community_edit($com);
		if ($r->value) {
			$r->message = "Community disablement saved.";
		}
		echo json_encode($r);
	} else {
		echo json_encode(return_obj_fail("Permission denied"));
	}
}


function admin_community_save_name() {
	global $site;
	
	if ($site->community->member >= LEVEL_ADMIN) {
		$com = community_get($site->post->id);
		$com->name = $site->post->name;
		$r = community_edit($com);
		if ($r->value) {
			$r->message = "Community name saved.";
		}
		echo json_encode($r);
	} else {
		echo json_encode(return_obj_fail("Permission denied"));
	}
}

function admin_community_save_name_url() {
	global $site;
	
	if ($site->community->member >= LEVEL_ADMIN) {
		$com = community_get($site->post->id);
		$com->name_url = $site->post->name;
		$r = community_edit($com);
		if ($r->value) {
			$r->message = "Community URL name saved.";
		}
		echo json_encode($r);
	} else {
		echo json_encode(return_obj_fail("Permission denied"));
	}
}

function admin_denied2() {
	global $site;
	die_gracefully("Nope...", "You're account doesn't have access to this");
}

function admin_index() {
	global $site;

	include("header.php");
	navigation();
?>
	<div class="container">
		<h1 class="page-title">Community // <?=$site->community->name?></h1>
		<div class="row">

			<div class="col-xl-1 col-lg-2 col-md-3">
				<?=admin_navigation(1)?>
			</div>
		
			<div class="col-xl-11 col-lg-10 col-md-9">

<?php
if (isset($site->get->op)) {
	switch ($site->get->op) {
		case "general":
			admin_community_general();
			break;
		case "features":
			admin_community_features();
			break;
		case "staff":
			admin_community_staff();
			break;
		case "league":
			if (isset($site->get->k)) {
				switch ($site->get->sop) {
					case "setup":
						admin_league_edit();
						break;
					case "groups":
						if (isset($site->get->id) && is_numeric($site->get->id)) {
							admin_league_group_view();
						} else {
							admin_league_group();
						}
						break;
					case "staff":
						admin_league_staff();
						break;
					case "members":
						admin_league_members();
						break;
					case "matches":
					if (isset($site->get->mk)) {
						admin_league_match_view();
						break;
					} else {
						admin_league_matches();
						break;
					}
				}
			} else {	
				admin_community_league();
			}
			break;
	}
}
?>
			</div>
		</div>
	</div>
<?php
	include("footer.php");
}

function admin_league_staff() {
	global $site;
	$l = league_get_by_key($site->get->k);
	
?>

<div class="panel panel-default">
	<div class="panel-heading">League Staff - <?=$l->name?><button title="Add staff" data-toggle="modal" data-target="#addstaff" class="pull-right btn btn-primary btn-xs"><span class="glyphicon glyphicon-plus"></span></button></div>
	<div class="panel-body" id="leaguestaff"></div>
</div>

<style>
	.ui-autocomplete-loading {
		background: white url("<?=$site->settings->uri_loading?>") right center no-repeat;
	}
	
	/* Required when using autocomplete in modals, so it shows up on top of the modal */
	.ui-autocomplete {
		z-index: 5000;
	}
</style>

<div id="addstaff" class="modal fade">
	<form role="form" action="<?=url($site->settings->uri_rpc)?>" method="post">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Add Staff</h4>
				</div>
				<div class="modal-body">
					<p>Player Name</p>
					<p><input id="newstaffname" name="staffname" class="form-control" type="text" placeholder="Start typing a name..."/></p>
					
					<p>Permission Level</p>
					<p><select id="newstaffpermission" name="stafflevel" class="form-control"><?=league_staff_level_list(50000)?></select></p>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" name="addleaguestaff">Add</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
		<input type="hidden" name="op" value="Admin.League.Staff.Add"/>
		<input type="hidden" name="league_id" value="<?=$l->id?>"/>
		<input type="hidden" name="return" value="<?=get_return_url()?>"/>
	</form>
</div>

<script>
	$(document).ready(function($) {	
		refresh_staff();
		
		$("#newstaffname").autocomplete({
			source: '<?=url($site->settings->uri_rpc)?>?op=NameLookup',
			minLength: 3
		});
		
		$(".deletestaff a").click(function(){
			alert($(".deletestaff").data("removeid"));
		});
	});
	
	function refresh_staff() {
		$("#leaguestaff").html("<img src=\"<?=$site->settings->uri_loading?>\">");
		$.get("<?=url($site->settings->uri_rpc)?>?op=ListLeagueStaff&id=<?=$l->id?>", function(data) {
			var r = JSON.parse(data);
			$("#leaguestaff").html("");
			if (r.resultset) {
				var l = r.resultset;
				$("#leaguestaff").append("<div class=\"row\">");
				$("#leaguestaff").append("<div class=\"col-xl-3 col-lg-3 col-md-4 col-sm-6 col-xs-6 staff-header\">Player</div>");
				$("#leaguestaff").append("<div class=\"col-xl-9 col-lg-9 col-md-8 col-sm-6 col-xs-6 staff-header\">Role</div>");
				$("#leaguestaff").append("</div>");
				for (var i=0; i<l.length; i++) {
					$("#leaguestaff").append("<div class=\"row\">");
					$("#leaguestaff").append("<div class=\"col-xl-3 col-lg-3 col-md-4 col-sm-6 col-xs-6\"><a href=\"<?=$site->settings->uri_user?>/" + l[i].name_url + "\">" + l[i].name + "</a></div>");
					if (l[i].rolename == "creator") {
						$("#leaguestaff").append("<div class=\"col-xl-9 col-lg-9 col-md-8 col-sm-6 col-xs-6\">" + l[i].rolename + "</div>");
					} else {
						$("#leaguestaff").append("<div class=\"col-xl-9 col-lg-9 col-md-8 col-sm-6 col-xs-6\">" + l[i].rolename + " <a title=\"Remove " + l[i].name +"\" onClick=\"deletestaff("+l[i].id+")\" href=\"#\"><span class=\"glyphicon glyphicon-remove bad\"></span></a></div>");
					}
					$("#leaguestaff").append("</div>");
				}
			}
		});
	}
	
	function deletestaff(staffid) {
		$.post("<?=url($site->settings->uri_rpc)?>", {op: 'Admin.League.Staff.Delete', id: staffid}, function(result) {
			var r = JSON.parse(result);
			if (r.value == 1) {
				toastr.success(r.message);
			} else {
				toastr.error(r.message);
			}
		});
		
		refresh_staff();
	}
	
</script>
<?php
}

function admin_navigation($h) {
	global $site;
	$op = (isset($site->get->op)) ? $site->get->op : "";
	$sok = (isset($site->get->sop)) ? $site->get->sop : "";
?>

<div class="panel panel-default">
	<div class="panel-heading">Community Settings</div>
	<div class="panel-body">
		<ul>
			<li><a class="<?=($op == 'general') ? "nav-selected" : ""?>" href="<?=admin_url()?>?op=general">General</a></li>
			<li><a class="<?=($op == 'features') ? "nav-selected" : ""?>" href="<?=admin_url()?>?op=features">Features</a></li>
			<li><a class="<?=($op == 'staff') ? "nav-selected" : ""?>" href="<?=admin_url()?>?op=staff">Staff</a></li>
			<li><a class="<?=($op == 'league') ? "nav-selected" : ""?>" href="<?=admin_url()?>?op=league">Leagues</a></li>
			<li><a class="<?=($op == 'ladder') ? "nav-selected" : ""?>" href="<?=admin_url()?>?op=ladder">Ladders</a></li>
			<li><a class="<?=($op == 'map') ? "nav-selected" : ""?>" href="<?=admin_url()?>?op=map">Maps</a></li>
			<li><a class="<?=($op == 'demo') ? "nav-selected" : ""?>" href="<?=admin_url()?>?op=demo">Demos</a></li>
			<li><a class="<?=($op == 'config') ? "nav-selected" : ""?>" href="<?=admin_url()?>?op=config">Config/PAK</a></li>
		</ul>
	</div>
</div>
<?php
	if ($op == "league" && $sok != "") {
		admin_navigation_league();
	}
}

function admin_navigation_league() {
	global $site;
	$sop = (isset($site->get->sop)) ? $site->get->sop : "";
?>
<div class="panel panel-default">
	<div class="panel-heading">League Settings</div>
	<div class="panel-body">
		<ul>
			<li><a class="<?=($sop == 'setup') ? "nav-selected" : ""?>" href="<?=admin_url()?>?op=league&amp;sop=setup&amp;k=<?=$site->get->k?>">Setup</a></li>
			<li><a class="<?=($sop == 'staff') ? "nav-selected" : ""?>" href="<?=admin_url()?>?op=league&amp;sop=staff&amp;k=<?=$site->get->k?>">Staff</a></li>
			<li><a class="<?=($sop == 'members') ? "nav-selected" : ""?>" href="<?=admin_url()?>?op=league&amp;sop=members&amp;k=<?=$site->get->k?>">Members</a></li>
			<li><a class="<?=($sop == 'groups') ? "nav-selected" : ""?>" href="<?=admin_url()?>?op=league&amp;sop=groups&amp;k=<?=$site->get->k?>">Groups</a></li>
			<li><a class="<?=($sop == 'matches') ? "nav-selected" : ""?>" href="<?=admin_url()?>?op=league&amp;sop=matches&amp;k=<?=$site->get->k?>">Matches</a></li>
			<li><a class="<?=($sop == 'playoffs') ? "nav-selected" : ""?>" href="<?=admin_url()?>?op=league&amp;sop=playoffs&amp;k=<?=$site->get->k?>">Playoffs</a></li>
		</ul>
	</div>
</div>
<?php
}

function admin_url() {
	global $site;
	echo "/{$site->community->name_url}/admin";
}

function admin_league_edit() {
	global $site;
	
	$league = league_get_by_key($site->get->k);
	$locked = ($league->locked) ? " disabled" : "";
	$type = select_values(array("score-based", "time-based"), $league->league_type);
	$teamtype = select_values(array("single players", "multi-player teams"), $league->team_type);
	$disabled = select_values(array("no", "yes"), $league->disabled);
	$suopen = select_values(array("no", "yes"), $league->enrollment_open);
	$inviteonly = select_values(array("no", "yes"), $league->invite_only);
	$invisible = select_values(array("no", "yes"), $league->invisible);
?>
<style>
	.row { margin-bottom: 20px;}
	.panel-heading {
		text-overflow: ellipsis;
	}
</style>

<div class="panel panel-default">
	<div class="panel-heading">Edit League - <?=$league->name?><button class="pull-right btn btn-primary btn-xs" data-toggle="modal" data-target="#deleteleague"><span class="glyphicon glyphicon-remove"></span></button></div>
	<div class="panel-body">
		<div class="row">
			<div class="col-xl-1 col-lg-2 col-md-4 col-sm-4">Name</div>
			<div class="col-xl-11 col-lg-10 col-md-8 col-sm-8"><input class="form-control" type="text" id="league_name" value="<?=$league->name?>"/></div>
		</div>
		<div class="row">
			<div class="col-xl-1 col-lg-2 col-md-4 col-sm-4">URL Name</div>
			<div class="col-xl-11 col-lg-10 col-md-8 col-sm-8"><input class="form-control" type="text" id="league_name_url" value="<?=$league->name_url?>"/></div>
		</div>
		<div class="row">
			<div class="col-xl-1 col-lg-2 col-md-4 col-sm-4">Short Name</div>
			<div class="col-xl-11 col-lg-10 col-md-8 col-sm-8"><input class="form-control" type="text" id="league_name_short" value="<?=$league->name_short?>"/></div>
		</div>
		<div class="row">
			<div class="col-xl-1 col-lg-2 col-md-4 col-sm-4">League Type</div>
			<div class="col-xl-11 col-lg-10 col-md-8 col-sm-8"><select <?=$locked?> class="form-control" id="league_type"><?=$type?></select></div>
		</div>
		<div class="row">
			<div class="col-xl-1 col-lg-2 col-md-4 col-sm-4">Team Type</div>
			<div class="col-xl-11 col-lg-10 col-md-8 col-sm-8"><select <?=$locked?> class="form-control" id="league_teamtype"><?=$teamtype?></select></div>
		</div>
		<div class="row">
			<div class="col-xl-1 col-lg-2 col-md-4 col-sm-4">Disabled</div>
			<div class="col-xl-11 col-lg-10 col-md-8 col-sm-8"><select class="form-control" id="league_disabled"><?=$disabled?></select></div>
		</div>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">Enrollment</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-xl-1 col-lg-2 col-md-4 col-sm-4">Sign-ups Open</div>
			<div class="col-xl-11 col-lg-10 col-md-8 col-sm-8"><p>Signups will remain open as long as this is set to yes.</p><select class="form-control" id="league_suopen"><?=$suopen?></select></div>
		</div>
		<div class="row">
			<div class="col-xl-1 col-lg-2 col-md-4 col-sm-4">Signup Date Range</div>
			<div class="col-xl-11 col-lg-10 col-md-8 col-sm-8 form-inline">
				<p>Signups will remain open between these dates.</p>
				<input class="form-control" type="text" id="league_sustart" value="<?=$league->enrollment_start?>"/> to
				<input class="form-control" type="text" id="league_suend" value="<?=$league->enrollment_end?>"/>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-1 col-lg-2 col-md-4 col-sm-4">Signup Team Limit</div>
			<div class="col-xl-11 col-lg-10 col-md-8 col-sm-8"><p>Use 0 to force no limit</p><input class="form-control" type="text" id="league_sulimit" value="<?=$league->enrollment_limit?>"/></div>
		</div>
		<div class="row">
			<div class="col-xl-1 col-lg-2 col-md-4 col-sm-4">Invite only</div>
			<div class="col-xl-11 col-lg-10 col-md-8 col-sm-8"><select class="form-control" id="league_inviteonly"><?=$inviteonly?></select></div>
		</div>
		<div class="row">
			<div class="col-xl-1 col-lg-2 col-md-4 col-sm-4">Invisible</div>
			<div class="col-xl-11 col-lg-10 col-md-8 col-sm-8"><select class="form-control" id="league_invisible"><?=$invisible?></select></div>
		</div>
	</div>
</div>

<div id="deleteleague" class="modal fade">
	<form role="form" action="<?=url($site->settings->uri_rpc)?>" method="post">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">You're about to <strong>delete</strong> this league!!</h4>
				</div>
				<div class="modal-body">
					<p>Are you <em>sure</em> you want to do this?</p>
					<ul>
						<li>League matches will be removed</li>
						<li>League memberships will be removed</li>
						<li>League player statistics will be removed</li>
						<li>Screenshots and demos for this league will be deleted</li>
					</ul>
					<p>This action permanently deletes data and cannot be undone.</p>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-danger" name="deleteleague">Yes, I understand, do it</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
		<input type="hidden" name="op" value="Admin.League.Delete"/>
		<input type="hidden" name="league_key" value="<?=$site->get->k?>"/>
	</form>
</div>

<script>
	jQuery(document).ready(function($) {	
		$("#league_name").blur(function() {
			$.post("<?=url($site->settings->uri_rpc)?>", {op: 'Admin.League.SetName', name: $("#league_name").val(), id: <?=$league->id?>}, function(result) {
				var r = JSON.parse(result);
				if (r.value == 1) {
					toastr.success(r.message);
				} else {
					toastr.error(r.message);
				}
			});
		});
		
		$("#league_name_short").blur(function() {
			$.post("<?=url($site->settings->uri_rpc)?>", {op: 'Admin.League.SetShortName', name: $("#league_name_short").val(), id: <?=$league->id?>}, function(result) {
				var r = JSON.parse(result);
				if (r.value == 1) {
					toastr.success(r.message);
				} else {
					toastr.error(r.message);
				}
			});
		});
		
		$("#league_name_url").blur(function() {
			$.post("<?=url($site->settings->uri_rpc)?>", {op: 'Admin.League.SetNameURL', name: $("#league_name_url").val(), id: <?=$league->id?>}, function(result) {
				var r = JSON.parse(result);
				if (r.value == 1) {
					toastr.success(r.message);
				} else {
					toastr.error(r.message);
				}
			});
		});
		
		$("#league_type").change(function() {
			$.post("<?=url($site->settings->uri_rpc)?>", {op: 'Admin.League.SetType', type: $("#league_type").val(), id: <?=$league->id?>}, function(result) {
				var r = JSON.parse(result);
				if (r.value == 1) {
					toastr.success(r.message);
				} else {
					toastr.error(r.message);
				}
			});
		});
		
		$("#league_teamtype").change(function() {
			$.post("<?=url($site->settings->uri_rpc)?>", {op: 'Admin.League.SetTeamType', type: $("#league_teamtype").val(), id: <?=$league->id?>}, function(result) {
				var r = JSON.parse(result);
				if (r.value == 1) {
					toastr.success(r.message);
				} else {
					toastr.error(r.message);
				}
			});
		});
		
		$("#league_disabled").change(function() {
			$.post("<?=url($site->settings->uri_rpc)?>", {op: 'Admin.League.SetDisabled', disabled: $("#league_disabled").val(), id: <?=$league->id?>}, function(result) {
				var r = JSON.parse(result);
				if (r.value == 1) {
					toastr.success(r.message);
				} else {
					toastr.error(r.message);
				}
			});
		});
		
		$("#league_suopen").change(function() {
			$.post("<?=url($site->settings->uri_rpc)?>", {op: 'Admin.League.SetEnrollmentOpen', open: $("#league_suopen").val(), id: <?=$league->id?>}, function(result) {
				var r = JSON.parse(result);
				if (r.value == 1) {
					toastr.success(r.message);
				} else {
					toastr.error(r.message);
				}
			});
		});
		
		$("#league_sulimit").blur(function() {
			$.post("<?=url($site->settings->uri_rpc)?>", {op: 'Admin.League.SetEnrollLimit', limit: $("#league_sulimit").val(), id: <?=$league->id?>}, function(result) {
				var r = JSON.parse(result);
				if (r.value == 1) {
					toastr.success(r.message);
				} else {
					toastr.error(r.message);
				}
			});
		});
		
		$('#league_sustart').datepicker({
				autoclose: true,
				todayHighlight: true,
				format: "yyyy-mm-dd"
		});
			
		$("#league_sustart").change(function() {
			//alert($("#league_sustart").val());
			$.post("<?=url($site->settings->uri_rpc)?>", {op: 'Admin.League.SetEnrollStart', start: $("#league_sustart").val(), id: <?=$league->id?>}, function(result) {
				var r = JSON.parse(result);
				if (r.value == 1) {
					toastr.success(r.message);
				} else {
					toastr.error(r.message);
				}
			});
		});
		
		$('#league_suend').datepicker({
				autoclose: true,
				todayHighlight: true,
				format: "yyyy-mm-dd"
		});
			
		$("#league_suend").change(function() {
			//alert($("#league_suend").val());
			$.post("<?=url($site->settings->uri_rpc)?>", {op: 'Admin.League.SetEnrollEnd', end: $("#league_suend").val(), id: <?=$league->id?>}, function(result) {
				var r = JSON.parse(result);
				if (r.value == 1) {
					toastr.success(r.message);
				} else {
					toastr.error(r.message);
				}
			});
		});
		
		$("#league_inviteonly").change(function() {
			$.post("<?=url($site->settings->uri_rpc)?>", {op: 'Admin.League.SetInviteOnly', invite: $("#league_inviteonly").val(), id: <?=$league->id?>}, function(result) {
				var r = JSON.parse(result);
				if (r.value == 1) {
					toastr.success(r.message);
				} else {
					toastr.error(r.message);
				}
			});
		});
		
		$("#league_invisible").change(function() {
			$.post("<?=url($site->settings->uri_rpc)?>", {op: 'Admin.League.SetInvisible', invisible: $("#league_invisible").val(), id: <?=$league->id?>}, function(result) {
				var r = JSON.parse(result);
				if (r.value == 1) {
					toastr.success(r.message);
				} else {
					toastr.error(r.message);
				}
			});
		});
	});
</script>
<?php
}

function admin_league_save_name() {
	global $site;
	
	if ($site->community->member >= LEVEL_ADMIN) {
		$l = league_get($site->post->id);
		$l->name = $site->post->name;
		$r = league_edit($l);
		if ($r->value) {
			$r->message = "League name saved.";
		}
		echo json_encode($r);
	} else {
		echo json_encode(return_obj_fail("Permission denied"));
	}
}

function admin_league_save_shortname() {
	global $site;
	
	if ($site->community->member >= LEVEL_ADMIN) {
		$l = league_get($site->post->id);
		$l->name_short = $site->post->name;
		$r = league_edit($l);
		if ($r->value) {
			$r->message = "League short name saved.";
		}
		echo json_encode($r);
	} else {
		echo json_encode(return_obj_fail("Permission denied"));
	}
}

function admin_league_save_urlname() {
	global $site;
	
	if ($site->community->member >= LEVEL_ADMIN) {
		$l = league_get($site->post->id);
		$l->name_url = $site->post->name;
		$r = league_edit($l);
		if ($r->value) {
			$r->message = "League URL name saved.";
		}
		echo json_encode($r);
	} else {
		echo json_encode(return_obj_fail("Permission denied"));
	}
}

function admin_league_group() {
	global $site;
	$l = league_get_by_key($site->get->k);
	$g = group_list($l->id);
?>
<div class="panel panel-default">
	<div class="panel-heading">Groups for <?=$l->name?> <button title="Add a Group" data-toggle="modal" data-target="#addgroup" class="pull-right btn btn-primary btn-xs"><span class="glyphicon glyphicon-plus"></span></button></div>
	<div class="panel-body">
		<ul>
<?php for ($i=0; $i<sizeof($g); $i++) { ?>
			<li><a href="<?=admin_url()?>?op=<?=$site->get->op?>&sop=<?=$site->get->sop?>&k=<?=$site->get->k?>&id=<?=$g[$i]->id?>"><?=$g[$i]->name?></a></li>
			<div id="editgroup<?=$g[$i]->id?>" class="modal fade">
				<form role="form" action="<?=url($site->settings->uri_rpc)?>" method="post">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title">Edit Group</h4>
							</div>
							<div class="modal-body">
								<p>Group Name</p>
								<p><input name="groupname" class="form-control" type="text" value="<?=$g[$i]->name?>" required/></p>
								<p>URL Name</p>
								<p><input name="groupnameurl" class="form-control" type="text" value="<?=$g[$i]->name_url?>" required/></p>
								<p>Sort</p>
								<p><input name="sortorder" class="form-control" type="text" value="<?=$g[$i]->sort_order?>" required/></p>
							</div>
							<div class="modal-footer">
								<input type="submit" class="btn btn-danger pull-left" name="deleteleaguegroup" value="Delete">
								<button type="submit" class="btn btn-primary" name="editleaguegroup">Save</button>
								<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
							</div>
						</div>
					</div>
					<input type="hidden" name="op" value="Admin.League.Group.Edit"/>
					<input type="hidden" name="league_id" value="<?=$l->id?>"/>
					<input type="hidden" name="group_id" value="<?=$g[$i]->id?>"/>
					<input type="hidden" name="return" value="<?=get_return_url()?>"/>
				</form>
			</div>
<?php } ?>
		</ul>
	</div>
</div>

<div id="addgroup" class="modal fade">
	<form role="form" action="<?=url($site->settings->uri_rpc)?>" method="post">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Add Group</h4>
				</div>
				<div class="modal-body">
					<p>Group Name</p>
					<p><input id="newgroupname" name="groupname" class="form-control" type="text" placeholder="Division 1" required/></p>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" name="addleaguegroup">Add</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
		<input type="hidden" name="op" value="Admin.League.Group.Add"/>
		<input type="hidden" name="league_id" value="<?=$l->id?>"/>
		<input type="hidden" name="return" value="<?=get_return_url()?>"/>
	</form>
</div>
<?php
}

function admin_league_save_disabled() {
	global $site;
	
	if ($site->community->member >= LEVEL_ADMIN) {
		$l = league_get($site->post->id);
		$l->disabled = $site->post->disabled;
		$r = league_edit($l);
		if ($r->value) {
			$r->message = "League disablement saved.";
		}
		echo json_encode($r);
	} else {
		echo json_encode(return_obj_fail("Permission denied"));
	}
}

function admin_league_save_enrollmentopen() {
	global $site;
	
	if ($site->community->member >= LEVEL_ADMIN) {
		$l = league_get($site->post->id);
		$l->enrollment_open = $site->post->open;
		$r = league_edit($l);
		if ($r->value) {
			$r->message = "League Enrollment saved.";
		}
		echo json_encode($r);
	} else {
		echo json_encode(return_obj_fail("Permission denied"));
	}
}

function admin_league_save_enrollmentlimit() {
	global $site;
	
	if ($site->community->member >= LEVEL_ADMIN) {
		$l = league_get($site->post->id);
		$l->enrollment_limit = $site->post->limit;
		$r = league_edit($l);
		if ($r->value) {
			$r->message = "League Enrollment Limit saved.";
		}
		echo json_encode($r);
	} else {
		echo json_encode(return_obj_fail("Permission denied"));
	}
}

function admin_league_save_enrollmentstart() {
	global $site;
	
	if ($site->community->member >= LEVEL_ADMIN) {
		$l = league_get($site->post->id);
		$l->enrollment_start = $site->post->start;
		$r = league_edit($l);
		if ($r->value) {
			$r->message = "League Enrollment Start saved.";
		}
		echo json_encode($r);
	} else {
		echo json_encode(return_obj_fail("Permission denied"));
	}
}

function admin_league_save_enrollmentend() {
	global $site;
	
	if ($site->community->member >= LEVEL_ADMIN) {
		$l = league_get($site->post->id);
		$l->enrollment_end = $site->post->end;
		$r = league_edit($l);
		if ($r->value) {
			$r->message = "League Enrollment End saved.";
		}
		echo json_encode($r);
	} else {
		echo json_encode(return_obj_fail("Permission denied"));
	}
}

function admin_league_save_inviteonly() {
	global $site;
	
	if ($site->community->member >= LEVEL_ADMIN) {
		$l = league_get($site->post->id);
		$l->invite_only = $site->post->invite;
		$r = league_edit($l);
		if ($r->value) {
			$r->message = "League Invite only saved.";
		}
		echo json_encode($r);
	} else {
		echo json_encode(return_obj_fail("Permission denied"));
	}
}

function admin_league_save_invisible() {
	global $site;
	
	if ($site->community->member >= LEVEL_ADMIN) {
		$l = league_get($site->post->id);
		$l->invisible = $site->post->invisible;
		$r = league_edit($l);
		if ($r->value) {
			$r->message = "League Invisibility saved.";
		}
		echo json_encode($r);
	} else {
		echo json_encode(return_obj_fail("Permission denied"));
	}
}

function admin_league_save_type() {
	global $site;
	
	if ($site->community->member >= LEVEL_ADMIN) {
		$l = league_get($site->post->id);
		$l->league_type = $site->post->type;
		$r = league_edit($l);
		if ($r->value) {
			$r->message = "League type saved.";
		}
		echo json_encode($r);
	} else {
		echo json_encode(return_obj_fail("Permission denied"));
	}
}

function admin_league_save_teamtype() {
	global $site;
	
	if ($site->community->member >= LEVEL_ADMIN) {
		$l = league_get($site->post->id);
		$l->team_type = $site->post->type;
		$r = league_edit($l);
		if ($r->value) {
			$r->message = "League team type saved.";
		}
		echo json_encode($r);
	} else {
		echo json_encode(return_obj_fail("Permission denied"));
	}
}

function admin_league_create() {
	global $site;
	
	$l = new StdClass();
	$l->key = generate_key();
	$l->name = $site->post->fullname;
	$l->name_url = name_to_urlname($l->name);
	$l->name_short = $site->post->shortname;
	$l->community = $site->post->community_id;
	$l->creator = $site->user->id;
 
	$r = league_create($l);
	
	// add creator as staff
	if ($r->value == RESULT_SUCCESS) {
		$s = new StdClass();
		$s->league_id = $r->league_id;
		$s->player_id = $site->user->player_id;
		$s->level = LEAGUE_STAFF_CREATOR;
		$s->added_by = $s->player_id;
		
		league_staff_add($s);
	}
	
	$c = community_get($l->community);
	redirect(
		sprintf(
			"/%s%s?op=league&sop=setup&k=%s", 
			$c->name_url, 
			$site->settings->uri_admin, 
			$l->key
		)
	);
}

function admin_league_delete() {
	global $site;
	
	$l = league_get_by_key($site->post->league_key);
	$c = community_get($l->community);
	
	// check if we're allowed
	
	league_delete($l->id);
	
	redirect(sprintf("/%s%s?op=league", $c->name_url, $site->settings->uri_admin));
}

function admin_league_staff_add() {
	global $site;
	$u = user_get_by_name($site->post->staffname);
	
	$s = new StdClass();
	$s->league_id = $site->post->league_id;
	$s->player_id = $u->player_id;
	$s->level = $site->post->stafflevel;
	$s->added_by = $site->user->id;
	
	league_staff_add($s);
	redirect_return();
}


function admin_league_staff_delete() {
	global $site;
	$r = league_staff_delete($site->post->id);
	echo json_encode($r);
}

function admin_league_group_add() {
	global $site;
	$g = new StdClass();
	$g->league = $site->post->league_id;
	$g->name = $site->post->groupname;
	$g->name_url = name_to_urlname($g->name);
	$g->playoff = 0;
	$g->player_count = 0;
	$g->creator = $site->user->id;
	
	group_create($g);
	
	redirect_return();
}

function admin_league_group_edit() {
	global $site;
	$g = group_get($site->post->group_id);
	$sort = (is_numeric($site->post->sortorder)) ? $site->post->sortorder : 0;
	
	if (isset($site->post->deleteleaguegroup) && $site->post->deleteleaguegroup == "Delete") {
		group_delete($g->id);
		redirect_return();
	}
	
	$g->name = $site->post->groupname;
	$g->name_url = name_to_urlname($site->post->groupnameurl);
	$g->sort_order = $sort;
	
	group_edit($g);
	
	redirect_return();
}

function admin_league_members() {
	global $site;
	$l = league_get_by_key($site->get->k);
	
	$members = league_player_list($l->access_key);
	$removebtn = (sizeof($members) > 0) ? "<button id=\"remove\" class=\"btn btn-xs btn-default\" type=\"submit\">Remove Selected</button>\n" : "\n";
?>

<div class="panel panel-default">
	<div class="panel-heading">Members - <?=$l->name?></div>
	<div class="panel-body" id="leaguemembers">
		<form class="form" role="form" method="post" action="<?=$site->settings->uri_admin?>">
			<div class="row">
				<div class="col-xs-9 col-sm-10 col-md-10 col-lg-11 col-xl-11"><input id="addmember" class="form-control" name="members" type="text" placeholder="<?=($l->team_type == LEAGUE_TEAM_TYPE_PLAYER) ? "Add Players" : "Add Teams"?>"/></div>
				<div class="col-xs-3 col-sm-2 col-md-2 col-lg-1 col-xl-1"><button class="btn btn-primary" type="submit">Add</button></div>
			</div>
			<input type="hidden" name="op" value="League.Member.Add"/>
			<input type="hidden" name="league" value="<?=$l->id?>"/>
			<input type="hidden" name="community_id" value="<?=$site->community->id?>"/>
			<input type="hidden" name="return" value="<?=get_return_url()?>"/>
		</form>
		<div class="space-md"></div>
		
		<form class="form" role="form" method="post" action="<?=$site->settings->uri_admin?>">
<?php for ($i=0; $i<sizeof($members); $i++) { ?>
			<div class="row">
				<div class="col-xs-2 col-sm-1 col-md-1 col-lg-1 col-xl-1"><input class="" type="checkbox" name="members[]" value="<?=$members[$i]->team?>"></div>
				<div class="col-xs-10 col-sm-11 col-md-11 col-lg-11 col-xl-11"><?=$members[$i]->name?></div>
			</div>
<?php } ?>
			<input type="hidden" name="op" value="League.Member.Remove"/>
			<input type="hidden" name="league" value="<?=$l->id?>"/>
			<input type="hidden" name="community_id" value="<?=$site->community->id?>"/>
			<input type="hidden" name="return" value="<?=get_return_url()?>"/>
			<div class="space-sm"></div>
			<?=$removebtn?>
		</form>

	</div>
</div>

<script type="text/javascript">
	$(document).ready(function($) {
		$("#addmember").magicSuggest({
			allowFreeEntries: false,
			data: '<?=$site->settings->uri_rpc?>',
			dataUrlParams: {op: <?=($l->team_type == LEAGUE_TEAM_TYPE_PLAYER) ? "'NameLookup'" : "'TeamLookup'"?>},
			hideTrigger: true,
			highlight: false,
			maxDropHeight: 145,
			method: 'get',
			minChars: 2
		});
	});
</script>
	
<style>
	.ui-autocomplete-loading {
		background: white url("<?=$site->settings->uri_loading?>") right center no-repeat;
	}
	
	/* Required when using autocomplete in modals, so it shows up on top of the modal */
	.ui-autocomplete {
		z-index: 5000;
	}
</style>



<?php
}

function admin_league_member_add() {
	global $site;
	
	for ($i=0; $i<sizeof($site->post->members); $i++) {
		league_player_add($site->post->league, $site->post->members[$i]);
	}
	
	redirect_return();
}

function admin_league_member_remove() {
	global $site;

	for ($i=0; $i<sizeof($site->post->members); $i++) {
		league_player_remove($site->post->league, $site->post->members[$i]);
	}

	redirect_return();
}

function admin_league_group_view() {
	global $site;
	$l = league_get_by_key($site->get->k);
	$g = group_get($site->get->id);
	$m = group_get_members($g->id);
	$a = league_get_available_members($l->id);
	$p = league_player_list($l->access_key);
?>

<div class="panel panel-default">
	<div class="panel-heading">Group - <?=$l->name . " // " . $g->name?><a data-toggle="modal" data-target="#editgroup" class="pull-right" title="Edit Group" href="#"><span class="glyphicon glyphicon-cog"></span></a></div>
	<div class="panel-body" id="leaguemembers">
		<h4>Members <span class="badge"><?=sizeof($m)?></span> <a data-toggle="modal" data-target="#editmembers" href="#" title="Edit Members"><span class="glyphicon glyphicon-cog"></span></a></h4>
		<div class="space-sm"></div>
		<ul>
<?php foreach ((array)$m as $member) { ?>
			<li><?=$member->member_name?></li>
<?php } ?>
		</ul>
	</div>
</div>

<div id="editgroup" class="modal fade">
	<form role="form" action="<?=url($site->settings->uri_rpc)?>" method="post">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Edit Group</h4>
				</div>
				<div class="modal-body">
					<p>Group Name</p>
					<p><input name="groupname" class="form-control" type="text" value="<?=$g->name?>" required/></p>
					<p>URL Name</p>
					<p><input name="groupnameurl" class="form-control" type="text" value="<?=$g->name_url?>" required/></p>
					<p>Sort</p>
					<p><input name="sortorder" class="form-control" type="text" value="<?=$g->sort_order?>" required/></p>
				</div>
				<div class="modal-footer">
					<input type="submit" class="btn btn-danger pull-left" name="deleteleaguegroup" value="Delete">
					<button type="submit" class="btn btn-primary" name="editleaguegroup">Save</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
		<input type="hidden" name="op" value="Admin.League.Group.Edit"/>
		<input type="hidden" name="league_id" value="<?=$l->id?>"/>
		<input type="hidden" name="group_id" value="<?=$g->id?>"/>
		<input type="hidden" name="return" value="<?=get_return_url()?>"/>
	</form>
</div>

<div id="editmembers" class="modal fade">
	<link href="/assets/css/multi-select-0.9.12.css" media="screen" rel="stylesheet" type="text/css">
	<form role="form" action="<?=url($site->settings->uri_rpc)?>" method="post">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Group Members - <?=$g->name?></h4>
				</div>
				<div class="modal-body">
					<select id="members-select" name="members[]" multiple="multiple">
<?php foreach ((array)$a as $member) { ?>
						<option value="<?=$member->member_id?>"><?=$member->member_name?></option>
<?php } ?>
<?php foreach ((array)$m as $member) { ?>
						<option value="<?=$member->member_id?>" selected><?=$member->member_name?></option>
<?php } ?>
					</select>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" name="editleaguegroup">Save</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
		<input type="hidden" name="op" value="Admin.League.Group.Members.Edit"/>
		<input type="hidden" name="league_id" value="<?=$l->id?>"/>
		<input type="hidden" name="group_id" value="<?=$g->id?>"/>
		<input type="hidden" name="return" value="<?=get_return_url()?>"/>
	</form>
</div>

<script src="/assets/js/jquery.multi-select-0.9.12.js"></script>
<script>
	$(document).ready(function($) {	
		$('#members-select').multiSelect({
			selectableHeader: "<em>Available</em>",
			selectionHeader: "<em>Members</em>",
			selectableFooter: "<a id=\"select-all\" href=\"#\">Select All</a>",
			selectionFooter: "<a id=\"select-none\" href=\"#\">Remove All</a>"
		});
		
		$('#select-all').click(function(){
			$('#members-select').multiSelect('select_all');
			return false;
		});
		
		$('#select-none').click(function(){
			$('#members-select').multiSelect('deselect_all');
			return false;
		});
	});
</script>
<?php
}

function admin_league_group_members_edit() {
	global $site;
	
	group_member_remove_all($site->post->group_id);
	
	foreach ((array)$site->post->members as $member) {
		group_member_add($site->post->group_id, $member);
	}
	
	redirect_return();
}

function admin_league_matches() {
	global $site;
	$l = league_get_by_key($site->get->k);
	$g = group_list($l->id);
	
	foreach ((array)$g as $group) {
		$matches = match_list($group->id, $l->team_type);
?>	
<div class="panel panel-default">
	<div class="panel-heading"><?=$group->name?> // <?=$l->name?> // Matches <span class="pull-right"><button data-toggle="modal" data-target="#newmatch<?=$group->id?>" class="btn btn-xs btn-primary">New Match</button> <button data-toggle="modal" data-target="#genmatches<?=$group->id?>" class="btn btn-xs btn-primary">Generate</button></span></div>
	<div class="panel-body">
<?php if (sizeof($matches) > 0) { ?>
		<form action="<?=admin_url()?>" method="post">
			<div class="row">
				<div class="col-md-4 col-header">Teams</div>
				<div class="col-md-1 col-header">Round</div>
			</div>
<?php foreach ((array)$matches as $match) { ?>
			<div class="row">
				<div class="col-md-4"><input type="checkbox" name="matches[]" value="<?=$match->id?>"> <a href="?op=league&sop=matches&k=<?=$site->get->k?>&mk=<?=$match->access_key?>"><?=$match->team_home_name?> vs. <?=$match->team_away_name?></a></div>
				<div class="col-md-1"><?=$match->round?></div>
			</div>
<?php } ?>
			<div class="row" style="margin-top: 10px"><div class="col-md-12"><button class="btn btn-xs btn-warning" name="func" value="swap" type="submit">Swap Teams</button> <button class="btn btn-xs btn-danger" name="func" value="delete" type="submit">Delete</button></div></div>
			<input type="hidden" name="op" value="League.Match.Edit"/>
			<input type="hidden" name="return" value="<?=get_return_url()?>"/>
		</form>
<?php } else { ?>
		No matches yet
<?php } ?>
	</div>
</div>

<div id="genmatches<?=$group->id?>" class="modal fade">
	<form role="form" action="<?=admin_url()?>" method="post">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Generate Matches for <?=$group->name?></h4>
				</div>
				<div class="modal-body">
					<p>This will create a match for every team against every other team breaking them up into rounds. For existing matches, the team positions and round number may change. </p>
					<p>Do you want to do this?</p>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" name="addleaguegroup">Yup</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Nope</button>
				</div>
			</div>
		</div>
		<input type="hidden" name="op" value="Admin.League.Match.Generate"/>
		<input type="hidden" name="group_id" value="<?=$group->id?>"/>
		<input type="hidden" name="return" value="<?=get_return_url()?>"/>
	</form>
</div>
<?php
	$mems = "<option id=\"0\"></option>";
	$gm = group_get_members($group->id);
	foreach ((array)$gm as $member) {
		$mems .= "<option value=\"{$member->member_id}\">{$member->member_name}</option>";
	}
?>
<div id="newmatch<?=$group->id?>" class="modal fade">
	<form role="form" action="<?=admin_url()?>" method="post">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Create match for <?=$group->name?></h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="hometeam">Home Team</label>
						<select class="form-control" id="hometeam" name="hometeam"><?=$mems?></select>
					</div>
					
					<div class="form-group">
						<label for="hometeam">Away Team</label>
						<select class="form-control" id="awayteam" name="awayteam"><?=$mems?></select>
					</div>
					
					<div class="form-group">
						<label for="hometeam">Round</label>
						<input class="form-control" id="round" name="round"/>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" name="addleaguegroup">Create</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
		<input type="hidden" name="op" value="Admin.League.Match.Create"/>
		<input type="hidden" name="group_id" value="<?=$group->id?>"/>
		<input type="hidden" name="league_id" value="<?=$l->id?>"/>
		<input type="hidden" name="creator" value="<?=$site->user->player_id?>"/>
		<input type="hidden" name="team_type" value="<?=$l->team_type?>"/>
		<input type="hidden" name="return" value="<?=get_return_url()?>"/>
	</form>
</div>

<?php
	}
}

function admin_league_match_edit() {
	global $site;
		
	if ($site->post->func == "swap") {
		foreach ((array)$site->post->matches as $mid) {
			match_swap_teams($mid);
		}
	}
	
	if ($site->post->func == "delete") {
		foreach ((array)$site->post->matches as $mid) {
			match_delete($mid);
		}
	}
	
	redirect_return();
}

function admin_league_match_generate() {
	global $site;
	
	match_autocreate($site->post->group_id);
	redirect_return();
}

function admin_league_match_create() {
	global $site;
	
	$slug = new StdClass();
	$slug->team_home = $site->post->hometeam;
	$slug->team_away = $site->post->awayteam;
	$slug->round = $site->post->round;
	$slug->access_key = generate_key();
	$slug->group_id = $site->post->group_id;
	$slug->league_id = $site->post->league_id;
	$slug->creator = $site->post->creator;
	$slug->team_type = $site->post->team_type;
	match_create($slug);
	redirect_return();
}

function admin_league_match_view() {
	global $site;
	
	$l = league_get_by_key($site->get->k);
	$g = group_list($l->id);
	$m = match_get_by_key($site->get->mk);
	
	if ($l == null) {
		admin_error(
			"League not found", 
			"No league with the key <span class=\"code-font\">{$site->get->k}</span> was found."
		);
	}
	
	if ($m == null) {
		admin_error(
			"Match not found", 
			"No match with the key <span class=\"code-font\">{$site->get->mk}</span> was found."
		);
	}
	
?>	
<div class="panel panel-default">
	<div class="panel-heading">Match Detail <span class="pull-right"><button data-toggle="modal" data-target="#newmatch<?=$group->id?>" class="btn btn-xs btn-primary">New Match</button> <button data-toggle="modal" data-target="#genmatches<?=$group->id?>" class="btn btn-xs btn-primary">Generate</button></span></div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">Home team:</div>
			<div class="col-md-9"><?=$m->name_home?></div>
		</div>
		<div class="row">
			<div class="col-md-3">Away team:</div>
			<div class="col-md-9"><?=$m->name_away?></div>
		</div>
		
		
		<form action="<?=$site->settings->uri_admin?>" method="post">
			<input type="hidden" name="op" value="League.Match.Edit"/>
			<input type="hidden" name="return" value="<?=get_return_url()?>"/>
		</form>
	</div>
</div>
<?php
}

function admin_error($header, $body = "") {
	global $site;
	echo "<div class=\"well\"><h2>$header</h2><p>$body</p></div></div>";
	include("_inc/footer.php");
	die();
}

function admin_features_save() {
	global $site;
	
	$c = community_get($site->post->community);
	$c->features = 0;
	
	foreach ((array) $site->post->features as $f) {
		$c->features += $f;
	}

	community_edit($c);
	redirect(admin_url());
}

function admin_community_roles_save() {
	global $site;
	
	$total = 0;
	foreach ((array) $site->post->staffroles as $role) {
		$total += $role;
	}
	
	$m = community_membership_get($site->post->memberid);
	$m->level_mask = $total;
	community_membership_edit($m);
	
	redirect_return();
}

function admin_community_staff_add() {
	global $site;
	
	$o = new StdClass();
	
	$roles = 0;
	foreach ((array) $site->post->staffroles as $perm) {
		$roles += $perm;
	}
	
	$o->level_mask = $roles;
	$o->community = $site->post->community;
	
	foreach ((array) $site->post->members as $member) {
		$o->player = $member;
		community_membership_add($o);
	}
	
	redirect_return();
}

function admin_community_staff_update() {
	global $site;
	
	foreach ((array) $site->post->members as $player) {
		community_membership_delete($player);
	}
	
	redirect_return();
}
?>